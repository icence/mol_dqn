import hashlib
def string_hash(text):
    return int(hashlib.md5(text.encode("utf-8")).hexdigest()[:24], 16)

def read(x, H, M):
  for i in range(1000):
    try:
      with open("{}/{}".format(x, i), "r") as f:
        for l in f:
          a, b, c, d = l.split()
          H[string_hash(a)] = float(c)
          M[string_hash(a)] = float(d)
    except:
      pass
