FROM deepchemio/deepchem:2.0.0-cpu
RUN apt-get update && apt-get install -y vim
RUN pip install --upgrade pip
RUN pip install \
    setuptools==41.0.0 \
    tensorflow==1.14.0 \
    numpy==1.14.6 \
    Django==3.0.5 \
    gast==0.2.2 \
    padelpy==0.1.6 \
    djangorestframework==3.11.0

WORKDIR /home
RUN pip install git+https://github.com/openai/baselines.git@ea25b9e8b234e6ee1bca43083f8f3cf974143998#egg=baselines
WORKDIR /src

