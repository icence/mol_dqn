# coding=utf-8
# Copyright 2020 The Google Research Authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Lint as: python2, python3
"""Optimizes QED of a molecule with DQN.

This experiment tries to find the molecule with the highest QED
starting from a given molecule.
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import functools
import json
import os

from absl import app
from absl import flags

from rdkit import Chem

from rdkit.Chem import QED
from tensorflow.compat.v1 import gfile
from mol_dqn.chemgraph.dqn import deep_q_networks
from mol_dqn.chemgraph.dqn import molecules as molecules_mdp
from mol_dqn.chemgraph.dqn import run_dqn
from mol_dqn.chemgraph.dqn.tensorflow_core import core
from subprocess_call import call_herg, call_mutagenicity, call_carcinogens, call_cytotoxicity, call_coco2, call_bbb
import time
import math
import asyncio
import tensorflow as tf
import hashlib
from reaction_list import get_rxns
from cache import read
##from collections import namedtuple
##XAI = namedtuple('id', 'func', 'score')

loop = asyncio.get_event_loop()

flags.DEFINE_string('inputfile',
                    '',
                    'The init mols.')

flags.DEFINE_string('predict_step',
                    '',
                    'The overwrite step.')
FLAGS = flags.FLAGS
##H_DICTS = {}
##M_DICTS = {}
##A_DICTS = {}
##Y_DICTS = {}
##O_DICTS = {}
##B_DICTS = {}

H_DICTS = "fileh"
M_DICTS = "filem"
A_DICTS = "filea"
Y_DICTS = "filey"
O_DICTS = "fileo"
B_DICTS = "fileb"

def get_cache_file(smiles, filename):
    if os.path.exists(filename) == False:
        with open(filename, "w"):
            pass

    with open(filename) as f:
        for line in f:
            line = line.split()
            if line[0] == smiles.strip():
                return float(line[1])


def write_cache_file(smiles, value, filename):
    with open(filename, "a") as f:
        f.writelines("{} {}\n".format(smiles, value))


def string_hash(text):
  return int(hashlib.md5(text.encode("utf-8")).hexdigest()[:24], 16)


def between(num, Max, Min):
  return max(min(num, Max), Min)


start_time = int(time.time())
class HergRewardMolecule(molecules_mdp.MultipleInitMolecule):
  """The molecule whose reward is the herg."""

  def __init__(self, discount_factor, idx, record_path=True, **kwargs):
    """Initializes the class.

    Args:
      discount_factor: Float. The discount factor. We only
        care about the molecule at the end of modification.
        In order to prevent a myopic decision, we discount
        the reward at each step by a factor of
        discount_factor ** num_steps_left,
        this encourages exploration with emphasis on long term rewards.
      **kwargs: The keyword arguments passed to the base class.
    """
    super().__init__(**kwargs)
    self.record_path=record_path
    self.reward_path=[]
    self.idx = idx
    self.discount_factor = discount_factor
    self.fileidx = 0

  async def _get_mol_reward(self, state, dicts, func):
    try:
      score = get_cache_file(state, dicts)
      print("ls:", state, score)
      if score is not None:
        return score
      else:
        molecule = Chem.MolFromSmiles(state)
        if molecule is None or state == "":
          score = 0.0
        else:
          score = await loop.run_in_executor(None, func, state)
        write_cache_file(state, score, dicts)
        return score
    except:
      print("Error:", state)
      print("Error:", func.__name__)
      a = 1/0

  def weighted_score(self, score, threshold):
      score = 1 - score 
      if score > threshold:
          a = threshold
          b = score - threshold
          return a + 0.3 * b
      else:
          return score

  def weighted_score1(self, score, threshold, power=2):
      return -1*math.pow(score, power) + 1

  async def get_xai_points(self, state, targets):
    if state == "":
       return [1,1,1,1,1,1]

    if 'herg' in targets:
      herg = await self._get_mol_reward(state, H_DICTS, call_herg)
    else:
      herg = 1
    if 'mutagenicity' in targets:
      muta = await self._get_mol_reward(state, M_DICTS, call_mutagenicity)
    else:
      muta = 1
    if 'carcinogens' in targets:
      carc = await self._get_mol_reward(state, A_DICTS, call_carcinogens)
    else:
      carc = 1
    if 'cytotoxicity' in targets:
      cyto = await self._get_mol_reward(state, Y_DICTS, call_cytotoxicity)
    else:
      cyto = 1
    if 'coco2' in targets:
      coco2 = await self._get_mol_reward(state, O_DICTS, call_coco2)
      coco2 = (coco2 + 3.5) / -4.5
      coco2 = between(coco2, 1, 0)
    else:
      coco2 = 1
    if 'bbb' in targets:
      bbb = await self._get_mol_reward(state, B_DICTS, call_bbb)
      bbb = 1 - bbb
    else:
      bbb = 1

    return [herg, muta, carc, cyto, coco2, bbb]


  def get_penalty(self, state):
    sim_init = self.get_len_similarity(state, self._get_init_state())
    sim_last = self.get_len_similarity(state, self._get_last_state())
    stop_penalty = self._get_stop_penalty(state)
    return [sim_init, sim_last, stop_penalty]

  async def _get_total(self, state, targets):
    if state == "":
      return 0, [0,0,0], [1,1,1,1,1,1]
    xai_points = await self.get_xai_points(state, targets)
    penalty = self.get_penalty(state)
    score = 0
    for i, j in zip(xai_points, [1,0.3,1,1,0.33,0.3]):
      if i != 1:
         score += self.weighted_score(i, j)

    for s in self._path:
        if self.get_len_similarity(s, self._get_init_state()) < 0.45:
            print(s, self._get_init_state())
            print(self.get_len_similarity(s, self._get_init_state()))
            score = 0
    score = between(score, len(targets), 0)
    return score, penalty, xai_points

  def _get_init_state(self):
    return self._path[0]

  def _get_last_state(self):
    try:
      return self._path[-2]
    except:
      return self._path[0]

  def _get_stop_penalty(self, state):
      if state == self._get_last_state():
          return 0.002
      return 0

  async def _reward(self):
    """Reward of a state.

    Returns:
      Float. QED of the current state.
    """
    targets=['mutagenicity', 'coco2', 'bbb']
    if self.num_steps_taken == 1:
      a =  await self._get_total(self._get_init_state(), targets=targets)
      print(a)
      self.last_total = a[0]
      self.last_sim = a[1]
      self.last_score = a[2]

    total, sim, score = await self._get_total(self._state, targets=targets)
    if self.idx == 0:
      if self.num_steps_taken == 1:  
        t = int(time.time()) - start_time
        print ("tim:", t//3600, t//60, t)
        with open(self.model_dir + "/" + str(self.fileidx), "w") as f:
          a = [self.last_sim[0], *self.last_score]
          f.writelines(make_str(self._get_last_state(), [self.last_total, 0, self.last_sim[0], *(self.last_score)]))
        with open(self.model_dir + '/log' + str(self.fileidx), "w") as f:
          rprint("No.:", 0, f)
          rprint("Ori:", self._get_last_state(), f)
          if 'herg' in targets: 
            rprint("her:", self.last_score[0], f)
          if 'mutagenicity' in targets:
            rprint("mut:", self.last_score[1], f)
          if 'carcinogens' in targets:
            rprint("car:", self.last_score[2], f)
          if 'cytotoxicity' in targets:
            rprint("cyt:", self.last_score[3], f)
          if 'coco2' in targets:
            rprint("cac:", self.last_score[4], f)
          if 'bbb' in targets:
            rprint("bbb:", self.last_score[5], f)


      if self.num_steps_taken < self.max_steps:
       with open(self.model_dir + '/log' + str(self.fileidx), "a+") as f:
        rprint("No.:", self.num_steps_taken, f)
        rprint("sta:", self._state, f)
        if 'herg' in targets: 
         rprint("her:", score[0], f)
        if 'mutagenicity' in targets:
         rprint("mut:", score[1], f)
        if 'carcinogens' in targets:
         rprint("car:", score[2], f)
        if 'cytotoxicity' in targets:
         rprint("cyt:", score[3], f)
        if 'coco2' in targets:
         rprint("cac:", score[4], f)
        if 'bbb' in targets:
         rprint("bbb:", score[5], f)
        rprint("sim:", sim[0], f)
        rprint("lst:", self.last_total, f)
        rprint("all:", total, f)
        rprint("dif:", total - self.last_total - sim[-1], f)
      else:
       with open(self.model_dir + '/log' + str(self.fileidx), "a+") as f:
        rprint ("No.:", self.num_steps_taken, f)
        rprint ("sta:", "END_STATE", f)

    diff = total - self.last_total - sim[-1]
    self.last_sim = sim
    self.last_score = score

    ret = None
    if self._state == "" and self.num_steps_taken >= self.max_steps:
        ret = self.last_total
    self.last_total = total

    if self._state:
      ret = diff

    with open(self.model_dir + "/" + str(self.fileidx), "a+") as f:
        f.writelines(make_str(self._state, [total, ret, sim[0], *score]))
    if self.num_steps_taken >= self.max_steps:
        self.fileidx += 1
    return ret

def make_str(s, a):
  if s.strip() == "":
     s = "END_STATE===================================================================================="
  z = [s.strip()]
 
  for i in a:
    z.append(str(round(i,3)))

  z[0] = z[0] + "\n"
  return "\t".join(z) + '\n'

def rprint(a, x, fp=None):
  if fp:
    if type(x) == type(""):
      fp.writelines(a + ' ' + x + '\n')
    else:
      fp.writelines(a + ' ' + str(round(x, 3)) + '\n')
  if type(x) != type(""):
    print(a, round(x, 3))
  else:
    print(a, x)


def main(argv):
  del argv  # unused.
  if FLAGS.hparams is not None:
    with gfile.Open(FLAGS.hparams, 'r') as f:
      hparams = deep_q_networks.get_hparams(**json.load(f))
  else:
    hparams = deep_q_networks.get_hparams()
##  print(FLAGS.inputfile)
##  return 
##  if FLAGS.rxns
##  print (hparams.rxn_files)
##  rxns = None
  if hparams.rxn_files:
      rxns = get_rxns(hparams.rxn_files)
  else:
      rxns = None
  if FLAGS.old_model_dir:
      read(FLAGS.old_model_dir, H_DICTS, M_DICTS)
  dqn = deep_q_networks.DeepQNetwork(
      step = 0,
      input_shape=(hparams.batch_size, hparams.fingerprint_length*2 + 1),
      q_fn=functools.partial(
          deep_q_networks.multi_layer_model, hparams=hparams),
      optimizer=hparams.optimizer,
      grad_clipping=hparams.grad_clipping,
      num_bootstrap_heads=hparams.num_bootstrap_heads,
      gamma=hparams.gamma,
      epsilon=1.0)

  environments = []
  for i in range(1):
    environment = HergRewardMolecule(
        idx = i,
        discount_factor=hparams.discount_factor,
        atom_types=set(hparams.atom_types),
        init_mols=FLAGS.inputfile,
        allow_removal=hparams.allow_removal,
        allow_no_modification=hparams.allow_no_modification,
        allow_bonds_between_rings=hparams.allow_bonds_between_rings,
        allowed_ring_sizes=set(hparams.allowed_ring_sizes),
        max_steps=hparams.max_steps_per_episode,
        model_dir=FLAGS.model_dir,
        rxns=rxns)
    environments.append(environment)

  tf.reset_default_graph()

  with tf.Session() as sess:
    dqn.build()
    all_vars = tf.global_variables()
##    print(all_vars)
##    for v in all_vars:
##          print("%s with value %s" % (v.name, sess.run(v)))
##        if v.name == "d/OptimizeLoss/d/q_fn/dense_0/kernel/Adam:0":
##          print("%s with value %s" % (v.name, sess.run(v)))
##    return
    if FLAGS.old_model_dir:
        sess.run(tf.global_variables_initializer())
##        saver = tf.train.Saver(tf.trainable_variables())
        saver = tf.train.Saver()
        saver.restore(sess, tf.train.latest_checkpoint(FLAGS.old_model_dir))
    else:
        sess.run(tf.global_variables_initializer())
##    for v in all_vars:
##        if v.name == "d/OptimizeLoss/d/q_fn/dense_0/kernel/Adam:0":
##        if "ower" in v.name:
##          print("%s with value %s" % (v.name, sess.run(v)))
##    return
    dqn.step = run_dqn.val_tf(dqn.scope + "/global_step:0")
    tasks = [loop.create_task(run_dqn.run_training(hparams, env, dqn, sess)) for env in environments]

    loop.run_until_complete(asyncio.wait(tasks))
  core.write_hparams(hparams, os.path.join(FLAGS.model_dir, 'config.json'))


if __name__ == '__main__':
##  try:
##    with open('data_h', 'r') as fp:
##      H_DICTS = json.load(fp)
##    with open('data_m', 'r') as fp:
##      M_DICTS = json.load(fp)
##    with open('data_a', 'r') as fp:
##      A_DICTS = json.load(fp)
##    with open('data_y', 'r') as fp:
##      Y_DICTS = json.load(fp)
##    with open('data_o', 'r') as fp:
##      O_DICTS = json.load(fp)
##    with open('data_b', 'r') as fp:
##      B_DICTS = json.load(fp)
    app.run(main)
##  except:
##    pass
##  finally:
##    with open('data_h', 'w') as fp:
##      json.dump(H_DICTS, fp)
##    with open('data_m', 'w') as fp:
##      json.dump(M_DICTS, fp)
##    with open('data_a', 'w') as fp:
##      json.dump(A_DICTS, fp)
##    with open('data_y', 'w') as fp:
##      json.dump(Y_DICTS, fp)
##    with open('data_o', 'w') as fp:
##      json.dump(O_DICTS, fp)
##    with open('data_b', 'w') as fp:
##      json.dump(B_DICTS, fp)
##    with open('data' + str(int(time.time()))+'.txt', 'w') as fp:
##      json.dump(DICTS, fp)
