# coding=utf-8
# Copyright 2020 The Google Research Authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Lint as: python2, python3
"""Executor for deep Q network models."""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import functools
import json
import os
import sys
import time

from absl import app
from absl import flags
from absl import logging
from baselines.common import schedules
from baselines.deepq import replay_buffer

import numpy as np

from rdkit import Chem
from rdkit import DataStructs
from rdkit.Chem import AllChem
from rdkit.Chem import Descriptors
from rdkit.Chem import QED

from six.moves import range
import tensorflow.compat.v1 as tf
from tensorflow.compat.v1 import gfile

from mol_dqn.chemgraph.dqn import deep_q_networks
from mol_dqn.chemgraph.dqn import molecules as molecules_mdp
from mol_dqn.chemgraph.dqn.py import molecules
from mol_dqn.chemgraph.dqn.tensorflow_core import core
import asyncio
import requests
import json

flags.DEFINE_string('old_model_dir',
                    '',
                    'The model for training or predicting.')
flags.DEFINE_string('model_dir',
                    '/namespace/gas/primary/zzp/dqn/r=3/exp2_bs_dqn',
                    'The directory to save data to.')
flags.DEFINE_string('target_molecule', 'C1CCC2CCCCC2C1',
                    'The SMILES string of the target molecule.')
flags.DEFINE_string('start_molecule', None,
                    'The SMILES string of the start molecule.')
flags.DEFINE_float(
    'similarity_weight', 0.5,
    'The weight of the similarity score in the reward function.')
flags.DEFINE_float('target_weight', 493.60,
                   'The target molecular weight of the molecule.')
flags.DEFINE_string('hparams', None, 'Filename for serialized HParams.')
flags.DEFINE_boolean('multi_objective', False,
                     'Whether to run multi objective DQN.')

##flags.DEFINE_string('step',
##                    '',
##                    'The overwrite step.')

FLAGS = flags.FLAGS

##logging.get_absl_handler().use_absl_log_file('absl_logging', './')

class TargetWeightMolecule(molecules_mdp.Molecule):
  """Defines the subclass of a molecule MDP with a target molecular weight."""

  def __init__(self, target_weight, **kwargs):
    """Initializes the class.

    Args:
      target_weight: Float. the target molecular weight.
      **kwargs: The keyword arguments passed to the parent class.
    """
    super(TargetWeightMolecule, self).__init__(**kwargs)
    self.target_weight = target_weight

  def _reward(self):
    """Calculates the reward of the current state.

    The reward is defined as the negative l1 distance between the current
    molecular weight and target molecular weight range.

    Returns:
      Float. The negative distance.
    """
    molecule = Chem.MolFromSmiles(self._state)
    if molecule is None:
      return -self.target_weight**2
    lower, upper = self.target_weight - 25, self.target_weight + 25
    mw = Descriptors.MolWt(molecule)
    if lower <= mw <= upper:
      return 1
    return -min(abs(lower - mw), abs(upper - mw))


class MultiObjectiveRewardMolecule(molecules_mdp.Molecule):
  """Defines the subclass of generating a molecule with a specific reward.

  The reward is defined as a 1-D vector with 2 entries: similarity and QED
    reward = (similarity_score, qed_score)
  """

  def __init__(self, target_molecule, **kwargs):
    """Initializes the class.

    Args:
      target_molecule: SMILES string. the target molecule against which we
        calculate the similarity.
      **kwargs: The keyword arguments passed to the parent class.
    """
    super(MultiObjectiveRewardMolecule, self).__init__(**kwargs)
    target_molecule = Chem.MolFromSmiles(target_molecule)
    self._target_mol_fingerprint = self.get_fingerprint(target_molecule)
    self._target_mol_scaffold = molecules.get_scaffold(target_molecule)
    self.reward_dim = 2

  def get_fingerprint(self, molecule):
    """Gets the morgan fingerprint of the target molecule.

    Args:
      molecule: Chem.Mol. The current molecule.

    Returns:
      rdkit.ExplicitBitVect. The fingerprint of the target.
    """
    return AllChem.GetMorganFingerprint(molecule, radius=2)

  def get_similarity(self, smiles):
    """Gets the similarity between the current molecule and the target molecule.

    Args:
      smiles: String. The SMILES string for the current molecule.

    Returns:
      Float. The Tanimoto similarity.
    """

    structure = Chem.MolFromSmiles(smiles)
    if structure is None:
      return 0.0
    fingerprint_structure = self.get_fingerprint(structure)

    return DataStructs.TanimotoSimilarity(self._target_mol_fingerprint,
                                          fingerprint_structure)

  def _reward(self):
    """Calculates the reward of the current state.

    The reward is defined as a tuple of the similarity and QED value.

    Returns:
      A tuple of the similarity and qed value
    """
    # calculate similarity.
    # if the current molecule does not contain the scaffold of the target,
    # similarity is zero.
    if self._state is None:
      return 0.0, 0.0
    mol = Chem.MolFromSmiles(self._state)
    if mol is None:
      return 0.0, 0.0
    if molecules.contains_scaffold(mol, self._target_mol_scaffold):
      similarity_score = self.get_similarity(self._state)
    else:
      similarity_score = 0.0
    # calculate QED
    qed_value = QED.qed(mol)
    return similarity_score, qed_value


# TODO(zzp): use the tf.estimator interface.
def run_training(hparams, environment, dqn, sess):
    """Runs the training procedure.
  
    Briefly, the agent runs the action network to get an action to take in
    the environment. The state transition and reward are stored in the memory.
    Periodically the agent samples a batch of samples from the memory to
    update(train) its Q network. Note that the Q network and the action network
    share the same set of parameters, so the action network is also updated by
    the samples of (state, action, next_state, reward) batches.
  
  
    Args:
      hparams: tf.contrib.training.HParams. The hyper parameters of the model.
      environment: molecules.Molecule. The environment to run on.
      dqn: An instance of the DeepQNetwork class.
  
    Returns:
      None
    """
    summary_writer = tf.summary.FileWriter(FLAGS.model_dir)
##    all_vars = tf.trainable_variables()
##    model_saver = tf.train.Saver(all_vars, max_to_keep=hparams.max_num_checkpoints)
    model_saver = tf.train.Saver(max_to_keep=hparams.max_num_checkpoints)
    # The schedule for the epsilon in epsilon greedy policy.
    exploration = schedules.PiecewiseSchedule(
        [(0, 1.0), (int(hparams.num_episodes / 2), 0.1),
         (hparams.num_episodes, 0.01)],
        outside_value=0.01)
    if hparams.prioritized:
      memory = replay_buffer.PrioritizedReplayBuffer(hparams.replay_buffer_size,
                                                     hparams.prioritized_alpha)
      beta_schedule = schedules.LinearSchedule(
          hparams.num_episodes, initial_p=hparams.prioritized_beta, final_p=0)
    else:
      memory = replay_buffer.ReplayBuffer(hparams.replay_buffer_size)
      beta_schedule = None

##    if FLAGS.old_model_dir:
##        saver = tf.train.Saver()
##        saver.restore(sess, tf.train.latest_checkpoint(FLAGS.old_model_dir))
##    else:
##        sess.run(tf.global_variables_initializer())
##    print(dqn.step)

    sess.run(dqn.update_op)
##    global_step = val_tf(dqn.scope + "/global_step:0")
    global_step = 0
    for episode in range(hparams.num_episodes):
      global_step = _episode(
          environment=environment,
          dqn=dqn,
          memory=memory,
          episode=episode,
          global_step=global_step,
          hparams=hparams,
          summary_writer=summary_writer,
          exploration=exploration,
          beta_schedule=beta_schedule)
##      dqn.step = global_step
      global_ste = val_tf(dqn.scope + "/global_step:0")
##      print("global: ", dqn.step, global_ste)
      if (episode + 1) % hparams.update_frequency == 0:
##        print("dqn update")
        sess.run(dqn.update_op)
      if (episode + 1) % hparams.save_frequency == 0:
        model_saver.save(
            sess,
            os.path.join(FLAGS.model_dir, 'ckpt'),
            write_meta_graph=False,
            global_step=dqn.step)
        with open(FLAGS.model_dir + '/ep', 'w') as f:
            f.write(str(episode))

def val_tf(_name):
  v = tf.get_default_graph().get_tensor_by_name(_name)
  return tf.get_default_session().run(v)

def print_tf(_name="", _type="trainable"):
  if _type == "trainable":
    all_vars = tf.trainable_variables()
  else:
    all_vars = tf.global_variables()

  if _name:
    v = tf.get_default_graph().get_tensor_by_name(_name)
    print("%s with value %s" % (v.name, tf.get_default_session().run(v)))
  else:
    for v in all_vars:
##      print("%s with value " % (v.name, ))
      print("%s with value %s" % (v.name, tf.get_default_session().run(v)))

def _episode(environment, dqn, memory, episode, global_step, hparams,
             summary_writer, exploration, beta_schedule):
  """Runs a single episode.

  Args:
    environment: molecules.Molecule; the environment to run on.
    dqn: DeepQNetwork used for estimating rewards.
    memory: ReplayBuffer used to store observations and rewards.
    episode: Integer episode number.
    global_step: Integer global step; the total number of steps across all
      episodes.
    hparams: HParams.
    summary_writer: FileWriter used for writing Summary protos.
    exploration: Schedule used for exploration in the environment.
    beta_schedule: Schedule used for prioritized replay buffers.

  Returns:
    Updated global_step.
  """
  episode_start_time = time.time()
  environment.initialize()
  if hparams.num_bootstrap_heads:
    head = np.random.randint(hparams.num_bootstrap_heads)
  else:
    head = 0
  for step in range(hparams.max_steps_per_episode):
    result = _step(
        environment=environment,
        dqn=dqn,
        memory=memory,
        episode=episode,
        hparams=hparams,
        exploration=exploration,
        head=head)
    if step == hparams.max_steps_per_episode - 1:
      episode_summary = dqn.log_result(result.state, result.reward)
      summary_writer.add_summary(episode_summary, global_step)
      print ("\n\nepisode: {}".format(episode))
      print_tf("d_1/OptimizeLoss/beta1_power:0", "")  
      logging.info('Episode %d/%d took %gs', episode + 1, hparams.num_episodes,
                   time.time() - episode_start_time)
      logging.info('SMILES: %s\n', result.state)
      # Use %s since reward can be a tuple or a float number.
      logging.info('The reward is: %s', str(environment.last_total))
    if (episode > min(50, hparams.num_episodes / 10)) and (
        global_step % hparams.learning_frequency == 0):
      if hparams.prioritized:
        (state_t, _, reward_t, state_tp1, done_mask, weight,
         indices) = memory.sample(
             hparams.batch_size, beta=beta_schedule.value(episode))
      else:
        (state_t, _, reward_t, state_tp1,
         done_mask) = memory.sample(hparams.batch_size)
        weight = np.ones([reward_t.shape[0]])
      # np.atleast_2d cannot be used here because a new dimension will
      # be always added in the front and there is no way of changing this.
      if reward_t.ndim == 1:
        reward_t = np.expand_dims(reward_t, axis=1)
      td_error, error_summary, _ = dqn.train(
          states=state_t,
          rewards=reward_t,
          next_states=state_tp1,
          done=np.expand_dims(done_mask, axis=1),
          weight=np.expand_dims(weight, axis=1))
      summary_writer.add_summary(error_summary, global_step)
##      logging.info('Current TD error: %.4f', (np.abs(td_error)))
##      logging.info('Current TD er: %.4f', aaa)
      logging.info('Current TD error: %.4f', np.mean(np.abs(td_error)))
##      logging.info('Current TD error: ', (np.abs(td_error)))
      if hparams.prioritized:
        memory.update_priorities(
            indices,
            np.abs(np.squeeze(td_error) + hparams.prioritized_epsilon).tolist())
    global_step += 1
    dqn.step += 1
  return global_step

def build_observations(act, hparams, steps_left, start_mol):
      ret = np.append(deep_q_networks.get_fingerprint(act, hparams), steps_left)
      return np.append(ret, start_mol)


def _step(environment, dqn, memory, episode, hparams, exploration, head):
  """Runs a single step within an episode.

  Args:
    environment: molecules.Molecule; the environment to run on.
    dqn: DeepQNetwork used for estimating rewards.
    memory: ReplayBuffer used to store observations and rewards.
    episode: Integer episode number.
    hparams: HParams.
    exploration: Schedule used for exploration in the environment.
    head: Integer index of the DeepQNetwork head to use.

  Returns:
    molecules.Result object containing the result of the step.
  """
  # Compute the encoding for each valid action from the current state.
  if environment.num_steps_taken == 0:
      if FLAGS.predict_step:
          environment._counter = hparams.max_steps_per_episode - int(FLAGS.predict_step)
  steps_left = hparams.max_steps_per_episode - environment.num_steps_taken
  valid_actions = sorted(list(environment.get_valid_actions()))

##      if int(FLAGS.predict_step) <= 0:
##         sys.exit(0)
##      steps_left = int(FLAGS.predict_step)
##  if steps_left == 1:
##      valid_actions = [""]
  start_mol = deep_q_networks.get_fingerprint(environment._path[0], hparams)

  observations = np.vstack([
      build_observations(act, hparams, steps_left, start_mol)
      for act in valid_actions
  ])
  action = valid_actions[dqn.get_action(observations, head=head, update_epsilon=exploration.value(episode), use_last_action=False)]
##  print("this action", action)
##  print("hiss action", valid_actions[dqn.get_action(observations, head=head, update_epsilon=exploration.value(episode), use_last_action=True)])
  actions = dqn.get_actions(observations, head=head)
##  logging.info(valid_actions)
##  sys.exit(0)
##  act = dqn.get_action(observations, head=head)
##  logging.info('The action are: %s', str(act))
##  logging.info('The actions are: %s', str(action))
  action_t_fingerprint = build_observations(action, hparams, steps_left, start_mol)
  if False:
      w_url = 'http://192.168.0.99:5000/writefile'
      d = {'smiles': action}
      try:
          a = requests.post(w_url, data=d).text
      except:
          pass
  if FLAGS.predict_step:
##      logging.info(valid_actions)
##      logging.info(actions)
##      logging.info(int(steps_left))
##      logging.info(environment.num_steps_taken)
      print(FLAGS.inputfile)
      write_actions(valid_actions, actions, steps_left, FLAGS.inputfile)
  result = environment.step(action)

  steps_left = hparams.max_steps_per_episode - environment.num_steps_taken
  action_fingerprints = np.vstack([
      build_observations(act, hparams, steps_left, start_mol)
      for act in environment.get_valid_actions()
  ])
  # we store the fingerprint of the action in obs_t so action
  # does not matter here.

##  logging.info('The actions are: %s %s', str(action), str(steps_left))
##  logging.info('The acti: %s %s', str(result.state), str(result.reward))

##  if steps_left == 0:
##    print("step0")
##    action_fingerprints = np.vstack([
##        build_observations(act, hparams, steps_left, start_mol)
##        for act in [""]
##    ])
##  print("next", type(action_fingerprints))
##  print("next", action_fingerprints)
  print("next", steps_left, action)
  print("result.termin", float(result.terminated))
  memory.add(
      obs_t=action_t_fingerprint,
      action=0,
      reward=result.reward,
      obs_tp1=action_fingerprints,
      done=float(result.terminated))

##  if steps_left > 0 or True:
##      action = environment.get_valid_actions()[dqn.get_action(action_fingerprints, head=head, update_epsilon=exploration.value(episode))]
##      print("next_action", action)
##      with open("/src/mnt/dqn/6/mol_dqn/zi", "w") as f:
##          f.write(action)
##      w_url = 'http://192.168.0.99:5000/writefile'
##      d = {'smiles': action}
##      try:
##          a = requests.post(w_url, data=d).text
##      except:
##          pass
##
##  else:
##      dqn.last_action = None

  return result

def write_actions(valid_actions ,actions, steps_left, inputfile):
  if steps_left <= 0:
    sys.exit(0)
    return
##  else:
##    logging.info(steps_left)
  count = 0
##  with open(inputfile.replace('temp', '')) as f:
##     lines = [x for x in f]
##  print(lines)
  os.makedirs(FLAGS.old_model_dir+'/predict', exist_ok=True)
  inputfile_trun = inputfile.split('/')[-1]
  filename = FLAGS.old_model_dir+'/predict/' + inputfile_trun + '.out'
  if os.path.exists(filename) == False:
      with open(inputfile, 'r') as f:
          x = [x for x in f]
      with open(filename, 'w') as f:
          f.writelines(x[0].strip() + '\n')
  with open(filename, "a") as f:
    for i in actions:
##      print(i)
##      print(valid_actions)
##      f.writelines(lines)
##      if lines[-1].endswith('\n') == False:
##         f.writelines('\n')
      f.writelines(valid_actions[i].strip() + '\n')
      count += 1
      if count >= 2:
        if steps_left == 1 and count == 2:
            continue
        else:
            break

##  sys.exit(0)
  

##def run_dqn(multi_objective=False):
##  """Run the training of Deep Q Network algorithm.
##
##  Args:
##    multi_objective: Boolean. Whether to run the multiobjective DQN.
##  """
##  if FLAGS.hparams is not None:
##    with gfile.Open(FLAGS.hparams, 'r') as f:
##      hparams = deep_q_networks.get_hparams(**json.load(f))
##  else:
##    hparams = deep_q_networks.get_hparams()
##  logging.info(
##      'HParams:\n%s', '\n'.join([
##          '\t%s: %s' % (key, value)
##          for key, value in sorted(hparams.values().items())
##      ]))
##
##  # TODO(zzp): merge single objective DQN to multi objective DQN.
##  if multi_objective:
##    environment = MultiObjectiveRewardMolecule(
##        target_molecule=FLAGS.target_molecule,
##        atom_types=set(hparams.atom_types),
##        init_mol=FLAGS.start_molecule,
##        allow_removal=hparams.allow_removal,
##        allow_no_modification=hparams.allow_no_modification,
##        allow_bonds_between_rings=False,
##        allowed_ring_sizes={3, 4, 5, 6},
##        max_steps=hparams.max_steps_per_episode)
##
##    dqn = deep_q_networks.MultiObjectiveDeepQNetwork(
##        objective_weight=np.array([[FLAGS.similarity_weight],
##                                   [1 - FLAGS.similarity_weight]]),
##        input_shape=(hparams.batch_size, hparams.fingerprint_length + 1),
##        q_fn=functools.partial(
##            deep_q_networks.multi_layer_model, hparams=hparams),
##        optimizer=hparams.optimizer,
##        grad_clipping=hparams.grad_clipping,
##        num_bootstrap_heads=hparams.num_bootstrap_heads,
##        gamma=hparams.gamma,
##        epsilon=1.0)
##  else:
##    environment = TargetWeightMolecule(
##        target_weight=FLAGS.target_weight,
##        atom_types=set(hparams.atom_types),
##        init_mol=FLAGS.start_molecule,
##        allow_removal=hparams.allow_removal,
##        allow_no_modification=hparams.allow_no_modification,
##        allow_bonds_between_rings=hparams.allow_bonds_between_rings,
##        allowed_ring_sizes=set(hparams.allowed_ring_sizes),
##        max_steps=hparams.max_steps_per_episode)
##
##    dqn = deep_q_networks.DeepQNetwork(
##        input_shape=(hparams.batch_size, hparams.fingerprint_length + 1),
##        q_fn=functools.partial(
##            deep_q_networks.multi_layer_model, hparams=hparams),
##        optimizer=hparams.optimizer,
##        grad_clipping=hparams.grad_clipping,
##        num_bootstrap_heads=hparams.num_bootstrap_heads,
##        gamma=hparams.gamma,
##        epsilon=1.0)
##
##  run_training(
##      hparams=hparams,
##      environment=environment,
##      dqn=dqn,
##  )
##
##  core.write_hparams(hparams, os.path.join(FLAGS.model_dir, 'config.json'))
##
##
##def main(argv):
##  del argv  # unused.
##  run_dqn(FLAGS.multi_objective)
##
##
##if __name__ == '__main__':
##  app.run(main)
