# coding=utf-8
# Copyright 2020 The Google Research Authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Lint as: python2, python3
"""Optimizes QED of a molecule with DQN.

This experiment tries to find the molecule with the highest QED
starting from a given molecule.
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import functools
import json
import os

from absl import app
from absl import flags

from rdkit import Chem

from rdkit.Chem import QED
from tensorflow.compat.v1 import gfile
from mol_dqn.chemgraph.dqn import deep_q_networks
from mol_dqn.chemgraph.dqn import molecules as molecules_mdp
from mol_dqn.chemgraph.dqn import run_dqn
from mol_dqn.chemgraph.dqn.tensorflow_core import core
from subprocess_call import call_herg, call_mutagenicity, call_carcinogens, call_cytotoxicity
import time
import math
import asyncio
import tensorflow as tf
import hashlib
from reaction_list import get_rxns
from cache import read
loop = asyncio.get_event_loop()

flags.DEFINE_string('inputfile',
                    '',
                    'The init mols.')

flags.DEFINE_string('step',
                    '',
                    'The overwrite step.')
FLAGS = flags.FLAGS
H_DICTS = {}
M_DICTS = {}
A_DICTS = {}
Y_DICTS = {}
def string_hash(text):
  return int(hashlib.md5(text.encode("utf-8")).hexdigest()[:24], 16)

def between(a, Max, Min):
  a = min(a, Max)
  a = max(a, Min)
  return a

start_time = int(time.time())
class HergRewardMolecule(molecules_mdp.MultipleInitMolecule):
  """The molecule whose reward is the herg."""

  def __init__(self, discount_factor, idx, record_path=True, **kwargs):
    """Initializes the class.

    Args:
      discount_factor: Float. The discount factor. We only
        care about the molecule at the end of modification.
        In order to prevent a myopic decision, we discount
        the reward at each step by a factor of
        discount_factor ** num_steps_left,
        this encourages exploration with emphasis on long term rewards.
      **kwargs: The keyword arguments passed to the base class.
    """
    super().__init__(**kwargs)
    self.record_path=record_path
    self.reward_path=[]
    self.idx = idx
    self.discount_factor = discount_factor
    self.fileidx = 0

  async def _get_herg_reward(self, state):
    return await self._get_mol_reward(state, H_DICTS, call_herg)
  async def _get_mutagenicity_reward(self, state):
    return await self._get_mol_reward(state, M_DICTS, call_mutagenicity)
  async def _get_carcinogens_reward(self, state):
    return await self._get_mol_reward(state, A_DICTS, call_carcinogens)
  async def _get_cytotoxicity_reward(self, state):
    return await self._get_mol_reward(state, Y_DICTS, call_cytotoxicity)

  async def _get_mol_reward(self, state, dicts, func):
    s_hash = string_hash(state)
    score = dicts.get(s_hash)
    if score is None:
      molecule = Chem.MolFromSmiles(state)
      if molecule is None or state == "":
        dicts[s_hash] = 0
        return 0.0
      score = await loop.run_in_executor(None, func, state)
      dicts[s_hash] = score
    return score

  async def _get_total(self, state, targets=['herg'], init=False):
    if 'herg' in targets:
      herg = await self._get_herg_reward(state)
    else:
      herg = 1
    if 'mutagenicity' in targets:
      muta = await self._get_mutagenicity_reward(state)
    else:
      muta = 1
    if 'carcinogens' in targets:
      carc = await self._get_carcinogens_reward(state)
    else:
      carc = 1
    if 'cytotoxicity' in targets:
      cyto = await self._get_cytotoxicity_reward(state)
    else:
      cyto = 1

    sim = self.get_similarity(state)
    if init:
##      return 1 - herg + 1 - muta + 1 - carc + 1 - cyto, [sim, sim], [herg, muta, carc, cyto]
      sort = sorted([herg, muta, carc, cyto])
      score = 2 * (1 - sort[3]) + 1 * (1 - sort[2]) + 0.5 * (1 - sort[1]) + 0.5 * (1 - sort[0])
      return score, [sim, sim], [herg, muta, carc, cyto]

    sim2 = self.get_similarity(self._get_last_state(), state)
    
    if sim > 0.4:
        adj_sim = 1
    elif sim > 0.2:
        adj_sim = 0.75
    elif sim > 0.1:
        adj_sim = 0.5
    elif sim > 0.05:
        adj_sim = 0.25
    else:
        adj_sim = 0
        
##    if sim2 > 0.2:
##        adj_sim2 = 0
##    else:
##        adj_sim2 = 0.02
##
    if self._get_last_state() == self._state:
        adj_sim2 = 0.08
    else:
        adj_sim2 = 0.0

    if sim < 0.1:
        adj_sim3 = 0
    else:
        adj_sim3 = 1
    score = (1 - herg + 1 - muta + 1 - carc + 1 - cyto) * adj_sim3 + adj_sim
    
    sort = sorted([herg, muta, carc, cyto])
    score = 2 * (1 - sort[3]) + 1 * (1 - sort[2]) + 0.5 * (1 - sort[1]) + 0.5 * (1 - sort[0])
    score = score * adj_sim3 

    max_lim = len(targets) 
    min_lim = 0
    return between(score, max_lim, min_lim), [sim, adj_sim2], [herg, muta, carc, cyto]

  def _get_last_state(self):
    return self._path[-2]
  def _get_init_state(self):
    return self._path[0]

  async def _reward(self):
    """Reward of a state.

    Returns:
      Float. QED of the current state.
    """
    if self.num_steps_taken == 1:
      a =  await self._get_total(self._get_init_state(), targets=['herg', 'mutagenicity', 'carcinogens', 'cytotoxicity'], init=True)
      print(a)
      self.last_total = a[0]
      self.last_sim = a[1]
      self.last_score = a[2]
##      self.last_total, self.last_sim, self.last_score = await self._get_total(self._get_init_state(), targets=['herg', 'mutagenicity', 'carcinogens', 'cytotoxicity'], init=True)

    total, sim, score = await self._get_total(self._state, targets=['herg', 'mutagenicity', 'carcinogens', 'cytotoxicity'])
    
    if self.idx == 0:
      if self.num_steps_taken == 1:  
        t = int(time.time()) - start_time
        print ("tim:", t//3600, t//60, t)
        with open(self.model_dir + "/" + str(self.fileidx), "w") as f:
          a = [self.last_sim[0], *self.last_score]
          f.writelines(make_str(self._get_last_state(), [self.last_sim[0], *(self.last_score)]))
        with open(self.model_dir + '/log' + str(self.fileidx), "w") as f:
          rprint("No.:", 0, f)
          rprint("Ori:", self._get_last_state(), f)
          rprint("her:", self.last_score[0], f)
          rprint("mut:", self.last_score[1], f)
          rprint("car:", self.last_score[2], f)
          rprint("cyt:", self.last_score[3], f)
      with open(self.model_dir + "/" + str(self.fileidx), "a+") as f:
        if self._state:
          f.writelines(make_str(self._state, [sim[0], *score]))

      if self.num_steps_taken < self.max_steps:
       with open(self.model_dir + '/log' + str(self.fileidx), "a+") as f:
        rprint("No.:", self.num_steps_taken, f)
        rprint("sta:", self._state, f)
        rprint("her:", score[0], f)
        rprint("mut:", score[1], f)
        rprint("car:", score[2], f)
        rprint("cyt:", score[3], f)
        rprint("sim:", sim[0], f)
        rprint("lst:", self.last_total, f)
        rprint("all:", total, f)
        rprint("dif:", total - self.last_total - sim[1], f)
      else:
       with open(self.model_dir + '/log' + str(self.fileidx), "a+") as f:
        rprint ("No.:", self.num_steps_taken, f)
        rprint ("sta:", "END_STATE", f)
        self.fileidx += 1

    diff = total - self.last_total - sim[1]
    self.last_sim = sim
    self.last_score = score
    self.last_total = total
    
    if self._state:
      return diff
    else:
      return total

def make_str(s, a):
  z = [s.strip()]
  for i in a:
    z.append(str(round(i,3)))

  z[0] = z[0] + (95-len(z[0]))*" "
  return "\t".join(z) + '\n'

def rprint(a, x, fp=None):
  if fp:
    if type(x) == type(""):
      fp.writelines(a + ' ' + x + '\n')
    else:
      fp.writelines(a + ' ' + str(round(x, 3)) + '\n')
  if type(x) != type(""):
    print(a, round(x, 3))
  else:
    print(a, x)


def main(argv):
  del argv  # unused.
  if FLAGS.hparams is not None:
    with gfile.Open(FLAGS.hparams, 'r') as f:
      hparams = deep_q_networks.get_hparams(**json.load(f))
  else:
    hparams = deep_q_networks.get_hparams()
##  print(FLAGS.inputfile)
##  return 
##  if FLAGS.rxns
##  print (hparams.rxn_files)
##  rxns = None
  if hparams.rxn_files:
      rxns = get_rxns(hparams.rxn_files)
  else:
      rxns = None
  if FLAGS.old_model_dir:
      read(FLAGS.old_model_dir, H_DICTS, M_DICTS)
  dqn = deep_q_networks.DeepQNetwork(
      step = 0,
      input_shape=(hparams.batch_size, hparams.fingerprint_length + 1),
      q_fn=functools.partial(
          deep_q_networks.multi_layer_model, hparams=hparams),
      optimizer=hparams.optimizer,
      grad_clipping=hparams.grad_clipping,
      num_bootstrap_heads=hparams.num_bootstrap_heads,
      gamma=hparams.gamma,
      epsilon=1.0)

  environments = []
  for i in range(1):
    environment = HergRewardMolecule(
        idx = i,
        discount_factor=hparams.discount_factor,
        atom_types=set(hparams.atom_types),
        init_mols=FLAGS.inputfile,
        allow_removal=hparams.allow_removal,
        allow_no_modification=hparams.allow_no_modification,
        allow_bonds_between_rings=hparams.allow_bonds_between_rings,
        allowed_ring_sizes=set(hparams.allowed_ring_sizes),
        max_steps=hparams.max_steps_per_episode,
        model_dir=FLAGS.model_dir,
        rxns=rxns)
    environments.append(environment)

  tf.reset_default_graph()

  with tf.Session() as sess:
    dqn.build()
    all_vars = tf.global_variables()
##    print(all_vars)
##    for v in all_vars:
##          print("%s with value %s" % (v.name, sess.run(v)))
##        if v.name == "d/OptimizeLoss/d/q_fn/dense_0/kernel/Adam:0":
##          print("%s with value %s" % (v.name, sess.run(v)))
##    return
    if FLAGS.old_model_dir:
        sess.run(tf.global_variables_initializer())
        saver = tf.train.Saver(tf.trainable_variables())
##        saver = tf.train.Saver()
        saver.restore(sess, tf.train.latest_checkpoint(FLAGS.old_model_dir))
    else:
        sess.run(tf.global_variables_initializer())
##    for v in all_vars:
##        if v.name == "d/OptimizeLoss/d/q_fn/dense_0/kernel/Adam:0":
##        if "ower" in v.name:
##          print("%s with value %s" % (v.name, sess.run(v)))
##    return
    dqn.step = run_dqn.val_tf(dqn.scope + "/global_step:0")
    tasks = [loop.create_task(run_dqn.run_training(hparams, env, dqn, sess)) for env in environments]

    loop.run_until_complete(asyncio.wait(tasks))
  core.write_hparams(hparams, os.path.join(FLAGS.model_dir, 'config.json'))


if __name__ == '__main__':
  try:
    with open('data_h', 'r') as fp:
      H_DICTS = json.load(fp)
    with open('data_m', 'r') as fp:
      M_DICTS = json.load(fp)
    with open('data_a', 'r') as fp:
      A_DICTS = json.load(fp)
    with open('data_y', 'r') as fp:
      Y_DICTS = json.load(fp)
    app.run(main)
  except:
    with open('data_h', 'w') as fp:
      json.dump(H_DICTS, fp)
    with open('data_m', 'w') as fp:
      json.dump(M_DICTS, fp)
    with open('data_a', 'w') as fp:
      json.dump(A_DICTS, fp)
    with open('data_y', 'w') as fp:
      json.dump(Y_DICTS, fp)
##    with open('data' + str(int(time.time()))+'.txt', 'w') as fp:
##      json.dump(DICTS, fp)
