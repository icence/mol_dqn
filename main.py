# coding=utf-8
# Copyright 2020 The Google Research Authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Lint as: python2, python3
"""Optimizes QED of a molecule with DQN.

This experiment tries to find the molecule with the highest QED
starting from a given molecule.
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import functools
import json
import os

from absl import app
from absl import flags

from rdkit import Chem

from rdkit.Chem import QED
from tensorflow.compat.v1 import gfile
from mol_dqn.chemgraph.dqn import deep_q_networks
from mol_dqn.chemgraph.dqn import molecules as molecules_mdp
from mol_dqn.chemgraph.dqn import run_dqn
from mol_dqn.chemgraph.dqn.tensorflow_core import core
from subprocess_call import call_herg
import time
import math
import asyncio
import tensorflow as tf
import hashlib

loop = asyncio.get_event_loop()

flags.DEFINE_string('inputfile',
                    '',
                    'The init mols.')

FLAGS = flags.FLAGS
DICTS = {}
def string_hash(text):
  return int(hashlib.md5(text.encode("utf-8")).hexdigest()[:24], 16)

start_time = int(time.time())
class HergRewardMolecule(molecules_mdp.MultipleInitMolecule):
  """The molecule whose reward is the herg."""

  def __init__(self, discount_factor, idx, record_path=True, **kwargs):
    """Initializes the class.

    Args:
      discount_factor: Float. The discount factor. We only
        care about the molecule at the end of modification.
        In order to prevent a myopic decision, we discount
        the reward at each step by a factor of
        discount_factor ** num_steps_left,
        this encourages exploration with emphasis on long term rewards.
      **kwargs: The keyword arguments passed to the base class.
    """
    super().__init__(**kwargs)
    self.record_path=record_path
    self.reward_path=[]
    self.idx = idx
    self.discount_factor = discount_factor
    self.fileidx = 0

  async def _get_mol_reward(self, state):
    s_hash = string_hash(state)
    score = DICTS.get(s_hash)
    if score is None:
      molecule = Chem.MolFromSmiles(state)
      if molecule is None or state == "":
        DICTS[s_hash] = 0
        return 0.0
      score = await loop.run_in_executor(None, call_herg, state)
      DICTS[s_hash] = score
    return score

  def _get_last_state(self):
    return self._path[-2]

  async def _reward(self):
    """Reward of a state.

    Returns:
      Float. QED of the current state.
    """
    score = await self._get_mol_reward(self._state)
    sim = self.get_similarity(self._state)
##    total = 1.5 * min(sim, 0.7) + 0.3 * max(0, sim - 0.7) +1 - score 
    total =  min(sim, 0.7) / 0.7 + 1 - score
    if self.last_total is None:
      last_score = await self._get_mol_reward(self._get_last_state())

##      if self.idx == 1:
##        print("lasts: ",last_score)
      last_sim = self.get_similarity(self._get_last_state())
      self.last_total =  min(last_sim, 0.7) / 0.7 + 1 - last_score
      if self.idx == 1:
        print("\nOri:", self._get_last_state(), last_score)
    if self.idx == 1:
      t = int(time.time()) - start_time
##      if len(DICTS) > 100000:
##        DICTS.clear()
##      print (self._path)
      print ("tim:", t//3600, t//60, t)
      if self._state:
##        print (self._path)
        print ("No.:", self.num_steps_taken)#, self._state)
        rprint ("sco:", score)
        rprint ("sim:", sim)
        rprint ("lst:", self.last_total)
        rprint ("all:", total)
        rprint ("dif:", total - self.last_total)
      else:
##        print ("null_state")
        print ("No.:", self.num_steps_taken, " END_STATE")
##        rprint ("sco: ", last_score)
##        rprint ("sim: ", last_sim)
##        rprint ("all      : ", self.last_total)
##        rprint ("lst: ", self.last_total)
##        rprint ("dif      : ", 0.0)
##
##        if self.idx == 1:
##            self.fileidx += 1
##            with open("/src/mnt/dqn/6/mol_dqn/a/"+str(self.fileidx), "w") as f:
##                for m in self._path:
##                    if m:
##                        f.writelines(m+'\n')
##                        f.writelines(str(round(self.get_similarity(m), 3)))
##                        f.writelines('\n')
##                        f.writelines(str(round(DICTS[string_hash(m)], 3)))
##                        f.writelines('\n')

            
    diff = total - self.last_total
    self.last_total = total
    if self._state:
      return diff
    else:
      return 0.0

def rprint(a , x):
  print(a, round(x, 3))


def main(argv):
  del argv  # unused.
  if FLAGS.hparams is not None:
    with gfile.Open(FLAGS.hparams, 'r') as f:
      hparams = deep_q_networks.get_hparams(**json.load(f))
  else:
    hparams = deep_q_networks.get_hparams()
##  print(FLAGS.inputfile)
##  return 
  environment1 = HergRewardMolecule(
      idx = 1,
      discount_factor=hparams.discount_factor,
      atom_types=set(hparams.atom_types),
      init_mols=FLAGS.inputfile,
      allow_removal=hparams.allow_removal,
      allow_no_modification=hparams.allow_no_modification,
      allow_bonds_between_rings=hparams.allow_bonds_between_rings,
      allowed_ring_sizes=set(hparams.allowed_ring_sizes),
      max_steps=hparams.max_steps_per_episode)
  dqn = deep_q_networks.DeepQNetwork(
      step = 0,
      input_shape=(hparams.batch_size, hparams.fingerprint_length + 1),
      q_fn=functools.partial(
          deep_q_networks.multi_layer_model, hparams=hparams),
      optimizer=hparams.optimizer,
      grad_clipping=hparams.grad_clipping,
      num_bootstrap_heads=hparams.num_bootstrap_heads,
      gamma=hparams.gamma,
      epsilon=1.0)

  environment2 = HergRewardMolecule(
      idx = 2,
      discount_factor=hparams.discount_factor,
      atom_types=set(hparams.atom_types),
      init_mols=FLAGS.inputfile,
      allow_removal=hparams.allow_removal,
      allow_no_modification=hparams.allow_no_modification,
      allow_bonds_between_rings=hparams.allow_bonds_between_rings,
      allowed_ring_sizes=set(hparams.allowed_ring_sizes),
      max_steps=hparams.max_steps_per_episode)
  environment3 = HergRewardMolecule(
      idx = 3,
      discount_factor=hparams.discount_factor,
      atom_types=set(hparams.atom_types),
      init_mols=FLAGS.inputfile,
      allow_removal=hparams.allow_removal,
      allow_no_modification=hparams.allow_no_modification,
      allow_bonds_between_rings=hparams.allow_bonds_between_rings,
      allowed_ring_sizes=set(hparams.allowed_ring_sizes),
      max_steps=hparams.max_steps_per_episode)
  environment4 = HergRewardMolecule(
      idx = 4,
      discount_factor=hparams.discount_factor,
      atom_types=set(hparams.atom_types),
      init_mols=FLAGS.inputfile,
      allow_removal=hparams.allow_removal,
      allow_no_modification=hparams.allow_no_modification,
      allow_bonds_between_rings=hparams.allow_bonds_between_rings,
      allowed_ring_sizes=set(hparams.allowed_ring_sizes),
      max_steps=hparams.max_steps_per_episode)
  environment5 = HergRewardMolecule(
      idx = 5,
      discount_factor=hparams.discount_factor,
      atom_types=set(hparams.atom_types),
      init_mols=FLAGS.inputfile,
      allow_removal=hparams.allow_removal,
      allow_no_modification=hparams.allow_no_modification,
      allow_bonds_between_rings=hparams.allow_bonds_between_rings,
      allowed_ring_sizes=set(hparams.allowed_ring_sizes),
      max_steps=hparams.max_steps_per_episode)
  environment6 = HergRewardMolecule(
      idx = 6,
      discount_factor=hparams.discount_factor,
      atom_types=set(hparams.atom_types),
      init_mols=FLAGS.inputfile,
      allow_removal=hparams.allow_removal,
      allow_no_modification=hparams.allow_no_modification,
      allow_bonds_between_rings=hparams.allow_bonds_between_rings,
      allowed_ring_sizes=set(hparams.allowed_ring_sizes),
      max_steps=hparams.max_steps_per_episode)
  environment7 = HergRewardMolecule(
      idx = 7,
      discount_factor=hparams.discount_factor,
      atom_types=set(hparams.atom_types),
      init_mols=FLAGS.inputfile,
      allow_removal=hparams.allow_removal,
      allow_no_modification=hparams.allow_no_modification,
      allow_bonds_between_rings=hparams.allow_bonds_between_rings,
      allowed_ring_sizes=set(hparams.allowed_ring_sizes),
      max_steps=hparams.max_steps_per_episode)

  tf.reset_default_graph()

  with tf.Session() as sess:
    dqn.build()
    all_vars = tf.global_variables()
##    print(all_vars)
##    for v in all_vars:
##          print("%s with value %s" % (v.name, sess.run(v)))
##        if v.name == "d/OptimizeLoss/d/q_fn/dense_0/kernel/Adam:0":
##          print("%s with value %s" % (v.name, sess.run(v)))
##    return
    if FLAGS.old_model_dir:
        sess.run(tf.global_variables_initializer())
##        saver = tf.train.Saver(tf.trainable_variables())
        saver = tf.train.Saver()
        saver.restore(sess, tf.train.latest_checkpoint(FLAGS.old_model_dir))
    else:
        sess.run(tf.global_variables_initializer())
##    for v in all_vars:
##        if v.name == "d/OptimizeLoss/d/q_fn/dense_0/kernel/Adam:0":
##        if "ower" in v.name:
##          print("%s with value %s" % (v.name, sess.run(v)))
##    return
    dqn.step = run_dqn.val_tf(dqn.scope + "/global_step:0")
    tasks = []
##    run_dqn.run_training(hparams, environment1, dqn, sess, 1)
    tasks.append(loop.create_task(run_dqn.run_training(hparams, environment1, dqn, sess, 1)))
##    tasks.append(loop.create_task(run_dqn.run_training(hparams, environment2, dqn, sess, 2)))
##    tasks.append(loop.create_task(run_dqn.run_training(hparams, environment3, dqn, sess, 3)))
##    tasks.append(loop.create_task(run_dqn.run_training(hparams, environment4, dqn, sess, 4)))
##    tasks.append(loop.create_task(run_dqn.run_training(hparams, environment5, dqn, sess, 5)))
##    tasks.append(loop.create_task(run_dqn.run_training(hparams, environment6, dqn, sess, 6)))
##    tasks.append(loop.create_task(run_dqn.run_training(hparams, environment7, dqn, sess, 7)))

    loop.run_until_complete(asyncio.wait(tasks))
  core.write_hparams(hparams, os.path.join(FLAGS.model_dir, 'config.json'))


if __name__ == '__main__':
  try:
    app.run(main)
  except:
    with open('data' + str(int(time.time()))+'.txt', 'w') as fp:
      json.dump(DICTS, fp)
