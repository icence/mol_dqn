import tempfile
import subprocess
import os
import sys
import json
from rdkit import Chem, DataStructs
from rdkit.Chem.AllChem import GetMorganFingerprint
def create_file(input_str, rxn=None):
    with tempfile.NamedTemporaryFile('w+t', delete=False) as fp:
        if rxn:
            fp.write(input_str + '\n')
            fp.write(rxn)
        else:
            fp.write('smiles,name,value\n')
            fp.write(input_str + ',' + input_str + ',1\n')
        return fp.name

def create_protein_file(input_str):
    with tempfile.NamedTemporaryFile('w+t', delete=False) as fp:
        fp.write('smiles,name,value\n')
        fp.write(input_str.replace('_', ',')  + ',1\n')
    return fp.name     

def delete_file(filename):
    os.unlink(filename)

def get_fingerprint(molecule):
    return GetMorganFingerprint(molecule, radius=2)

def get_similarity(smiles, smiles2):
    structure = Chem.MolFromSmiles(smiles)
    if structure is None:
      return 0.0
    fingerprint_structure = get_fingerprint(structure)

    if smiles2:
       structure = Chem.MolFromSmiles(smiles2)
       if structure is None:
           return 0.0
       fingerprint_structure2 = get_fingerprint(structure)
       return DataStructs.TanimotoSimilarity(fingerprint_structure, fingerprint_structure2)

def call_sim(smiles, smiles2):
    return get_similarity(smiles, smiles2)

def call_logp(smiles):
    filename = create_file(smiles)
    a = subprocess.check_output("python call_model.py logp " + filename + " 2>/tmp/null", shell=True)
    a = str(a).split('\\n')[-2]
    return float(a)


def call_herg(smiles):
    filename = create_file(smiles)
    a = subprocess.check_output("python call_model.py herg " + filename + " 2>/tmp/null", shell=True)
    a = str(a).split('\\n')[-2]
    return float(a)

def call_mutagenicity(smiles):
    filename = create_file(smiles)
    a = subprocess.check_output("python call_model.py mutagenicity " + filename + " 2>/tmp/null", shell=True)
    a = str(a).split('\\n')[-2]
    return float(a)

def call_carcinogens(smiles):
    filename = create_file(smiles)
    a = subprocess.check_output("python call_model.py carcinogens " + filename + " 2>/tmp/null", shell=True)
    a = str(a).split('\\n')[-2]
    return float(a)

def call_cytotoxicity(smiles):
    filename = create_file(smiles)
    a = subprocess.check_output("python call_model.py cytotoxicity " + filename + " 2>/tmp/null", shell=True)
    a = str(a).split('\\n')[-2]
    return float(a)

def call_coco2(smiles):
    filename = create_file(smiles)
    a = subprocess.check_output("python call_model.py coco2 " + filename + " 2>/tmp/null", shell=True)
    a = str(a).split('\\n')[-2]
    return float(a)

def call_bbb(smiles):
    filename = create_file(smiles)
    a = subprocess.check_output("python call_model.py BBB " + filename + " 2>/tmp/null", shell=True)
    a = str(a).split('\\n')[-2]
    return float(a)

def call_all(smiles):
    filename = create_file(smiles)
    a = subprocess.check_output("python call_model.py mutagenicity_coco2_BBB " + filename + " 2>/tmp/null", shell=True)
    a = str(a).split('\\n')[-2]
    return a

def call_protein_docking(smiles, protein):
    filename = create_protein_file(smiles+'_'+protein)
    a = subprocess.check_output("python call_docking_model.py docking " + filename + " " + protein + " 2>/tmp/null", shell=True)
    a = str(a).split('\\n')[-2]
    return float(a)

def call_rxn(smiles, rxn, filename=None):
##    if filename is None:
    filename = create_file(smiles, rxn)
    a = subprocess.check_output("python call_rxn.py " + filename + " 2>/tmp/null", shell=True)
    return json.loads(a.decode().strip().replace("'", '"'))

def call_rxns(smiles, rxn_filename='zz_reaction'):
    filename = create_file(smiles, rxn_filename)
    a = subprocess.check_output("python3 call_rxns.py " + rxn_filename + " " + filename + " 2>/tmp/null", shell=True)
    print(a)
    return json.loads(a.decode().strip().replace("'", '"'))


##call_herg("C1CN(CCC1NC2=NC3=CC=CC=C3N2CC4=CC=C(C=C4)F)CCC5=CC=C(C=C5)O")
def create_input_file(input_str):
    with tempfile.NamedTemporaryFile('w+t', delete=False) as fp:
        fp.write(input_str)
        return fp.name
def list_tree():
    cmd = "tree b"
    a = subprocess.check_output(cmd, shell=True)
    print(str(a.decode()))

def cat(x):
    cmd = "cat {}".format(x)
    a = subprocess.check_output(cmd, shell=True)
    print(str(a.decode()))
cmd2 = 'python train5.py --model_dir="/tmp/tmp1231231231231" --old_model_dir="/src/mnt/dqn/backup6/6/mol_dqn/52"  --inputfile="{}" --hparams="/src/mnt/dqn/backup6/6/mol_dqn/configs/naive_dqn.json" --predict_step={} 2>/tmp/tmp123123'
cmd3 = 'python train_logp.py --model_dir="/tmp/tmp1231231231231" --old_model_dir="/src/mnt/dqn/7/mol_dqn/logp3" --inputfile="{}" --rxn="true" --hparams="./configs/naive_dqn_muta.json" --predict_step={}'# 2>/tmp/tmp123123'
def call_dqn(filename, step):
    if step == 1:
        with open(filename, "r") as f:
            lines = [x for x in f]
        with open(filename+'f', "w") as f:
            f.writelines(lines)
        return
##    if os.path.exists(filename+'0'):
##        return
    with open(filename, "r") as f:
        lines = [x for x in f]
        lines = [lines[-1]]
    for input_str in lines:
        input_str = input_str.split()[0]
        input_str = input_str.split(',')[0]
        with open(filename+'temp', "w") as f:
            f.writelines(lines)
##        temp = create_input_file(input_str)
        s = cmd3.format(filename, step)
        print(s)
        print(os.path.dirname(os.path.abspath(__file__)))
        subprocess.check_output(s, shell=True)
##        delete_file(temp)

if __name__ == '__main__':
  old_model_dir = '/src/mnt/dqn/7/mol_dqn/logp3'
  call_dqn(old_model_dir + '/predict/z', 4)

##    mol = "C=C(C1=C(C)C(O)C(C)=C(CN)C1=O)C1(C)CCC(O)(NC(=O)C2CCCN2C(=O)C2C=CC3NC2c2ccccc2C3CC23CC4CC(CC(C4)C2)C3)C(=O)N1C(C)C"
####    mol = "CC1C=CC(=CC=1)[N+](=O)[O-]"
####    mol = 'CC1C(=O)C(C)C(C)(O)C(=O)C=1CCCCC(NC(=O)C1CCCN1C(=O)C(C=CC(CC(C)C)NC(=O)OC(C)C)CC1=CC=CC=N1)C(=O)NC(C)C'
####    mol = '[N+](=O)([O-])C1=CC=C(C=O)C=C1'
##    mol = 'C(C(C)C)S(=O)(=O)Cl'
####    rxn = "{'id': 'US06166006', 'smarts': '[N+:1]([C:4]1[CH:11]=[CH:10][C:7]([CH:8]=O)=[CH:6][CH:5]=1)([O-:3])=[O:2].[CH3:12][O:13][CH2:14][CH2:15][NH2:16].[CH2:17]=O>ClCCCl>[CH3:12][O:13][CH2:14][CH2:15][N:16]([CH2:8][C:7]1[CH:10]=[CH:11][C:4]([N+:1]([O-:3])=[O:2])=[CH:5][CH:6]=1)[CH3:17]', 'products': ['COCCN(C)CC1=CC=C(C=C1)[N+](=O)[O-]'], 'reactants': ['[N+](=O)([O-])C1=CC=C(C=O)C=C1', 'COCCN', 'C=O']}"
##    print(call_rxns(mol, 'zzyc'))

##    mol = "C(=O)(C)N[C@H](C(=O)N[C@H](C(=O)N(CCCCC)CCCCC)CCC(=O)O)Cc1ccc(cc1)OP(O)(O)O"
##    mol = "[H]OC([H])([H])[H]"
##    mol = "[H]C1([H])CC1(C)C(O)C(C)=CC"
##    mol = "CC1=C(C)C(O)C(C)=C(CCCCC(NC(=O)C2CCC(=C(c3ccccc3)C(C=O)C=CC(N)CCC34CC5CC(CC(C5)C3)C4)N2)C(=O)NC(C)C)C1=O"
##    mol = "CC1=C(C)C(=O)C(CCCC[C@H](NC(=O)[C@@H]2CCCN2C(=O)[C@H](/C=C/C(N)CCC23CC4CC(CC(C4)C2)C3)CC2=CC=CC=C2)C(=O)NC(C)C)=C(C)C1=O"
##    mol = "CC1=CC(=O)C(C)=C(CCCCC2(NC(=O)C3CCC4=C(c5ccccc5)C(C=CC5CCC67CC8CC(CC(C8)C6=N5)C7)C(=O)N43)CC(C)NC2=O)C1=O"
##    import time
##    print(time.time())
##    print(call_mutagenicity(mol))
##    print(call_protein_docking(mol, 'casp3'))
####    print(call_carcinogens(mol))
####    print(call_cytotoxicity(mol))
##    print(call_coco2(mol))
##    print(call_bbb(mol))
##    print(time.time())
##    print(call_all(mol))
##    print(time.time())
##    print(call_mutagenicity("C1CN(CCC1NC2=NC3=CC=CC=C3N2CC4=CC=C(C=C4)F)CCC5=CC=C(C=C5)O"))
##    print(call_herg("C1CN(CCC1NC2=NC3=CC=CC=C3N2CC4=CC=C(C=C4)F)CCC5=CC=C(C=C5)O"))
##    print(call_carcinogens("C1CN(CCC1NC2=NC3=CC=CC=C3N2CC4=CC=C(C=C4)F)CCC5=CC=C(C=C5)O"))
##    print(call_cytotoxicity("C1CN(CCC1NC2=NC3=CC=CC=C3N2CC4=CC=C(C=C4)F)CCC5=CC=C(C=C5)O"))
##    print(call_cytotoxicity("C[C@H]1[C@H]([C@H](C[C@@H](O1)O[C@H]2C[C@@](CC3=C2C(=C4C(=C3O)C(=O)C5=C(C4=O)C(=CC=C5)OC)O)(C(=O)C)O)N)O.Cl"))
##  list_tree()



##  old_model_dir = '/src/mnt/dqn/6/mol_dqn/52'
##  os.makedirs(old_model_dir+'/predict', exist_ok=True)
##  with open(sys.argv[-1]) as f:
##      for l in f:
##          smile = l.split(',')
##          break
##  with open(old_model_dir+'/predict/a', "w") as f:
##      f.writelines(smile)
##  call_dqn(old_model_dir + '/predict/a', 10)
##  call_dqn(old_model_dir + '/predict/a3', 9)
##  call_dqn(old_model_dir + '/predict/a32', 8)
##  call_dqn(old_model_dir + '/predict/a322', 7)
##  call_dqn(old_model_dir + '/predict/a3222', 6)
##  call_dqn(old_model_dir + '/predict/a32222', 5)
##  call_dqn(old_model_dir + '/predict/a322222', 4)
##  call_dqn(old_model_dir + '/predict/a3222222', 3)
##  call_dqn(old_model_dir + '/predict/a32222222', 2)
##  call_dqn(old_model_dir + '/predict/a322222222', 1)
##  for a in range(5):
##    for aa in range(5):
##      for aaa in range(5):
##        for aaaa in range(5):
##          sfx = "a32222{}{}{}{}".format(a,aa,aaa,aaaa)
##          for i in range(1,11):
##            call_dqn(old_model_dir + '/predict/' + sfx[:i], 11-i)



####  with open(old_model_dir+'/predict/9nextk', 'w') as f:
##    with open(old_model_dir+'/predict/9nextt') as ff:
##      lines = [x for x in ff]
##      f.writelines(lines[2])
##  call_dqn(old_model_dir + '/predict/9nextk', 9)
##  with open(old_model_dir+'/predict/8nextk', 'w') as f:
##    with open(old_model_dir+'/predict/8nextt') as ff:
##      lines = [x for x in ff]
##      f.writelines(lines[2])
##  call_dqn(old_model_dir + '/predict/8nextk', 8)
##  with open(old_model_dir+'/predict/7nextk', 'w') as f:
##    with open(old_model_dir+'/predict/7nextt') as ff:
##      lines = [x for x in ff]
##      f.writelines(lines[2])
##  call_dqn(old_model_dir + '/predict/7nextk', 7)
##  with open(old_model_dir+'/predict/6nextk', 'w') as f:
##    with open(old_model_dir+'/predict/6nextt') as ff:
##      lines = [x for x in ff]
##      f.writelines(lines[2])
##  call_dqn(old_model_dir + '/predict/6nextk', 6)
##  with open(old_model_dir+'/predict/5nextk', 'w') as f:
##    with open(old_model_dir+'/predict/5nextt') as ff:
##      lines = [x for x in ff]
##      f.writelines(lines[2])
##  call_dqn(old_model_dir + '/predict/5nextk', 5)
##  with open(old_model_dir+'/predict/4nextk', 'w') as f:
##    with open(old_model_dir+'/predict/4nextt') as ff:
##      lines = [x for x in ff]
##      f.writelines(lines[0])
##  call_dqn(old_model_dir + '/predict/4nextk', 4)
##  with open(old_model_dir+'/predict/3nextk', 'w') as f:
##    with open(old_model_dir+'/predict/3nextt') as ff:
##      lines = [x for x in ff]
##      f.writelines(lines[2])
##  call_dqn(old_model_dir + '/predict/3nextk', 3)
##  with open(old_model_dir+'/predict/2nextk', 'w') as f:
##    with open(old_model_dir+'/predict/2nextt') as ff:
##      lines = [x for x in ff]
##      f.writelines(lines[2])
##  call_dqn(old_model_dir + '/predict/2nextk', 2)
##  with open(old_model_dir+'/predict/1nextk', 'w') as f:
##    with open(old_model_dir+'/predict/1nextt') as ff:
##      lines = [x for x in ff]
##      f.writelines(lines[2])
##  call_dqn(old_model_dir + '/predict/1nextk', 1)
