import tensorflow as tf
import numpy as np

from tensorflow.keras.layers import Layer, Dense
from tensorflow.keras import backend as K
from tensorflow import keras

from tensorflow.python.eager import context
from tensorflow.python.framework import common_shapes
from tensorflow.python.framework import dtypes
from tensorflow.python.framework import ops
from tensorflow.python.framework import tensor_shape
from tensorflow.python.keras import activations
#from tensorflow.python.keras import backend as K
from tensorflow.python.keras import constraints
from tensorflow.python.keras import initializers
from tensorflow.python.keras import regularizers
#from tensorflow.python.keras.engine.base_layer import Layer
from tensorflow.python.keras.engine.input_spec import InputSpec
from tensorflow.python.keras.utils import conv_utils
from tensorflow.python.keras.utils import generic_utils
from tensorflow.python.keras.utils import tf_utils
from tensorflow.python.ops import array_ops
from tensorflow.python.ops import gen_math_ops
from tensorflow.python.ops import math_ops
from tensorflow.python.ops import nn
from tensorflow.python.ops import nn_ops
from tensorflow.python.ops import standard_ops
from tensorflow.python.ops import variable_scope
from tensorflow.python.util import nest
from tensorflow.python.util import tf_inspect
from tensorflow.python.util.tf_export import keras_export

class Max(Layer):
    def call(self, inputs):
        return tf.maximum(inputs[0], inputs[1])

class Norm2(Layer):
    def call(self, inputs):
        x1, x2 = inputs
#        return tf.pow((x1*x1+x2*x2), 1/2)
        return tf.pow((x1*x1*x1*x1*x1*x1*x1*x1*x1*x1*x1*x1*x1*x1*x1*x1*x1*x1*x1*x1*x1*x1 + x2*x2*x2*x2*x2*x2*x2*x2*x2*x2*x2*x2*x2*x2*x2*x2*x2*x2*x2*x2*x2*x2), 1/22)

def MaxTensor(t):
    return tf.to_int32(K.max(t))

class InputToSparse(Layer):
    def call(self, inputs):
        idx, val, shape = inputs
        B, N, E = [MaxTensor(x) for x in shape]
      
        a, b = idx, val
        a = batch_to_axis(a, B, E)
        a = keep_axis(a, -1, func=tf.not_equal, idx=1)
        a = tf.cast(a, tf.int64)

        b = to_2d(b)
        b = keep_axis(b, -1, func=tf.not_equal)
        b = to_1d(b)
        sparse = tf.SparseTensor(a, b, [B, N, N])
        return sparse

class ToAdj(Layer):
    def call(self, inputs):
        edges, shape = inputs
        B, N, E = [MaxTensor(x) for x in shape]
        def map_function(x):
            i = x[0]
            sparse_slice = tf.sparse.reshape(tf.sparse.slice(
                edges, [i, 0, 0], [1, edges.dense_shape[1], edges.dense_shape[2]]),
                [edges.dense_shape[1], edges.dense_shape[2]])
            pad = pad_up_to(sparse_slice.indices, [E, 2], -1)
            pad = tf.cast(pad, tf.float32)
            pad = elem_to_batch(pad, N)
            pad = tf.reshape(pad, (edges.dense_shape[1], 30))
            return pad

        elems = (tf.range(0, edges.dense_shape[0], delta=1, dtype=tf.int64),)
        return tf.map_fn(map_function, elems, dtype=tf.float32, back_prop=True)

class SparseGraphMaxPool(Layer):
    def __init__(self, k, **kwargs):
        self.k = k
        super().__init__(**kwargs)

    def get_config(self):
        config = {
            'k': self.k,
        }
        base_config = super().get_config()
        return dict(list(base_config.items()) + list(config.items()))

    def call(self, inputs):
        features, adj, shape = inputs
        B, N, E = [MaxTensor(x) for x in shape]
        C = adj.get_shape().as_list()[2]

        Z = tf.zeros_like(features[:,0:1,:])
        features = tf.concat([features, Z], axis=1)

        edges = tf.mod(tf.cast(adj, tf.int32), N + 1)
        edges = batch_to_elem(edges, B, N, C)
        for i in range(self.k):
           features = tf.gather_nd(features, edges, batch_dims=0)
           features = K.max(features, axis=2)
           if i != self.k - 1:
               features = tf.concat([features, Z], axis=1)

        return features

def omit(tensor, num, func=tf.equal, const=0):
    c = K.ones_like(tensor) * const
    z = tf.where(func(tensor, num), c, tensor)
    return z

def keep_axis(tensor, num, func=tf.equal, idx=0):
    t = tensor[:, idx]
    t = tf.where(func(t, num))
    t = tf.gather_nd(tensor, t)
    return t

class ToFirst(Layer):
    def call(self, inputs):
        return inputs[:,0:1]
class Temp(Layer):
    def call(self, inputs):
        return inputs / 1

class ToOmits(Layer):
    def call(self, inputs):
      return inputs[0]

class ToSparse(Layer):
    def __init__(self, shape, **kwargs):
        self.shape = tf.cast(shape, tf.int64)
        super().__init__(**kwargs)

    def call(self, edges):
        idx = tf.where(tf.not_equal(edges, 0))
        sparse = tf.SparseTensor(idx, tf.gather_nd(edges, idx), self.shape)#(self.B, self.N, self.N))
        return sparse

def pad_up_to(t, max_in_dims, constant_values):
    s = tf.shape(t)
    paddings = [[0, m-s[i]] for (i,m) in enumerate(max_in_dims)]
    return tf.pad(t, paddings, 'CONSTANT', constant_values=constant_values)


def elem_to_batch(x, R):
    def map_function(y):
      i = y[0]
      first, last = x[:,0], x[:,1:]

      pad = tf.cast(first, tf.float32)
      pad = tf.where(tf.equal(pad, i))
      pad = tf.gather_nd(last, pad)
      pad = pad_up_to(pad, [30, 1], -1)
      pad = pad[:, 0]
      return tf.cast(pad, tf.float32)

    D = tf.cast(tf.range(0, R, delta=1), tf.float32)
    elems = (D,)
    return tf.map_fn(map_function, elems, dtype=tf.float32, back_prop=True)

def batch_to_elem(x, B, R, C):
    x = tf.cast(x, tf.int32)
    x = tf.expand_dims(x, axis=3)
    batch_idx = tf.reshape(tf.range(3000), (-1,1))[:B]
    batch_idx = tf.tile(batch_idx, (1, R*C))
    batch_idx = tf.reshape(batch_idx, (B, R, C, 1))
    x = tf.concat([batch_idx, x], axis=3)
    return x

def batch_to_axis(x, B, leng):
    x = tf.cast(x, tf.int32)
    R = leng
    C = x.get_shape().as_list()[2]
    x = tf.reshape(x, [B, R, C])
    batch_idx = tf.reshape(tf.range(3000), (-1,1))[:B]
    batch_idx = tf.tile(batch_idx, (1, R))
    batch_idx = tf.reshape(batch_idx, (B, R, 1))
    x = tf.concat([batch_idx, x], axis=2)
    x = to_2d(x)
    return x

def to_2d(x):
    C = x.get_shape().as_list()[2]
    x = tf.reshape(x, (-1, C))
    return x

def to_1d(x):
    return tf.reshape(x, (-1,))

class GraphLayer(Layer):
    """base layer"""
    def __init__(self,
                 activation=None,
                 **kwargs):
        """Initialize the layer.
        :param step_num: Two nodes are considered as connected if they could be reached in `step_num` steps.
        :param activation: The activation function after convolution.
        :param kwargs: Other arguments for parent class.
        """
        self.activation = keras.activations.get(activation)
        super(GraphLayer, self).__init__(**kwargs)

    def get_config(self):
        config = {
            'activation': self.activation,
        }
        base_config = super(GraphLayer, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))

    def call(self, inputs, **kwargs):
        features, edges = inputs
        input_shape = tf.shape(features)
        edges = K.cast(edges, K.floatx())
        outputs = self.activation(self._call(features, edges))
        return outputs

    def _call(self, features, edges):
        raise NotImplementedError('The class is not intended to be used directly.')


class D2(Layer):
  def __init__(self, **kwargs):
     super().__init__(**kwargs)

  def call(self, features):
      features = K.permute_dimensions(features,(0, 2, 1))
      z = tf.norm(features, axis=2, ord=2, keepdims=False)
      return z
         
class TopK(Layer):
    def __init__(self, k, **kwargs):
       self.k = k
       super().__init__(**kwargs)

    def get_config(self):
        config = {
            'k': self.k,
        }
        base_config = super().get_config()
        return dict(list(base_config.items()) + list(config.items()))

    def call(self, features):
        node_num = K.shape(features)[1]
        Z = tf.zeros_like(features[:,0:1,:])
        features = tf.concat([features, Z], axis=1)
        features = K.permute_dimensions(features,(0, 2, 1))
        z = tf.math.top_k(features, k=self.k, sorted=True)[0]
        return z

class Linear1(Layer):
    def __init__(self, **kwargs):
       super().__init__(**kwargs)

    def call(self, features):
        z = tf.expand_dims(features, axis=2)
        z = Dense(1)(z)
        z = tf.squeeze(z, squeeze_dims=2)
        return z

class StdNormalize(Layer):
    def __init__(self, **kwargs):
       super().__init__(**kwargs)

    def call(self, features):
        z = tf.math.reduce_std(features, axis=1, keepdims=True) + K.epsilon()
        u = tf.math.reduce_mean(features, axis=1, keepdims=True)
        z = (features - u) /z 
        return z

class Max1(Layer):
    def __init__(self, **kwargs):
       super().__init__(**kwargs)

    def call(self, features):
        node_num = K.shape(features)[1]
        Z = K.ones_like(features) * 1

        features = tf.math.minimum(Z, features)
        return features

class TopKIndex(Layer):
    def __init__(self, k, **kwargs):
       self.k = k
       super().__init__(**kwargs)

    def call(self, features):
        node_num = K.shape(features)[1]
        features = K.permute_dimensions(features,(0, 2, 1))
        zeros = K.zeros_like(features)
        z = tf.math.top_k(features, k=self.k, sorted=True)[0]
#        y = tf.reshape(z[:,:,-1], (-1, self.k))
#        y = z[:,:,-1]
#        y = K.permute_dimensions(y,(1,0))
#        print(features.shape)
#        print(z.shape)
#        print(y.shape)
#        yy =z
        z = K.min(z, axis=2, keepdims=True)
#        z= y
        z = tf.math.greater_equal(features, z)
        z = tf.where(z,features,zeros)
        z = K.permute_dimensions(z,(0, 2, 1))
        return z

class LastK(Layer):
    def __init__(self, k, **kwargs):
       self.k = k
       super().__init__(**kwargs)

    def call(self, features):
        node_num = K.shape(features)[1]
        features = K.permute_dimensions(features,(0, 2, 1))
        c1 = tf.math.less_equal(features, [0.])
        z = K.ones_like(features) * 10000000
        features = tf.where(c1, z, features)
        features = features * -1
        z = tf.math.top_k(features, k=self.k)[0]
        z = z * -1 
        c1 = tf.math.equal(z, [10000000])
        Z = K.zeros_like(z)
        z = tf.where(c1, Z, z)
        z = K.permute_dimensions(z,(0, 2, 1))
        return z

class DividedByTop(Layer):
    def __init__(self, k, **kwargs):
       self.k = k
       super().__init__(**kwargs)

    def call(self, features):
        node_num = K.shape(features)[1]
        features = K.permute_dimensions(features,(0, 2, 1))
        z = K.max(features, axis=2,keepdims=True)
#        z = K.(z,(0, 2, 1))
        z = features/(z+0.00001)
        z = K.permute_dimensions(z,(0, 2, 1))
        return z

class NormK(Layer):
    def __init__(self, k, **kwargs):
       self.k = k
       super().__init__(**kwargs)

    def call(self, features):
        node_num = K.shape(features)[1]
        features = K.permute_dimensions(features,(0, 2, 1))
        for i in range(self.k):
          if i == 0:
            z = tf.norm(features, axis=2, ord=1, keepdims=True)
          else:
            zz = tf.norm(features, axis=2, ord=i+1, keepdims=True)
            z = tf.concat([z, zz], 2)

        z = K.permute_dimensions(z,(0, 2, 1))
        return z


class SparseGraphConv(GraphLayer):
    r"""Graph convolutional layer.
    h_i^{(t)} = \sigma \left ( \frac{ G_i^T (h_i^{(t - 1)} W + b)}{\sum G_i}  \right )
    """

    def __init__(self,
                 units,
                 kernel_initializer='glorot_uniform',
                 kernel_regularizer=None,
                 kernel_constraint=None,
                 use_bias=True,
                 bias_initializer='zeros',
                 bias_regularizer=None,
                 bias_constraint=None,
                 **kwargs):
        """Initialize the layer.
        :param units: Number of new states. If the input shape is (batch_size, node_num, feature_len), then the output
                      shape is (batch_size, node_num, units).
        :param kernel_initializer: The initializer of the kernel weight matrix.
        :param kernel_regularizer: The regularizer of the kernel weight matrix.
        :param kernel_constraint:  The constraint of the kernel weight matrix.
        :param use_bias: Whether to use bias term.
        :param bias_initializer: The initializer of the bias vector.
        :param bias_regularizer: The regularizer of the bias vector.
        :param bias_constraint: The constraint of the bias vector.
        :param kwargs: Other arguments for parent class.
        """
        self.units = units
        self.kernel_initializer = tf.keras.initializers.get(kernel_initializer)
        self.kernel_regularizer = tf.keras.regularizers.get(kernel_regularizer)
        self.kernel_constraint = tf.keras.constraints.get(kernel_constraint)
        self.use_bias = use_bias
        self.bias_initializer = tf.keras.initializers.get(bias_initializer)
        self.bias_regularizer = tf.keras.regularizers.get(bias_regularizer)
        self.bias_constraint = tf.keras.constraints.get(bias_constraint)
        self.W, self.b = None, None
        super().__init__(**kwargs)

    def get_config(self):
        config = {
            'units': self.units,
            'kernel_initializer': keras.initializers.serialize(self.kernel_initializer),
            'kernel_regularizer': keras.regularizers.serialize(self.kernel_regularizer),
            'kernel_constraint': keras.constraints.serialize(self.kernel_constraint),
            'use_bias': self.use_bias,
            'bias_initializer': keras.initializers.serialize(self.bias_initializer),
            'bias_regularizer': keras.regularizers.serialize(self.bias_regularizer),
            'bias_constraint': keras.constraints.serialize(self.bias_constraint),
        }
        base_config = super().get_config()
        return dict(list(base_config.items()) + list(config.items()))

    def build(self, input_shape):
        feature_dim = int(input_shape[0][-1])
        self.W = self.add_weight(
            shape=(feature_dim, self.units),
            initializer=self.kernel_initializer,
            regularizer=self.kernel_regularizer,
            constraint=self.kernel_constraint,
            name='{}_W'.format(self.name),
        )
        if self.use_bias:
            self.b = self.add_weight(
                shape=(self.units,),
                initializer=self.bias_initializer,
                regularizer=self.bias_regularizer,
                constraint=self.bias_constraint,
                name='{}_b'.format(self.name),
            )

#    def _call(self, features, edges):
#        features = K.dot(features, self.W)
#        if self.use_bias:
#            features += self.b
#        D = tf.sparse.reduce_sum(edges, axis=2, keepdims=True) + K.epsilon()
#        AD = edges/D
#        DAD = tf.sparse.transpose(tf.sparse.transpose(AD, perm=[0,2,1])/D, perm=[0,2,1])
#        features = sparse_batch_dot(DAD, features)
#        return features
#
    def _call(self, features, edges):
        features = K.dot(features, self.W)
        if self.use_bias:
            features += self.b
        D = tf.sparse.reduce_sum(edges, axis=2, keepdims=True) + K.epsilon()
        AD = edges/D
        features = sparse_batch_dot(AD, features)
        return features

    def compute_output_shape(self, input_shape):
        return (input_shape[0], input_shape[1], self.units)

class SpBatchDot(Layer):
    def call(self, sp):
        return sparse_batch_dot(sp, sp)
    

def sparse_batch_dot(a, b):
    if isinstance(b, tf.sparse.SparseTensor):
        return sparse_sparse_batch_dot(a, b)
    else:
        return sparse_dense_batch_dot(a, b)

def sparse_dense_batch_dot(sp_a, b):
    def map_function(x):
        i, dense_slice = x[0], x[1]
        sparse_slice = tf.sparse.reshape(tf.sparse.slice(
            sp_a, [i, 0, 0], [1, sp_a.dense_shape[1], sp_a.dense_shape[2]]),
            [sp_a.dense_shape[1], sp_a.dense_shape[2]])
        mult_slice = tf.sparse.matmul(sparse_slice, dense_slice)
        return mult_slice

    elems = (tf.range(0, sp_a.dense_shape[0], delta=1, dtype=tf.int64), b)
    return tf.map_fn(map_function, elems, dtype=tf.float32, back_prop=True)

def sparse_sparse_batch_dot(sp_a, sp_b):
    def map_function(x):
        i = x[0]
        sparse_slice = tf.sparse.reshape(tf.sparse.slice(
            sp_a, [i, 0, 0], [1, sp_a.dense_shape[1], sp_a.dense_shape[2]]),
            [sp_a.dense_shape[1], sp_a.dense_shape[2]])
        sparse_slice2 = tf.sparse.reshape(tf.sparse.slice(
            sp_b, [i, 0, 0], [1, sp_b.dense_shape[1], sp_b.dense_shape[2]]),
            [sp_b.dense_shape[1], sp_b.dense_shape[2]])
        dense_slice = tf.sparse.to_dense(sparse_slice2)
        mult_slice = tf.sparse.matmul(sparse_slice, dense_slice)
        return mult_slice

    elems = (tf.range(0, sp_a.dense_shape[0], delta=1), ) 
    return tf.map_fn(map_function, elems, dtype=tf.float32, back_prop=True)


