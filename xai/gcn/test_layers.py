import os
os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"]=""

import tensorflow as tf
import numpy as np
from tensorflow.keras.layers import *

from layers import *

def test_graph_conv():
    input_data = np.array([
        [
            [2, 1, 3],
            [4, 1, 1],
            [3, 2, 1],
            [1, 2, 1],
        ],
        [
            [0, 4, 2],
            [2, 3, 4],
            [0, 0, 0],
            [0, 0, 0],
        ],
    ]).astype('float32')
    input_edge = np.array([
        [
            [0, 1, 0, 0],
            [1, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
        ],
        [
            [1, 1, 0, 0],
            [1, 1, 0, 0],
            [0, 0, 1, 0],
            [0, 0, 0, 0],
        ]
    ]).astype('float32') #+ np.eye(4)
#    input_edge = np.array(
#[
# [
#  [1,2,0],
#  [1,2,0],
#  [1,2,0],
#  [1,2,0],
##  [[1, 0],[1, 1],[0, 2]],
##  [[1, 0],[1, 1],[0, 2]],
# ],
# [
#  [1,2,1],
#  [1,2,0],
#  [1,2,0],
#  [1,2,0],
##  [[0, 0],[0, 1],[0, 2]],
##  [[1, 0],[1, 1],[0, 2]],
# ]])#.astype('int32')
#    print(type(input_edge))
    input_edge = tf.SparseTensor(values=[1.0, 1, 1,1,1,1,1,1], indices=[[0, 0, 0], [0,0,1], [0,1,0], [0,1,1], [0,2,2], [1, 0, 0], [1,0,1], [1,1,0]], dense_shape=[2,4,4])
#    input_edge = tf.SparseTensor(values=[1, 1, 1,1,1,1], indices=[[0, 0, 0], [0,0,1], [0,1,0], [0,1,1], [0,2,2], [0,3,3]], dense_shape=[2,4,4])

#    conv_layer = GraphEdgeTopK(
    input_edge = ToAdj()(input_edge)
    input_edge.set_shape((2,4,10))

#    conv_layer = elem_to_batch(input_edge.indices)
    conv_layer = SparseGraphMaxPool(
        1,
#        kernel_initializer='ones',
#        use_bias=False,
#    )(input_data)
#    conv_layer = keras.layers.GlobalAveragePooling1D()(input_data)
    )([input_data, input_edge])
#    )(input_edge)


    with tf.Session() as sess:
        init = tf.global_variables_initializer()
        sess.run(init)
        print(input_edge.eval())
        print(conv_layer.eval())
#        print(conv_layer.eval())

def test_graph_max_pooling():
    input_data = np.array([
        [
            [0, 4, 2],
            [2, 3, 4],
            [4, 5, 6],
            [7, 7, 8],
        ]
    ]).astype('float32')
    input_edge = np.array([
        [
            [0, 1, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
        ]
    ]).astype('float32') + np.eye(4)
    conv_layer = GraphMaxPool(
        name='GraphMaxPooling',
    )([input_data, input_edge])

    with tf.Session() as sess:
        init = tf.global_variables_initializer()
        sess.run(init)
        print(conv_layer.eval())

def test_graph_average_pooling():
    input_data = np.array([
        [
            [0, 4, 2],
            [2, 3, 4],
            [4, 5, 6],
            [7, 7, 8],
        ]
    ]).astype('float32')
    input_edge = np.array([
        [
            [0, 1, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
        ]
    ]).astype('float32') + np.eye(4)
    conv_layer = GraphAveragePool(
        name='GraphAveragePooling',
    )([input_data, input_edge])

    with tf.Session() as sess:
        init = tf.global_variables_initializer()
        sess.run(init)
        print(conv_layer.eval())

def test_to_sparse():
    input_edge = np.array([
        [
            [0, 1, 0, 0],
            [1, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
        ],
        [
            [1, 1, 0, 0],
            [1, 1, 0, 0],
            [0, 0, 1, 0],
            [0, 0, 0, 0],
        ]
    ]).astype('float32') 

    conv_layer = ToSparse((2,4,4))(input_edge)

    with tf.Session() as sess:
        init = tf.global_variables_initializer()
        sess.run(init)
        print(conv_layer.eval())

def test_sparse_graph_conv():
    input_data = np.array([
        [
            [2, 1, 3],
            [4, 1, 1],
            [3, 2, 1],
            [1, 2, 1],
        ],
        [
            [0, 4, 2],
            [2, 3, 4],
            [0, 0, 0],
            [0, 0, 0],
        ],
    ]).astype('float32')

    input_edge = tf.SparseTensor(values=[1,1,1,1,1,1,1,1], indices=[[0, 0, 0], [0,0,1], [0,1,0], [0,1,1], [0,2,2], [1, 0, 0], [1,0,1], [1,1,0]], dense_shape=[2,4,4])
#    input_edge = tf.SparseTensor(values=[1, 1, 1,1,1,1], indices=[[0, 0, 0], [0,0,1], [0,1,0], [0,1,1], [0,2,2], [0,3,3]], dense_shape=[2,4,4])


    conv_layer = SparseGraphConv(
        3,
        kernel_initializer='ones',
        use_bias=False,
    )([input_data, input_edge])


    with tf.Session() as sess:
        init = tf.global_variables_initializer()
        sess.run(init)
        print(conv_layer.eval())

def test_sparse_batch_dot():
    input_data = np.array([
        [
            [2, 1, 3],
            [4, 1, 1],
            [3, 2, 1],
            [1, 2, 1],
        ],
        [
            [0, 4, 2],
            [2, 3, 4],
            [0, 0, 0],
            [0, 0, 0],
        ],
    ]).astype('float32')

    input_edge = tf.SparseTensor(values=[1.0,1,1,1,1,1,1,1], indices=[[0, 0, 0], [0,0,1], [0,1,0], [0,1,1], [0,2,2], [1, 0, 0], [1,0,1], [1,1,0]], dense_shape=[2,4,4])


    layer1 = sparse_batch_dot(input_edge, input_edge)
    layer2 = sparse_batch_dot(input_edge, input_data)

    with tf.Session() as sess:
        init = tf.global_variables_initializer()
        sess.run(init)
        print(layer1.eval())
        print(layer2.eval())

def test_to_adj():
    lb = tf.constant([[2]])
    ln = tf.constant([[4]])
    le = tf.constant([[9]])
    input_data = np.array([
        [
            [2, 1, 3],
            [4, 1, 1],
            [3, 2, 1],
            [1, 2, 1],
        ],
        [
            [0, 4, 2],
            [2, 3, 4],
            [0, 0, 0],
            [0, 0, 0],
        ],
    ]).astype('float32')
    input_edge = np.array([
        [
            [0, 1, 0, 0],
            [1, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
        ],
        [
            [1, 1, 0, 0],
            [1, 1, 0, 0],
            [0, 0, 1, 0],
            [0, 0, 0, 0],
        ]
    ]).astype('float32')
    input_edge = tf.SparseTensor(values=[1.0, 1, 1,1,1,1,1,1], indices=[[0, 0, 0], [0,0,1], [0,1,0], [0,1,1], [0,2,2], [1, 0, 0], [1,0,1], [1,1,0]], dense_shape=[2,4,4])

    conv_layer = ToAdj()([input_edge, (lb, ln, le)])


    with tf.Session() as sess:
        init = tf.global_variables_initializer()
        sess.run(init)
        print(conv_layer.eval())

def test_sparse_graph_max_pool():
    lb = tf.constant([[2]])
    ln = tf.constant([[4]])
    le = tf.constant([[9]])
    input_data = np.array([
        [
            [2, 1, 3],
            [4, 1, 1],
            [3, 2, 1],
            [1, 2, 1],
        ],
        [
            [0, 4, 2],
            [2, 3, 4],
            [0, 0, 0],
            [0, 0, 0],
        ],
    ]).astype('float32')
    input_edge = tf.SparseTensor(values=[1.0, 1, 1,1,1,1,1,1], indices=[[0, 0, 0], [0,0,1], [0,1,0], [0,1,1], [0,2,2], [1, 0, 0], [1,0,1], [1,1,0]], dense_shape=[2,4,4])
    input_edge = ToAdj()([input_edge,(lb, ln, le)])
    input_edge.set_shape((2, 4, 30))

    conv_layer = SparseGraphMaxPool(
        3,
    )([input_data, input_edge, (lb, ln, le)])


    with tf.Session() as sess:
        init = tf.global_variables_initializer()
        sess.run(init)
        print(input_edge.eval())
        print(conv_layer.eval())

def test_omit():
    input_edge = np.array([
        [
            [1, 0],
            [1, 0],
        ],
        [
            [1, 1],
            [1, 1],
        ]
    ])
    conv_layer = omit(input_edge, 0 , const=-1)

    with tf.Session() as sess:
        init = tf.global_variables_initializer()
        sess.run(init)
        print(conv_layer.eval())

def test_keep_axis():
    input_edge = np.array(
        [
            [1, 0],
            [0, 0],
        ],
    )

    conv_layer = keep_axis(input_edge, 0)

    with tf.Session() as sess:
        init = tf.global_variables_initializer()
        sess.run(init)
        print(conv_layer.eval())


def test_batch_to_elem():
    input_edge = np.array([
        [
            [1, 0],
            [0, 0],
        ],
        [
            [1, 0],
            [0, 1],
        ],
    ])

    conv_layer = batch_to_elem(input_edge)

    with tf.Session() as sess:
        init = tf.global_variables_initializer()
        sess.run(init)
        print(conv_layer.eval())


def test_input_to_sparse():
    leng =[[3],[2]]
    input_data = np.array([
        [
            [1],
            [1],
            [1],
        ],
        [
            [1],
            [1],
            [-1]
        ],
    ])
    input_edge = np.array([
        [
            [0, 0],
            [0, 1],
            [1, 0],
        ],
        [
            [0, 1],
            [1, 0],
            [-1, -1]
        ],
    ])

    conv_layer = InputToSparse()([input_edge, input_data, leng])

    with tf.Session() as sess:
        init = tf.global_variables_initializer()
        sess.run(init)
        print(conv_layer.eval())

def test_sparse_graph_max_pool1():
    leng =[[3],[2]]
    input_data = np.array([
        [
            [1],
            [1],
            [1],
        ],
        [
            [1],
            [1],
            [-1]
        ],
    ])
    input_edge = np.array([
        [
            [0, 0],
            [0, 1],
            [1, 0],
        ],
        [
            [0, 1],
            [1, 0],
            [-1, -1]
        ],
    ])

    conv_layer = SetShape()([input_edge, leng])
#batch_to_axis(input_edge)

    with tf.Session() as sess:
        init = tf.global_variables_initializer()
        sess.run(init)
        print(conv_layer.eval())

test_sparse_graph_max_pool()
#test_to_adj()
#test_sparse_batch_dot()
#test_graph_average_pooling()
#test_graph_max_pooling()
#test_to_sparse()
#test_sparse_graph_conv()
