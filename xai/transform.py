import numpy as np
from scipy.sparse import coo_matrix

def adj_list_to_metrix(adj, num=0, self_loop=True):
    if num == 0:
        for i in adj:
            for j in i:
                num = max(num, j+1)

    if self_loop:
        z = np.eye(num)
    else:
        z = np.zeros((num, num))

    for i, item in enumerate(adj):
        for j in item:
            z[i][j] = 1
    return z

def adj_list_self_loop(adj):
    for idx, i in enumerate(adj):
        i.append(idx)
    return adj

def dense_to_sparse(edge):
    m = coo_matrix(edge)
    idx = np.array(list(zip(m.row, m.col)))
    val = np.expand_dims(np.array(m.data), axis=1)
    return idx, val
    
def two_metrices_to_metrix(m1, m2):
    right_up = np.zeros((m1.shape[0], m2.shape[0]))
    z = np.concatenate((m1, right_up), axis=1)
    zz = np.concatenate((right_up.T, m2), axis=1)
    zzz = np.vstack((z, zz))
    return zzz

def dial_metrix(metrices):
    z = np.zeros(shape=(0,0))
    for i in metrices:
        z = two_metrices_to_metrix(z, i)
    return z

def agg_mols(mols):
    num_mols = len(mols)
    atoms_per_mol = [mol.get_num_atoms() for mol in mols]
    edges = [mol.get_adjacency_list() for mol in mols]
    edges = [adj_list_to_metrix(edge) for edge in edges]
    edges = dial_metrix(edges)
    atom_features = [mol.get_atom_features() for mol in mols]

    z = np.empty(shape=(0, atom_features[0].shape[1]))
    for i in atom_features:
      z = np.vstack((z, i))
    atom_features = z

    d = {}
    d["e"] = np.asarray([edges])
    d["n"] = np.asarray([atom_features])
    return d


def dup_metrix(m):
    i = m[0]
    z = np.zeros(shape=(0,i.shape[-1]))
    for i in m:
      z = np.vstack((z, i))
    return z


#a = np.arange(9).reshape(3,3)
#b = np.arange(16).reshape(4,4)
#print (dial_metrix([a,b,a,b]))
#print (two_metrices_to_metrix(a,b))
#
#input_edge = np.array([
#        [
#            [0., 1., 0., 0.],
#            [0., 0., 0., 0.],
#            [0., 0., 0., 0.],
#            [0., 0., 0., 0.],
#        ],
#        [
#            [0., 1., 0., 0.],
#            [0., 0., 0., 0.],
#            [0., 0., 0., 0.],
#            [0., 0., 0., 0.],
#        ],
#    ])
#
#submetrice_to_metrix(input_edge)
