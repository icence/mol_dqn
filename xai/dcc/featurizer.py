#import deepchem as dc
import numpy as np
import pandas as pd
import tempfile
from rdkit import Chem
from .utils.save import log, DiskDataset
import time
#from .data.data_loader import DataLoader

tasks = ['value']

def convert_df_to_numpy(df, tasks, verbose=False):
  """Transforms a dataframe containing deepchem input into numpy arrays"""
  n_samples = df.shape[0]
  n_tasks = len(tasks)

  time1 = time.time()
  y = np.hstack(
      [np.reshape(np.array(df[task].values), (n_samples, 1)) for task in tasks])
  time2 = time.time()

  w = np.ones((n_samples, n_tasks))
  missing = np.zeros_like(y).astype(int)
  feature_shape = None

  for ind in range(n_samples):
    for task in range(n_tasks):
      if y[ind, task] == "":
        missing[ind, task] = 1

  # ids = df[id_field].values
  # Set missing data to have weight zero
  for ind in range(n_samples):
    for task in range(n_tasks):
      if missing[ind, task]:
        y[ind, task] = 0.
        w[ind, task] = 0.

  return y.astype(float), w.astype(float)

class DataLoader(object):
  """
  Handles loading/featurizing of chemical samples (datapoints).

  Currently knows how to load csv-files/pandas-dataframes/SDF-files. Writes a
  dataframe object to disk as output.
  """

  def __init__(self,
               tasks,
               smiles_field=None,
               id_field=None,
               mol_field=None,
               featurizer=None,
               verbose=True,
               log_every_n=1000):
    """Extracts data from input as Pandas data frame"""
    if not isinstance(tasks, list):
      raise ValueError("tasks must be a list.")
    self.verbose = verbose
    self.tasks = tasks
    self.smiles_field = smiles_field
    if id_field is None:
      self.id_field = smiles_field
    else:
      self.id_field = id_field
    self.mol_field = mol_field
    self.user_specified_features = None
#    if isinstance(featurizer, UserDefinedFeaturizer):
#      self.user_specified_features = featurizer.feature_fields
    self.featurizer = featurizer
    self.log_every_n = log_every_n

  def featurize(self, input_files, data_dir=None, shard_size=8192):
    """Featurize provided files and write to specified location.

    For large datasets, automatically shards into smaller chunks
    for convenience.

    Parameters
    ----------
    input_files: list
      List of input filenames.
    data_dir: str
      (Optional) Directory to store featurized dataset.
    shard_size: int
      (Optional) Number of examples stored in each shard.
    """
    log("Loading raw samples now.", self.verbose)
    log("shard_size: %d" % shard_size, self.verbose)

    if not isinstance(input_files, list):
      input_files = [input_files]

    def shard_generator():
      for shard_num, shard in enumerate(
          self.get_shards(input_files, shard_size)):
        time1 = time.time()
        X, valid_inds = self.featurize_shard(shard)
        ids = shard[self.id_field].values
        ids = ids[valid_inds]
        if len(self.tasks) > 0:
          # Featurize task results iff they exist.
          y, w = convert_df_to_numpy(shard, self.tasks, self.id_field)
          # Filter out examples where featurization failed.
          y, w = (y[valid_inds], w[valid_inds])
          assert len(X) == len(ids) == len(y) == len(w)
        else:
          # For prospective data where results are unknown, it makes
          # no sense to have y values or weights.
          y, w = (None, None)
          assert len(X) == len(ids)

        time2 = time.time()
        log(
            "TIMING: featurizing shard %d took %0.3f s" %
            (shard_num, time2 - time1), self.verbose)
        yield X, y, w, ids

    return DiskDataset.create_dataset(
        shard_generator(), data_dir, self.tasks, verbose=self.verbose)

  def get_shards(self, input_files, shard_size):
    """Stub for children classes."""
    raise NotImplementedError

  def featurize_shard(self, shard):
    """Featurizes a shard of an input dataframe."""
    raise NotImplementedError

def one_of_k_encoding(x, allowable_set):
  if x not in allowable_set:
    raise Exception("input {0} not in allowable set{1}:".format(
        x, allowable_set))
  return list(map(lambda s: x == s, allowable_set))


def one_of_k_encoding_unk(x, allowable_set):
  """Maps inputs not in the allowable set to the last element."""
  if x not in allowable_set:
    x = allowable_set[-1]
  return list(map(lambda s: x == s, allowable_set))


def cumulative_sum(l, offset=0):
  """Returns cumulative sums for set of counts.

  Returns the cumulative sums for a set of counts with the first returned value
  starting at 0. I.e [3,2,4] -> [0, 3, 5, 9]. Keeps final sum for searching.
  Useful for reindexing.

  Parameters
  ----------
  l: list
    List of integers. Typically small counts.
  """
  return np.insert(np.cumsum(l), 0, 0) + offset

def atom_features(atom,
                  bool_id_feat=False,
                  explicit_H=False,
                  use_chirality=False):
  if bool_id_feat:
    return np.array([atom_to_id(atom)])
  else:
    from rdkit import Chem
    results = one_of_k_encoding_unk(
      atom.GetSymbol(),
      [
        'C',
        'N',
        'O',
        'S',
        'F',
        'Si',
        'P',
        'Cl',
        'Br',
        'Mg',
        'Na',
        'Ca',
        'Fe',
        'As',
        'Al',
        'I',
        'B',
        'V',
        'K',
        'Tl',
        'Yb',
        'Sb',
        'Sn',
        'Ag',
        'Pd',
        'Co',
        'Se',
        'Ti',
        'Zn',
        'H',  # H?
        'Li',
        'Ge',
        'Cu',
        'Au',
        'Ni',
        'Cd',
        'In',
        'Mn',
        'Zr',
        'Cr',
        'Pt',
        'Hg',
        'Pb',
        'Unknown'
      ]) \
              + one_of_k_encoding(atom.GetDegree(),
                             [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]) + \
              one_of_k_encoding_unk(atom.GetImplicitValence() + atom.GetNumExplicitHs(), [0, 1, 2, 3, 4, 5, 6]) + \
              [atom.GetFormalCharge(), atom.GetNumRadicalElectrons()] + \
              one_of_k_encoding_unk(atom.GetHybridization(), [
                Chem.rdchem.HybridizationType.SP, Chem.rdchem.HybridizationType.SP2,
                Chem.rdchem.HybridizationType.SP3, Chem.rdchem.HybridizationType.
                                    SP3D, Chem.rdchem.HybridizationType.SP3D2
              ]) + [atom.GetIsAromatic()]
    # In case of explicit hydrogen(QM8, QM9), avoid calling `GetTotalNumHs`
    if not explicit_H:
      results = results + one_of_k_encoding_unk(atom.GetTotalNumHs(),
                                                [0, 1, 2, 3, 4])
    if use_chirality:
      try:
        results = results + one_of_k_encoding_unk(
            atom.GetProp('_CIPCode'),
            ['R', 'S']) + [atom.HasProp('_ChiralityPossible')]
      except:
        results = results + [False, False
                            ] + [atom.HasProp('_ChiralityPossible')]

    return np.array(results)


class ConvMol(object):
  """Holds information about a molecules.

  Resorts order of atoms internally to be in order of increasing degree. Note
  that only heavy atoms (hydrogens excluded) are considered here.
  """

  def __init__(self, atom_features, adj_list, symbols=None, smiles=None, max_deg=10, min_deg=0):
    """
    Parameters
    ----------
    atom_features: np.ndarray
      Has shape (n_atoms, n_feat)
    canon_ad_list: list
      List of length n_atoms, with neighor indices of each atom.
    max_deg: int, optional
      Maximum degree of any atom.
    min_deg: int, optional
      Minimum degree of any atom.
    """

    self.atom_features = atom_features
    self.n_atoms, self.n_feat = atom_features.shape
    self.deg_list = np.array([len(nbrs) for nbrs in adj_list], dtype=np.int32)
    self.canon_adj_list = adj_list
    self.deg_adj_lists = []
    self.deg_slice = []
    self.max_deg = max_deg
    self.min_deg = min_deg
    self.old_to_new=None
    self.symbols=list(symbols)
    self.new_symbols=None
    self.smiles=smiles
    self.membership = self.get_num_atoms() * [0]

    self._deg_sort()

    # Get the degree id list (which corrects for min_deg)
    self.deg_id_list = np.array(self.deg_list) - min_deg

    # Get the size of each degree block
    deg_size = [
        self.get_num_atoms_with_deg(deg)
        for deg in range(self.min_deg, self.max_deg + 1)
    ]

    self.degree_list = []
    for i, deg in enumerate(range(self.min_deg, self.max_deg + 1)):
      self.degree_list.extend([deg] * deg_size[i])

    # Get the the start indices for items in each block
    self.deg_start = cumulative_sum(deg_size)

    # Get the node indices when they are reset when the degree changes
    deg_block_indices = [
        i - self.deg_start[self.deg_list[i]] for i in range(self.n_atoms)
    ]

    # Convert to numpy array
    self.deg_block_indices = np.array(deg_block_indices)

  def get_atoms_with_deg(self, deg):
    """Retrieves atom_features with the specific degree"""
    start_ind = self.deg_slice[deg - self.min_deg, 0]
    size = self.deg_slice[deg - self.min_deg, 1]
    return self.atom_features[start_ind:(start_ind + size), :]

  def get_num_atoms_with_deg(self, deg):
    """Returns the number of atoms with the given degree"""
    return self.deg_slice[deg - self.min_deg, 1]

  def get_num_atoms(self):
    return self.n_atoms

  def _deg_sort(self):
    """Sorts atoms by degree and reorders internal data structures.

    Sort the order of the atom_features by degree, maintaining original order
    whenever two atom_features have the same degree.
    """
    old_ind = range(self.get_num_atoms())
    deg_list = self.deg_list
    new_ind = list(np.lexsort((old_ind, deg_list)))

    num_atoms = self.get_num_atoms()

    # Reorder old atom_features
    self.atom_features = self.atom_features[new_ind, :]

    # Reorder old deg lists
    self.deg_list = [self.deg_list[i] for i in new_ind]

    # Sort membership
    self.membership = [self.membership[i] for i in new_ind]

    # Create old to new dictionary. not exactly intuitive
    old_to_new = dict(zip(new_ind, old_ind))
    self.old_to_new = old_to_new
    new_symbols = [0] * len(self.symbols)
    for i, j in old_to_new.items():
        new_symbols[j] = (self.symbols[i], i)
    self.new_symbols = new_symbols
#    print("eeee",old_to_new)
    # Reorder adjacency lists
    self.canon_adj_list = [self.canon_adj_list[i] for i in new_ind]
    self.canon_adj_list = [[old_to_new[k]
                            for k in self.canon_adj_list[i]]
                           for i in range(len(new_ind))]

    # Get numpy version of degree list for indexing
    deg_array = np.array(self.deg_list)

    # Initialize adj_lists, which supports min_deg = 1 only
    self.deg_adj_lists = (self.max_deg + 1 - self.min_deg) * [0]

    # Parse as deg separated
    for deg in range(self.min_deg, self.max_deg + 1):
      # Get indices corresponding to the current degree
      rng = np.array(range(num_atoms))
      indices = rng[deg_array == deg]

      # Extract and save adjacency list for the current degree
      to_cat = [self.canon_adj_list[i] for i in indices]
      if len(to_cat) > 0:
        adj_list = np.vstack([self.canon_adj_list[i] for i in indices])
        self.deg_adj_lists[deg - self.min_deg] = adj_list

      else:
        self.deg_adj_lists[deg - self.min_deg] = np.zeros(
            [0, deg], dtype=np.int32)

    # Construct the slice information
    deg_slice = np.zeros([self.max_deg + 1 - self.min_deg, 2], dtype=np.int32)

    for deg in range(self.min_deg, self.max_deg + 1):
      if deg == 0:
        deg_size = np.sum(deg_array == deg)
      else:
        deg_size = self.deg_adj_lists[deg - self.min_deg].shape[0]

      deg_slice[deg - self.min_deg, 1] = deg_size
      # Get the cumulative indices after the first index
      if deg > self.min_deg:
        deg_slice[deg - self.min_deg, 0] = (
            deg_slice[deg - self.min_deg - 1, 0] +
            deg_slice[deg - self.min_deg - 1, 1])

    # Set indices with zero sized slices to zero to avoid indexing errors
    deg_slice[:, 0] *= (deg_slice[:, 1] != 0)
    self.deg_slice = deg_slice

  def get_atom_features(self):
    """Returns canonicalized version of atom features.

    Features are sorted by atom degree, with original order maintained when
    degrees are same.
    """
    return self.atom_features

  def get_adjacency_list(self):
    """Returns a canonicalized adjacency list.

    Canonicalized means that the atoms are re-ordered by degree.

    Returns
    -------
    list
      Canonicalized form of adjacency list.
    """
    return self.canon_adj_list

  def get_deg_adjacency_lists(self):
    """Returns adjacency lists grouped by atom degree.

    Returns
    -------
    list
      Has length (max_deg+1-min_deg). The element at position deg is
      itself a list of the neighbor-lists for atoms with degree deg.
    """
    return self.deg_adj_lists

  def get_deg_slice(self):
    """Returns degree-slice tensor.

    The deg_slice tensor allows indexing into a flattened version of the
    molecule's atoms. Assume atoms are sorted in order of degree. Then
    deg_slice[deg][0] is the starting position for atoms of degree deg in
    flattened list, and deg_slice[deg][1] is the number of atoms with degree deg.

    Note deg_slice has shape (max_deg+1-min_deg, 2).

    Returns
    -------
    deg_slice: np.ndarray
      Shape (max_deg+1-min_deg, 2)
    """
    return self.deg_slice

  # TODO(rbharath): Can this be removed?
  @staticmethod
  def get_null_mol(n_feat, max_deg=10, min_deg=0):
    """Constructs a null molecules

    Get one molecule with one atom of each degree, with all the atoms
    connected to themselves, and containing n_feat features.

    Parameters
    ----------
    n_feat : int
        number of features for the nodes in the null molecule
    """
    # Use random insted of zeros to prevent weird issues with summing to zero
    atom_features = np.random.uniform(0, 1, [max_deg + 1 - min_deg, n_feat])
    canon_adj_list = [
        deg * [deg - min_deg] for deg in range(min_deg, max_deg + 1)
    ]

    return ConvMol(atom_features, canon_adj_list)

  @staticmethod
  def agglomerate_mols(mols, max_deg=10, min_deg=0):
    """Concatenates list of ConvMol's into one mol object that can be used to feed
    into tensorflow placeholders. The indexing of the molecules are preseved during the
    combination, but the indexing of the atoms are greatly changed.

    Parameters
    ----
    mols: list
      ConvMol objects to be combined into one molecule."""

    num_mols = len(mols)

    # Results should be sorted by (atom_degree, mol_index)
    atoms_by_deg = np.concatenate([x.atom_features for x in mols])
    degree_vector = np.concatenate([x.degree_list for x in mols], axis=0)
    # Mergesort is a "stable" sort, so the array maintains it's secondary sort of mol_index
    all_atoms = atoms_by_deg[degree_vector.argsort(kind='mergesort')]

    # Sort all atoms by degree.
    # Get the size of each atom list separated by molecule id, then by degree
    mol_deg_sz = [[mol.get_num_atoms_with_deg(deg)
                   for mol in mols]
                  for deg in range(min_deg, max_deg + 1)]

    # Get the final size of each degree block
    deg_sizes = list(map(np.sum, mol_deg_sz))
    # Get the index at which each degree starts, not resetting after each degree
    # And not stopping at any speciic molecule

    deg_start = cumulative_sum_minus_last(deg_sizes)
    # Get the tensorflow object required for slicing (deg x 2) matrix, with the
    # first column telling the start indices of each degree block and the
    # second colum telling the size of each degree block

    # Input for tensorflow
    deg_slice = np.array(list(zip(deg_start, deg_sizes)))

    # Determines the membership (atom i belongs to membership[i] molecule)
    membership = [
        k for deg in range(min_deg, max_deg + 1) for k in range(num_mols)
        for i in range(mol_deg_sz[deg][k])
    ]

    # Get the index at which each deg starts, resetting after each degree
    # (deg x num_mols) matrix describing the start indices when you count up the atoms
    # in the final representation, stopping at each molecule,
    # resetting every time the degree changes
    start_by_deg = np.vstack([cumulative_sum_minus_last(l) for l in mol_deg_sz])

    # Gets the degree resetting block indices for the atoms in each molecule
    # Here, the indices reset when the molecules change, and reset when the
    # degree changes
    deg_block_indices = [mol.deg_block_indices for mol in mols]

    # Get the degree id lookup list. It allows us to search for the degree of a
    # molecule mol_id with corresponding atom mol_atom_id using
    # deg_id_lists[mol_id,mol_atom_id]
    deg_id_lists = [mol.deg_id_list for mol in mols]

    # This is used for convience in the following function (explained below)
    start_per_mol = deg_start[:, np.newaxis] + start_by_deg

    def to_final_id(mol_atom_id, mol_id):
      # Get the degree id (corrected for min_deg) of the considered atom
      deg_id = deg_id_lists[mol_id][mol_atom_id]

      # Return the final index of atom mol_atom_id in molecule mol_id.  Using
      # the degree of this atom, must find the index in the molecule's original
      # degree block corresponding to degree id deg_id (second term), and then
      # calculate which index this degree block ends up in the final
      # representation (first term). The sum of the two is the final indexn
      return start_per_mol[deg_id,
                           mol_id] + deg_block_indices[mol_id][mol_atom_id]

    # Initialize the new degree separated adjacency lists
    deg_adj_lists = [
        np.zeros([deg_sizes[deg], deg], dtype=np.int32)
        for deg in range(min_deg, max_deg + 1)
    ]

    # Update the old adjcency lists with the new atom indices and then combine
    # all together
    for deg in range(min_deg, max_deg + 1):
      row = 0  # Initialize counter
      deg_id = deg - min_deg  # Get corresponding degree id

      # Iterate through all the molecules
      for mol_id in range(num_mols):
        # Get the adjacency lists for this molecule and current degree id
        nbr_list = mols[mol_id].deg_adj_lists[deg_id]

        # Correct all atom indices to the final indices, and then save the
        # results into the new adjacency lists
        for i in range(nbr_list.shape[0]):
          for j in range(nbr_list.shape[1]):
            deg_adj_lists[deg_id][row, j] = to_final_id(nbr_list[i, j], mol_id)

          # Increment once row is done
          row += 1

    # Get the final aggregated molecule
    concat_mol = MultiConvMol(all_atoms, deg_adj_lists, deg_slice, membership,
                              num_mols)
    return concat_mol


def load_csv_files(filenames, shard_size=None, verbose=True):
  """Load data as pandas dataframe."""
  # First line of user-specified CSV *must* be header.
  shard_num = 1
  for filename in filenames:
    if shard_size is None:
      yield pd.read_csv(filename)
    else:
#      log("About to start loading CSV from %s" % filename, verbose)
      for df in pd.read_csv(filename, chunksize=shard_size):
#        log("Loading shard %d of size %s." % (shard_num, str(shard_size)),
#            verbose)
        df = df.replace(np.nan, str(""), regex=True)
        shard_num += 1
        yield df

class CSVLoader(DataLoader):
  """
  Handles loading of CSV files.
  """

  def get_shards(self, input_files, shard_size, verbose=True):
    """Defines a generator which returns data for each shard"""
    return load_csv_files(input_files, shard_size, verbose=verbose)

  def featurize_shard(self, shard):
    """Featurizes a shard of an input dataframe."""
    return featurize_smiles_df(shard, self.featurizer, field=self.smiles_field)
#
#  def atom_features(self, shard):
#    return featurize_smiles_df(shard, self.featurizer, field=self.smiles_field)


class Featurizer(object):
  """
  Abstract class for calculating a set of features for a molecule.

  Child classes implement the _featurize method for calculating features
  for a single molecule.
  """

  def featurize(self, mols, smiles=None, verbose=True, log_every_n=1000):
    """
    Calculate features for molecules.

    Parameters
    ----------
    mols : iterable
        RDKit Mol objects.
    """
    mols = list(mols)
    features = []
    for i, mol in enumerate(mols):
      if mol is not None:
        features.append(self._featurize(mol, smiles))
      else:
        features.append(np.array([]))

    features = np.asarray(features)
    return features

  def _featurize(self, mol, smiles):
    """
    Calculate features for a single molecule.

    Parameters
    ----------
    mol : RDKit Mol
        Molecule.
    """
    raise NotImplementedError('Featurizer is not defined.')

  def __call__(self, mols, smiles=None):
    """
    Calculate features for molecules.

    Parameters
    ----------
    mols : iterable
        RDKit Mol objects.
    """
    return self.featurize(mols, smiles)


class ConvMolFeaturizer(Featurizer):
  name = ['conv_mol']

  def __init__(self, master_atom=False, use_chirality=False,
               atom_properties=[]):
    """
    Parameters
    ----------
    master_atom: Boolean
      if true create a fake atom with bonds to every other atom.
      the initialization is the mean of the other atom features in
      the molecule.  This technique is briefly discussed in
      Neural Message Passing for Quantum Chemistry
      https://arxiv.org/pdf/1704.01212.pdf
    use_chirality: Boolean
      if true then make the resulting atom features aware of the
      chirality of the molecules in question
    atom_properties: list of string or None
      properties in the RDKit Mol object to use as additional
      atom-level features in the larger molecular feature.  If None,
      then no atom-level properties are used.  Properties should be in the
      RDKit mol object should be in the form
      atom XXXXXXXX NAME
      where XXXXXXXX is a zero-padded 8 digit number coresponding to the
      zero-indexed atom index of each atom and NAME is the name of the property
      provided in atom_properties.  So "atom 00000000 sasa" would be the
      name of the molecule level property in mol where the solvent
      accessible surface area of atom 0 would be stored.

    Since ConvMol is an object and not a numpy array, need to set dtype to
    object.
    """
    self.dtype = object
    self.master_atom = master_atom
    self.use_chirality = use_chirality
    self.atom_properties = list(atom_properties)

  def _get_atom_properties(self, atom):
    """
    For a given input RDKit atom return the values of the properties
    requested when initializing the featurize.  See the __init__ of the
    class for a full description of the names of the properties

    Parameters
    ----------
    atom: RDKit.rdchem.Atom
      Atom to get the properties of
    returns a numpy lists of floats of the same size as self.atom_properties
    """
    values = []
    for prop in self.atom_properties:
      mol_prop_name = str("atom %08d %s" % (atom.GetIdx(), prop))
      try:
        values.append(float(atom.GetOwningMol().GetProp(mol_prop_name)))
      except KeyError:
        raise KeyError("No property %s found in %s in %s" %
                       (mol_prop_name, atom.GetOwningMol(), self))
    return np.array(values)

  def _featurize(self, mol, smiles):
    """Encodes mol as a ConvMol object."""
    # Get the node features
#    for a in mol.GetAtoms():
#        print(a.GetSymbol())
    idx_nodes = [(a.GetIdx(), a.GetSymbol(),
                  np.concatenate((atom_features(
                      a, use_chirality=self.use_chirality),
                                  self._get_atom_properties(a))))
                 for a in mol.GetAtoms()]
    idx_nodes.sort()  # Sort by ind to ensure same order as rd_kit
    idx, symbols, nodes = list(zip(*idx_nodes))
    # Stack nodes into an array
    nodes = np.vstack(nodes)
    if self.master_atom:
      master_atom_features = np.expand_dims(np.mean(nodes, axis=0), axis=0)
      nodes = np.concatenate([nodes, master_atom_features], axis=0)

    # Get bond lists with reverse edges included
    edge_list = [
        (b.GetBeginAtomIdx(), b.GetEndAtomIdx()) for b in mol.GetBonds()
    ]

    # Get canonical adjacency list
    canon_adj_list = [[] for mol_id in range(len(nodes))]
    for edge in edge_list:
      canon_adj_list[edge[0]].append(edge[1])
      canon_adj_list[edge[1]].append(edge[0])

    if self.master_atom:
      fake_atom_index = len(nodes) - 1
      for index in range(len(nodes) - 1):
        canon_adj_list[index].append(fake_atom_index)
#    print(smiles)
    return ConvMol(nodes, canon_adj_list, symbols, smiles)


  def feature_length(self):
    return 75 + len(self.atom_properties)

  def __hash__(self):
    atom_properties = tuple(self.atom_properties)
    return hash((self.master_atom, self.use_chirality, atom_properties))

  def __eq__(self, other):
    if not isinstance(self, other.__class__):
      return False
    return self.master_atom == other.master_atom and \
           self.use_chirality == other.use_chirality and \
           tuple(self.atom_properties) == tuple(other.atom_properties)


def featurize_smiles_df(df, featurizer, field, log_every_N=1000, verbose=True):
  """Featurize individual compounds in dataframe.

  Given a featurizer that operates on individual chemical compounds
  or macromolecules, compute & add features for that compound to the
  features dataframe
  """
  sample_elems = df[field].tolist()
#  print(sample_elems)
  features = []
  from rdkit import Chem
  from rdkit.Chem import rdmolfiles
  from rdkit.Chem import rdmolops
  for ind, elem in enumerate(sample_elems):
#    print(elem)
#    print(type(elem))
    mol = Chem.MolFromSmiles(elem)
#    print(dir(mol))
    # TODO (ytz) this is a bandage solution to reorder the atoms so
    # that they're always in the same canonical order. Presumably this
    # should be correctly implemented in the future for graph mols.
#    print("aaaa")
#    print(type(mol))
#    print(dir(mol))
#    print((mol))

#    for a in mol.GetAtoms():
#        print(a.GetSymbol())
#        print(a.GetIdx())

#    print(type(featurizer.featurize([mol])))
#    print("eeeeee")
#    if mol:
#      new_order = rdmolfiles.CanonicalRankAtoms(mol, breakTies=False)
#      print("eeeafawef",list(new_order))
#      mol = rdmolops.RenumberAtoms(mol, new_order)
#      new_order = rdmolfiles.CanonicalRankAtoms(mol)
#      print("eeeafawef",list(new_order))
#    if ind % log_every_N == 0:
#      log("Featurizing sample %d" % ind, verbose)
#    print("aaaaaaaaaaaaaaaa",new_order)
#    print(featurizer.featurize([mol]))
#    for a in mol.GetAtoms():
#        print(a.GetSymbol())
#        print(a.GetIdx())
    features.append(featurizer.featurize([mol], elem))
#  print(features)
  valid_inds = np.array(
      [1 if elt.size > 0 else 0 for elt in features], dtype=bool)
#  print(valid_inds)
  features = [elt for (is_valid, elt) in zip(valid_inds, features) if is_valid]
  return np.squeeze(np.array(features), axis=1), valid_inds



#def make_dataset(path):
#    graph_featurizer = ConvMolFeaturizer()
#    loader = CSVLoader(tasks = tasks, smiles_field = "smiles", id_field = "name", featurizer = graph_featurizer)
#    dataset = loader.featurize(path)
#    splitter = dc.splits.splitters.RandomSplitter()
#    train_inds, valid_inds, test_inds = splitter.split(dataset , seed = 256, frac_train=.8, frac_valid=.1, frac_test=.1)
#
#    train_dir = tempfile.mkdtemp()
#    trainset = dataset.select(train_inds, train_dir)
#
#    test_dir = tempfile.mkdtemp()
#    testset = dataset.select(test_inds, test_dir)
#
#    valid_dir = tempfile.mkdtemp()
#    validset = dataset.select(valid_inds, valid_dir)
#    return trainset, testset, validset
#
#input_file='./data/w.csv'
#trainset, testset, validset = make_dataset(input_file)
#from transform import *
#
#def load_data(dataset):
#    ret_features = []
#    ret_bonds = []
#    ret_y = []
#    batches = dataset.iterbatches(64, pad_batches=False, deterministic=True)
#    for ind, (X_b, y_b, w_b, ids_b) in enumerate(batches):
#        for i in X_b:
#            num_atoms = i.get_atom_features().shape[0]
#            print(i.old_to_new)
#            print(i.symbols)
#            print(i.new_symbols)
#            bonds = adj_list_to_metrix(i.get_adjacency_list(), num_atoms)
#            ret_features.append(i.get_atom_features())
#            ret_bonds.append(bonds)
#
#        for i in y_b:
#            ret_y.append([[i[0]]])
#
#    for i in zip (ret_features, ret_bonds, ret_y):
#        yield np.asarray(i[0]), np.asarray(i[1]), np.asarray(i[2])
#
#for i in load_data(trainset):
#    print("$$$$$$$$$$$$$$$$$$",i)
#    break
