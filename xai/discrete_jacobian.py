import os
os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"]=""
os.environ["AUTOGRAPH_VERBOSITY"]="1"
import tempfile
import tensorflow as tf
import numpy as np
from tensorflow import keras
from tensorflow.keras import backend as K
from .gcn.layers import *
from .reader import *
from .transform import *
from .loss import *
import gc
import sys
from tensorflow.keras.layers import Dense, Flatten
from scipy import stats
np.set_printoptions(threshold=np.inf)
np.set_printoptions(precision=4)
np.set_printoptions(linewidth=np.inf ,formatter={
  'float_kind':'{: .2f}'.format,
  'int_kind': lambda x: (" " + str(x) + "    ")[:5],
  'str_kind': lambda x: (" " + str(x) + "    ")[:5],
})

custom_objects = {
  'SparseGraphConv':SparseGraphConv,
  'SparseGraphMaxPool':SparseGraphMaxPool,
  'TopK':TopK,
  'ToOmits':ToOmits,
  'InputToSparse':InputToSparse,
  'ToAdj':ToAdj,
  'loss':weighted_binary_crossentropy(28, 1),
  'Max':Max
}

def batch_dataset(dataset, batch_size=1):
    for i in batch_load_data(dataset, batch_size, category_size=2):
        yield i[0]
  
def get_symbols(dataset):
    for i in load_data(dataset):
        yield i[3]
  
def get_smiles(dataset):
    for i in load_data(dataset):
        yield i[4]

def rm_one_atom_edges(target, idx, num, val):
    count = 0
    e = idx.tolist()[0]
    ret = []
    for i in e:
        if i[0] != target and i[1] != target: 
            ret.append(i)
    val = to_numpy([[[1.0]]*len(ret)])
    num = to_numpy([len(ret)])
    ret = to_numpy([ret])
    return ret, num, val

def omit_one_atom_edges(f, n, e, num, v):
    zero = np.zeros((1,1,77)).astype('float32')
    for i in range(n):
        up = f[:,0:i,:]
        down = f[:,i+1:,:]
        ret = np.concatenate((up, zero, down), axis=1)
        idx, nu, val = rm_one_atom_edges(i, e, num, v)
        yield ret, idx, nu, val

def omit_one_atom_with_neighbors_edges(f, n, e, num, v):
    zero = np.zeros((1,1,77)).astype('float32')
    ff = f.copy()
    ee = e.tolist()[0]
    for i in range(n):
        for j in range(n):
            if [i, j] in ee:
                ff[0][j] = zero
        idx, nu, val = rm_one_atom_edges(i, e, num, v)
        yield ff, idx, nu, val
        ff = f.copy()

def omit_one_edge_with_neighbors_edges(f, n, e, num, v):
    zero = np.zeros((1,1,77)).astype('float32')
    ff = f.copy()
    adj = to_adj_list(e)
    ee = e.tolist()[0]
    for i in range(n):
        for j in range(n):
            if [i, j] in ee:
                for x in adj[i]:
                    x = int(x)
                    ff[0][x] = zero
                for x in adj[j]:
                    x = int(x)
                    ff[0][x] = zero
                idx, nu, val = rm_one_atom_edges(i, e, num, v)
                idx, nu, val = rm_one_atom_edges(j, idx, nu, val)

                yield ff, idx, nu, val
                ff = f.copy()

def omit_one_atom(f, n):
    zero = np.zeros((1,1,77)).astype('float32')
    for i in range(n):
        up = f[:,0:i,:]
        down = f[:,i+1:,:]
        ret = np.concatenate((up, zero, down), axis=1)
        yield ret

def omit_one_atom_with_neighbors(f, n, e):
    zero = np.zeros((1,1,77)).astype('float32')
    ff = f.copy()
    e = e.tolist()[0]
    for i in range(n):
        for j in range(n):
            if [i, j] in e:
                ff[0][j] = zero
        yield ff
        ff = f.copy()

def omit_one_atom_with_one_neighbor(f, n, e):
    zero = np.zeros((1,1,77)).astype('float32')
    ff = f.copy()
    e = e.tolist()[0]
    for i in range(n):
        for j in range(n):
            if [i, j] in e:
                ff[0][i] = zero
                ff[0][j] = zero
                yield ff
                ff = f.copy()

def omit_one_edge_with_neighbors(f, n, e):
    zero = np.zeros((1,1,77)).astype('float32')
    ff = f.copy()
    adj = to_adj_list(e)
    e = e.tolist()[0]
    for i in range(n):
        for j in range(i, n):
            if [i, j] in e:
                for x in adj[i]:
                    x = int(x)
                    ff[0][x] = zero
                for x in adj[j]:
                    x = int(x)
                    ff[0][x] = zero
                yield ff, (i, j)
                ff = f.copy()

def to_adj_list(e):
    e = e.tolist()[0]
    ret = []
    item = []
    now = 0
    for i in e:
        if i[0] == now:
            item.append(i[1])
        else:
            ret.append(item)
            item = []
            item.append(i[1])
            now += 1
    ret.append(item)
    return ret


def to_edge_atoms(edges, mapping):
    idx = [i[1] for i in mapping]
    dic = {}
    for i, j in enumerate(idx):
        dic[j] = i
    ret = []
    for e in edges:
        ret.append((mapping[dic[e[0]]][0], mapping[dic[e[1]]][0]))
    return ret

def diff_between_omit_and_unomit(new, old):
    old = to_number(old)
    ret = [x - old for x in new]
    old = [old] * len(ret)
    return new, old, ret
  
def to_tensor(x):
    return tf.convert_to_tensor(np.array(x).astype('float32'))

def to_numpy(x):
    return np.array(x).astype('float32')

def to_number(x):
    if isinstance(x, np.ndarray):
        return np.asarray(x).reshape(-1)[0]
    else:
        return x

def to_1d(x):
    return np.asarray(x).reshape(-1)

def abs_diff(x, y, z):
    if x == []:
         return y
    if y == []:
         return x

    if abs(to_number(x) - to_number(z)) - abs(to_number(y) - to_number(z)) > 0:
        return x
    return y

def scale_average_result(x, y, z, zScore=True, autoWeighted=True):
    if zScore:
        x = stats.zscore(x)
        y = stats.zscore(y)
        z = stats.zscore(z)
    if autoWeighted:
        z = z/2

    ret = []
    for i in range(len(x)):
        a = to_number(x[i])
        b = to_number(y[i])
        c = to_number(z[i])
        ret.append((a+b+c)/3)
    return ret

def average_result(x, y, z):
    ret = []
    for i in range(len(x)):
        a = to_number(x[i])
        b = to_number(y[i])
        c = to_number(z[i])
        ret.append((a+b+c)/3)
    return ret

def create_file(input_str):
    with tempfile.NamedTemporaryFile('w+t', delete=False) as fp:
        fp.write('name,smiles,value\n')
        fp.write(input_str + ',' + input_str + ',1\n')
        return fp.name

def delete_file(filename):
    os.unlink(filename)

def cat_list(a, b):
    ret = []
    for i in range(len(a)):
        ret.append(np.concatenate((a[i], b[i])))
    return ret

def repeat_list(a, n):
    ret  = []
    for i in a:
        ret.append(np.repeat(i, repeats=[n,], axis=0))
    return ret

def cat(a, b, axis=0):
    if a == []:
        return b
    return np.concatenate((a, b), axis=axis)

def generate_inputs(mol_features, inputs):
    a = []
    for f in mol_features:
        a.append(f)
        if len(a) == 50:
            yield _generate_inputs(a, inputs)
            a = []
    if a != []:
        yield _generate_inputs(a, inputs)

def _generate_inputs(mol_features, inputs):
    count = len(mol_features)
    a = []
    for f in mol_features:
        a = cat(a, f)
    inputs[0][0] = count
    inputs = repeat_list(inputs, count)
    inputs = [to_tensor(x) for x in inputs]
    inputs[1] = tf.convert_to_tensor(to_numpy(a))
    return inputs

def parse_atoms(sy, input_str):
    a = [x[0] for x in sy]
    z = [x[1] for x in sy]
    
    atoms = [0] * len(a)
    for i, j in zip(z,a):
        atoms[i] = j
    upper_atoms = [x.upper() for x in atoms]
    b = []
    for c in input_str.upper():
       if c in ''.join(upper_atoms) + 'H':
           b.append(c)
    idx = []
    i = 0
    len2 = 0
    last_len2 = 0
    for f in range(len(b)):
        if last_len2 != len2:
           last_len2 = len2
           continue
        if len(upper_atoms[i]) == 1 and b[f] == upper_atoms[i]:
            idx.append(f-len2)
            i += 1
        elif len(upper_atoms[i]) == 2 and (b[f] + b[f+1]) == upper_atoms[i]:
            idx.append(f-len2)
            len2 += 1
            i += 1

    assert len(a) == len(idx)
    z = [idx[x] for x in z]
    return atoms, z

model_herg_str = './xai/models/herg2-ep71-loss0.19-acc0.96-auc0.99-tp25-fp21-tn466-fn0-vloss0.65-vacc0.93-auc0.95-vtp2-vfp3-vtn35-vfn0.h5'
model_mu_str = './xai/models/mutagenicity-ep35-loss0.26-acc0.89-auc0.96-tp2798-fp419-tn2122-fn189-vloss0.69-vacc0.77-auc0.84-vtp132-vfp43-vtn113-vfn32.h5'
model_ca_str = './xai/models/carcinogens-ep98-loss0.11-acc0.97-auc0.99-tp546-fp11-tn360-fn19-vloss0.97-vacc0.85-auc0.87-vtp21-vfp4-vtn13-vfn2.h5'
model_cy_str = './xai/models/cytotoxicity-ep78-loss0.06-acc0.99-auc0.99-tp2592-fp63-tn2545-fn4-vloss0.98-vacc0.86-auc0.84-vtp537-vfp79-vtn575-vfn109.h5'
def predict(model_type, input_str, input_type='smiles'):
    sess = K.get_session()

    if model_type == 'herg':
        model = keras.models.load_model(model_herg_str, custom_objects=custom_objects)
    elif model_type == 'mutagenicity':
        model = keras.models.load_model(model_mu_str, custom_objects=custom_objects)
    elif model_type == 'carcinogens':
        model = keras.models.load_model(model_ca_str, custom_objects=custom_objects)
    elif model_type == 'cytotoxicity':
        model = keras.models.load_model(model_ca_str, custom_objects=custom_objects)
    else:
        return None
#    model.summary()
#    print(model.loss)
#    print(model.optimizer)
    if input_type == 'smiles':
        inputfile = create_file(input_str)
#    elif input_type == 'file':
#        inputfile = input_str
    else:
        return None

    trainset, testset, validset = make_dataset(inputfile, f1=1.)
    delete_file(inputfile)
 
    data = batch_dataset(trainset)
    symbol = get_symbols(trainset)
    smiles = get_smiles(trainset)
    for i in data:
        j = next(symbol)
        k = next(smiles)
        np_features = to_numpy(i[1])
        np_edge_idx = to_numpy(i[3])
        np_edge_num = to_numpy(i[4])
        np_edge_val = to_numpy(i[5])

        sy = j.new_symbols
        n = len(sy)
        
        candidates = [i[1]]
        for f in omit_one_atom(np_features, n):
            candidates.append(f)
        for f in omit_one_atom_with_neighbors(np_features, n, np_edge_idx):
            candidates.append(f)
        ret_edge = []
        for f, e in omit_one_edge_with_neighbors(np_features, n, np_edge_idx):
            candidates.append(f)
            ret_edge.append(e)

        result = []
        with sess.as_default() as s:
            for f in generate_inputs(candidates, i):
                 result = cat(result, to_1d(s.run(model(f))), -1)

        old_result = result[0:1]
        ret0 = result[1:n+1]
        ret1 = result[n+1:2*n+1]
        ret2 = result[2*n+1:]
        ret_temp = [[]] * n
        for x in range(len(ret_edge)):
            p, q = ret_edge[x]
            r = ret2[x]
            ret_temp[p] = abs_diff(ret_temp[p], r, old_result)
            ret_temp[q] = abs_diff(ret_temp[q], r, old_result)
        ret2 = ret_temp
#        print(to_numpy(ret0))
#        print(to_numpy(ret1))
#        print(to_numpy(ret2))
#        break
        new_result = scale_average_result(ret0, ret1, ret2)
        atoms, indices = parse_atoms(sy, input_str)
        new, old, new_minus_old = diff_between_omit_and_unomit(new_result, old_result)
        response = []
        for atom in zip(atoms, indices, new, old, new_minus_old):
            response.append(to_response_format(atom))
    K.clear_session()
    del model
    gc.collect()
    return response

def to_response_format(atom):
    return {'atom':atom[0], 'id':atom[1], 'new':round(atom[2], 3), 'old':round(atom[3], 3), 'new_minus_old':round(atom[4], 3), 'old_minus_new':round(-atom[4], 3)}

#import numpy as np
#import json
#class NpEncoder(json.JSONEncoder):
#    def default(self, obj):
#        if isinstance(obj, np.integer):
#            return int(obj)
#        elif isinstance(obj, np.floating):
#            return float(obj)
#        elif isinstance(obj, np.ndarray):
#            return obj.tolist()
#        else:
#            return super(NpEncoder, self).default(obj)
#
#
#if __name__ == '__main__':
##    print(predict('herg', 'C1CN(CCC1NC2=NC3=CC=CC=C3N2CC4=CC=C(C=C4)F)CCC5=CC=C(C=C5)O'))
##    a = (predict('herg', 'CC1=C(C)C2=C(CC[C@@](C)(CCC[C@H](C)CCC[C@H](C)CCCC(C)C)O2)C(C)=C1O'))
#    a = (predict('herg', 'CCN(CC)CCCC(C)NC1=CC=NC2=CC(Cl)=CC=C12'))
##    a = (predict('herg', 'O=C1C2=C(C=CC=C2O)CC2=CC=CC(O)=C12'))
##    a = (predict('herg', 'NC1=CC=NC=C1N'))
##    a = predict('herg', 'C1CN(CCC1NC2=NC3=CC=CC=C3N2CC4=CC=C(C=C4)F)CCC5=CC=C(C=C5)O')
##    for element in a:
#        element.pop('new_minus_old', None)
#        element.pop('old', None)
#        element.pop('new', None)
#        element.pop('atom', None)
#    print(json.dumps(a, sort_keys=True, indent=2, cls=NpEncoder))
