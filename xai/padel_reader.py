from padelpy import from_smiles
import numpy as np
from .fp import *

def FP(smiles):
  z = A.get(smiles)
  if type(z) == list:
    return z
  else:
    return [0]*881
  


def OLDFP(smiles):
#  print('ss', smiles)
  desc_fp = from_smiles(smiles, fingerprints=True)
  b = []
  for k,v in desc_fp.items():
    if (str.startswith(k, 'PubchemFP')):
      b.append(int(v))
#  return np.array(b)
  return b

#FP('[O-][N+](=O)c1ccc(Cl)cc1[O-][N+](=O)c1ccc(Cl)cc11')
#with open("data/pad.csv", mode='r') as f:
#  for i in f:
#    try:
##      print("'", i, "':", FP(i), ',')
#      s = "'{}':{},".format(i[:-1],FP(i))
#      print(s)
#    except KeyboardInterrupt:
#      break
#    except:
#      print("'", i, "':", "exception", ',')
