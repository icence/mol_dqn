import tensorflow as tf
import numpy as np
import tempfile
from .transform import *
from .dcc.featurizer import *
from .dcc.splitters import RandomSplitter
from .padel_reader import *
from rdkit import Chem
from scipy.sparse import csr_matrix
tasks = ['value']

def make_dataset(path, f1=.88):
    f2 = 0.1
    f3 = 1-f1-f2
    graph_featurizer = ConvMolFeaturizer()
    loader = CSVLoader(tasks = tasks, smiles_field = "smiles", id_field = "name", featurizer = graph_featurizer)
    dataset = loader.featurize(path)
    splitter = RandomSplitter()
    train_inds, valid_inds, test_inds = splitter.split(dataset , seed = 256, frac_train=f1, frac_valid=f2, frac_test=f3)

    train_dir = tempfile.mkdtemp()
    trainset = dataset.select(train_inds, train_dir)

    test_dir = tempfile.mkdtemp()
    testset = dataset.select(test_inds, test_dir)

    valid_dir = tempfile.mkdtemp()
    validset = dataset.select(valid_inds, valid_dir)
    return trainset, testset, validset

def is_in_ring(mol):
    indices = [x[1].item() for x in mol.new_symbols]
    smiles = mol.smiles
    m = Chem.MolFromSmiles(smiles)
    ret = [[int(m.GetAtomWithIdx(x).IsInRing()), int(m.GetAtomWithIdx(x).IsInRingSize(6))] for x in indices]
    return np.array(ret)

def count_pos(ret_y):
    a = {0:0, 1:0}
    for i in ret_y:
        k = int(i[0][0])
        a[k] = a[k] + 1
    return a

def duplicate(L, pos, neg):
    if (pos > 1) :
        target = 1.0
    elif (neg > 1):
        target = 0.0
    else:
        return L

    dup = max(pos, neg)

    ret = []
    for i in range(len(L)):
        ret.append([])

    for i in zip(*L):
        for j in range(len(ret)):
            ret[j].append(i[j])
            if i[2][0][0] == target:
                for k in range(dup - 1):
                    ret[j].append(i[j])

    return ret


def shuffle(L, n):
    for i in range(n):
        for j in range(len(L)):
            a = L[j]
            even = a[::2]
            odd = a[1::2]
            L[j] = even + odd
    return L


def load_data(dataset, balance=True):
    ret_features = []
    ret_bonds = []
    ret_idx = []
    ret_val = []
    ret_y = []
    ret_atoms = []
    ret_smiles = []
    ret_adj = []
    ret_nums = []
    batches = dataset.iterbatches(64, pad_batches=False, deterministic=True)
    a = 0
    for ind, (X_b, y_b, w_b, ids_b) in enumerate(batches):
        for i in X_b:
            num_atoms = i.get_atom_features().shape[0]
            bonds = i.get_adjacency_list()
            bonds = adj_list_to_metrix(i.get_adjacency_list(), num_atoms)
            idx, val = dense_to_sparse(bonds)
            features = i.get_atom_features()
            ring = is_in_ring(i)
            features = np.concatenate((features, ring), axis=1)
            ret_features.append(features)
            ret_idx.append(idx)
            ret_val.append(val)
            ret_atoms.append(i)
            ret_smiles.append(i.smiles)
            ret_adj.append(i.get_adjacency_list())
            ret_nums.append(num_atoms)
        for i in y_b:
            ret_y.append([[i[0]]])

    Array = [ret_features, ret_idx, ret_y, ret_atoms, ret_smiles, ret_adj, ret_nums, ret_val]

    if balance:
        dic = count_pos(ret_y)
        if dic[0] != 0 and dic[1] != 0:
            pos = dic[0]//dic[1]
            neg = dic[1]//dic[0]
            Array = duplicate(Array, pos, neg)
    Array = shuffle(Array, 3)
    dic = count_pos(Array[2])
#    print("The data count: ",dic)
    for i in zip (*Array):
        yield np.asarray(i[0]), np.asarray(i[1]), np.asarray(i[2]), i[3], i[4], i[5], i[6], np.asarray(i[7])


def align(f, const=0):
    r = 0
    c = 0
    for i in f:
      r = max(i.shape[0], r)
      c = max(i.shape[1], c)
    z = np.ones((r, c)) * const
    ret = []
    align_num = []
    for i in f:
      a = np.vstack((i, z))[:r, :c]
      ret.append(a)
      align_num.append(r)
    return ret, align_num


def batch_load_data(dataset, batch_size=2, fill_batch=False, category_size=2, balance=True):
    Features = []
    Idx = []
    Y = []
    Fingerprint = []
    Number = []
    Val = []
    for elem in load_data(dataset, balance=balance):
        f = elem[0]
        Features.append(f)

        idx = elem[1]
        Idx.append(idx)

        y = elem[2]
        if category_size > 2:
            y = tf.keras.utils.to_categorical(np.asarray(y), category_size)
        else:
            y = np.asarray(y)
        Y.append(y)

        smiles = elem[4]
#        fingerprint = np.array(FP(smiles))
        fingerprint = np.array([0]*881)
        Fingerprint.append(fingerprint)

        number = elem[6]
        number = np.array([number])
        Number.append(number)

        val = elem[7]
        val = np.array(val)
        Val.append(val)

        if len(Features) == batch_size:
            Features, AlignAtomNum = align(Features)
            Idx, AlignIdxNum = align(Idx, -1)
            Val, _ = align(Val, -1)
            Batch = [np.array([batch_size])] * batch_size
            if category_size > 2:
                yield [Batch, Features, AlignAtomNum, Idx, AlignIdxNum, Val, Number, Fingerprint], np.asarray(Y).reshape(batch_size, category_size)
            else:
                yield [Batch, Features, AlignAtomNum, Idx, AlignIdxNum, Val, Number, Fingerprint], np.asarray(Y).reshape(-1)
            Features, Idx, Val, Number, Fingerprint, Y = [], [], [], [], [], []

    if fill_batch and len(Features) != 0:
#        print("The last loop: " + str(len(Features)))

        batch_size = len(Features)
        if len(Features) == batch_size:
            Features, AlignAtomNum = align(Features)
            Idx, AlignIdxNum = align(Idx, -1)
            Val, _ = align(Val, -1)
            Batch = [np.array([batch_size])] * batch_size
            if category_size > 2:
                yield [Batch, Features, AlignAtomNum, Idx, AlignIdxNum, Val, Number, Fingerprint], np.asarray(Y).reshape(batch_size, category_size)
            else:
                yield [Batch, Features, AlignAtomNum, Idx, AlignIdxNum, Val, Number, Fingerprint], np.asarray(Y).reshape(-1)
            Features, Idx, Val, Number, Fingerprint, Y = [], [], [], [], [], []

#trainset, testset, validset = make_dataset('./data/herg.csv')
#trainset, testset, validset = make_dataset('./data/mutagenicity2.csv')
#n = 0
#for i in batch_load_data(trainset, batch_size=3, fill_batch=True):
#    n = n+1
#    print(n)
#    print("$$$$$$", i[0])
#    print("$$$$$$",i[0][1][0].shape)
#    print("$$$$$$",i[1].shape)
#    print("$$$$$$",type(i[0][0][0]))
#    print("$$$$$$",i[0][0][1].shape)
#    print("$$$$$$",i[3].smiles)
#    print("$$$$$$",type(i[3]))
#    break

def t_load_data(dataset, balance=False):
    ret_features = []
    ret_bonds = []
    ret_idx = []
    ret_val = []
    ret_y = []
    ret_atoms = []
    ret_smiles = []
    ret_adj = []
    ret_nums = []
    batches = dataset.iterbatches(64, pad_batches=False, deterministic=True)
    a = 0
    for ind, (X_b, y_b, w_b, ids_b) in enumerate(batches):
        for i in X_b:
            num_atoms = i.get_atom_features().shape[0]
            bonds = i.get_adjacency_list()
            bonds = adj_list_to_metrix(i.get_adjacency_list(), num_atoms)
            idx, val = dense_to_sparse(bonds)
            features = i.get_atom_features()
            ring = is_in_ring(i)
            features = np.concatenate((features, ring), axis=1)
            ret_features.append(features)
            ret_idx.append(idx)
            ret_val.append(val)
            ret_atoms.append(i)
            ret_smiles.append(i.smiles)
            ret_adj.append(i.get_adjacency_list())
            ret_nums.append(num_atoms)
        for i in y_b:
            ret_y.append([[i[0]]])

    Array = [ret_features, ret_idx, ret_y, ret_atoms, ret_smiles, ret_adj, ret_nums, ret_val]

    if balance:
        dic = count_pos(ret_y)
        if dic[0] != 0 and dic[1] != 0:
            pos = dic[0]//dic[1]
            neg = dic[1]//dic[0]
            Array = duplicate(Array, pos, neg)
    Array = shuffle(Array, 3)
    dic = count_pos(Array[2])
#    print("The data count: ",dic)
    b = []
    for i in zip (*Array):
        b.append(i)
#        yield np.asarray(i[0]), np.asarray(i[1]), np.asarray(i[2]), i[3], i[4], i[5], i[6], np.asarray(i[7])
#        yield [[np.asarray(i[0]),]], np.asarray(i[2])
        if len(b) == 2:
#           return [[np.asarray(b[0][0]), np.asarray(b[1][0])], [np.asarray(b[0][1]), np.asarray(b[1][1])]], [1, 1]
           return [[np.asarray(b[0][0]), np.asarray(b[0][0])], [np.asarray(b[0][1]), np.asarray(b[0][1])] ], [1,1]
#           return [[np.asarray(b[0][0]), np.asarray(b[0][0])], [np.asarray(b[0][0]), np.asarray(b[0][0])]], [1,1]
           return [(np.asarray(b[0][0]), np.asarray(b[0][1]))], [1]
           return [(np.asarray(b[0][0]), np.asarray(b[0][1])), (np.asarray(b[1][0]), np.asarray(b[1][1]))], [1, 1]
           b = []

##trainset, testset, validset = make_dataset('./data/herg.csv')
##n = 0
##for i in batch_load_data(trainset, batch_size=3, fill_batch=True):
##    n = n+1
##    print(n)
##    print("$$$$$$", i[0])
##    print("$$$$$$",i[0][0])
##    print("$$$$$$",type(i[0][0][0]))
##    print("$$$$$$",i[0][0][1].shape)
##    print("$$$$$$",i[3].smiles)
##    print("$$$$$$",type(i[3]))
##    break
#trainset, testset, validset = make_dataset('./data/mutagenicity.csv')
#for i in t_load_data(testset, balance=False):
#     print(i[0][0][0].shape)
#     print(i[0][0][1].shape)
#     print(i[1].shape)
#     print(i[1])
#     break

#import sys
#import deepchem
#print (sys.modules['deepchem.split'])
