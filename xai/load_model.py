import os
os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"]=""
os.environ["AUTOGRAPH_VERBOSITY"]="1"
import tempfile
import tensorflow as tf
import numpy as np
from tensorflow import keras
from tensorflow.keras import backend as K
from .gcn.layers import *
from .reader import *
from .transform import *
from .loss import *
import gc
import sys
from tensorflow.keras.layers import Dense, Flatten
from scipy import stats
np.set_printoptions(threshold=np.inf)
np.set_printoptions(precision=4)
np.set_printoptions(linewidth=np.inf ,formatter={
  'float_kind':'{: .2f}'.format,
  'int_kind': lambda x: (" " + str(x) + "    ")[:5],
  'str_kind': lambda x: (" " + str(x) + "    ")[:5],
})

custom_objects = {
  'SparseGraphConv':SparseGraphConv,
  'SparseGraphMaxPool':SparseGraphMaxPool,
  'TopK':TopK,
  'Temp':Temp,
  'ToFirst':ToFirst,
  'ToOmits':ToOmits,
  'InputToSparse':InputToSparse,
  'ToAdj':ToAdj,
  'loss':weighted_binary_crossentropy(28, 1),
  'Max':Max
}

def batch_dataset(dataset, batch_size=1):
    for i in batch_load_data(dataset, batch_size, category_size=2):
        yield i[0]
  
def get_symbols(dataset):
    for i in load_data(dataset):
        yield i[3]
  
def get_smiles(dataset):
    for i in load_data(dataset):
        yield i[4]

def diff_between_omit_and_unomit(new, old):
    old = to_number(old)
    ret = [x - old for x in new]
    old = [old] * len(ret)
    return new, old, ret
  
def to_tensor(x):
    return tf.convert_to_tensor(np.array(x).astype('float32'))

def to_numpy(x):
    return np.array(x).astype('float32')

def to_number(x):
    if isinstance(x, np.ndarray):
        return np.asarray(x).reshape(-1)[0]
    else:
        return x

def to_1d(x):
    return np.asarray(x).reshape(-1)

def abs_diff(x, y, z):
    if x == []:
         return y
    if y == []:
         return x

    if abs(to_number(x) - to_number(z)) - abs(to_number(y) - to_number(z)) > 0:
        return x
    return y

def create_file(input_str):
    with tempfile.NamedTemporaryFile('w+t', delete=False) as fp:
        fp.write('name,smiles,value\n')
        fp.write(input_str + ',' + input_str + ',1\n')
        return fp.name

def delete_file(filename):
    os.unlink(filename)

def repeat_list(a, n):
    ret  = []
    for i in a:
        ret.append(np.repeat(i, repeats=[n,], axis=0))
    return ret
def cat(a, b, axis=0):
    if a == []:
        return b
    return np.concatenate((a, b), axis=axis)

def generate_inputs(mol_features, inputs):
    a = []
    for f in mol_features:
        a.append(f)
        if len(a) == 50:
            yield _generate_inputs(a, inputs)
            a = []
    if a != []:
        yield _generate_inputs(a, inputs)

def _generate_inputs(mol_features, inputs):
    count = len(mol_features)
    a = []
    for f in mol_features:
        a = cat(a, f)
    inputs[0][0] = count
    inputs = repeat_list(inputs, count)
    inputs = [to_tensor(x) for x in inputs]
    inputs[1] = tf.convert_to_tensor(to_numpy(a))
    return inputs

model_herg_str = './xai/models/herg2-ep71-loss0.19-acc0.96-auc0.99-tp25-fp21-tn466-fn0-vloss0.65-vacc0.93-auc0.95-vtp2-vfp3-vtn35-vfn0.h5'
model_mu_str = './xai/models/mutagenicity-ep35-loss0.26-acc0.89-auc0.96-tp2798-fp419-tn2122-fn189-vloss0.69-vacc0.77-auc0.84-vtp132-vfp43-vtn113-vfn32.h5'
model_ca_str = './xai/models/carcinogens-ep98-loss0.11-acc0.97-auc0.99-tp546-fp11-tn360-fn19-vloss0.97-vacc0.85-auc0.87-vtp21-vfp4-vtn13-vfn2.h5'
model_cy_str = './xai/models/cytotoxicity-ep78-loss0.06-acc0.99-auc0.99-tp2592-fp63-tn2545-fn4-vloss0.98-vacc0.86-auc0.84-vtp537-vfp79-vtn575-vfn109.h5'
model_co_str = './xai/models/594coco2-ep01-loss0.02-acc0.00-auc0.00-tp0-fp0-tn0-fn1088-vloss0.12-vacc0.00-auc0.00-vtp0-vfp0-vtn0-vfn122.h5'
model_BB_str = './xai/models/349BBB-ep01-loss-0.00-acc0.95-auc0.97-tp1167-fp56-tn286-fn27-vloss0.03-vacc0.93-auc0.94-vtp137-vfp11-vtn28-vfn2.h5'


##k_sess = tf.Session()
##init = tf.global_variables_initializer()
##k_sess.run(init)
##K.set_session(k_sess)
def predict(model_type, input_str, input_type='smiles', delete=True):
##    old_sess = K.get_session()
##    sess = tf.Session()
##    K.set_session(sess)
    sess = K.get_session()
    init = tf.global_variables_initializer()
    sess.run(init)
##    print(sess)
##    K.clear_session()
##    print(sess)
##    if sess is tf.get_default_session():
##        print("True")
##    else:
##        print("False")
##    return 
##    K.set_session(sess)
##    sess = K.get_session()
##    sess = tf.Session()
    models = []
    if model_type == 'herg' or 'herg' in model_type:
        model = keras.models.load_model(model_herg_str, custom_objects=custom_objects)
        models.append(model)
    if model_type == 'mutagenicity' or 'mutagenicity' in model_type:
        model = keras.models.load_model(model_mu_str, custom_objects=custom_objects)
        models.append(model)
    if model_type == 'carcinogens' or 'carcinogens' in model_type:
        model = keras.models.load_model(model_ca_str, custom_objects=custom_objects)
        models.append(model)
    if model_type == 'cytotoxicity' or 'cytotoxicity' in model_type:
        model = keras.models.load_model(model_cy_str, custom_objects=custom_objects)
        models.append(model)
    if model_type == 'coco2' or 'coco2' in model_type:
        model = keras.models.load_model(model_co_str, custom_objects=custom_objects)
        models.append(model)
    if model_type == 'BBB' or 'BBB' in model_type:
        model = keras.models.load_model(model_BB_str, custom_objects=custom_objects)
        models.append(model)
    if len(models) == 0:
        return None

    if input_type == 'smiles':
        inputfile = create_file(input_str)
    else:
        inputfile = input_str

    trainset, testset, validset = make_dataset(inputfile, f1=1.)
    if delete:
        delete_file(inputfile)
    data = batch_dataset(trainset)
    symbol = get_symbols(trainset)
    smiles = get_smiles(trainset)
    for i in data:
        j = next(symbol)
        k = next(smiles)
        np_features = to_numpy(i[1])
        np_edge_idx = to_numpy(i[3])
        np_edge_num = to_numpy(i[4])
        np_edge_val = to_numpy(i[5])

        sy = j.new_symbols
        n = len(sy)
        candidates = [i[1]]

        result = []
        rrrr = []
##        with sess: #.as_default():
##        model = keras.models.load_model(model_mu_str, custom_objects=custom_objects)
        if '_' in model_type:
          for mmmm in models:
              for f in generate_inputs(candidates, i):
                  result = cat(result, to_1d(sess.run(mmmm(f))), -1)
                  rrrr.append(to_number(result))
                  result = []
        else:
          for f in generate_inputs(candidates, i):
            result = cat(result, to_1d(sess.run(model(f))), -1)
##    sess.close()
    K.clear_session()
##    K.set_session(old_sess)
##    del model
    gc.collect()
    if '_' in model_type:
      return rrrr
    else:
      return to_number(result)

def to_response_format(atom):
    return {'atom':atom[0], 'id':atom[1], 'new':round(atom[2], 3), 'old':round(atom[3], 3), 'new_minus_old':round(atom[4], 3), 'old_minus_new':round(-atom[4], 3)}

#import numpy as np
#import json
#class NpEncoder(json.JSONEncoder):
#    def default(self, obj):
#        if isinstance(obj, np.integer):
#            return int(obj)
#        elif isinstance(obj, np.floating):
#            return float(obj)
#        elif isinstance(obj, np.ndarray):
#            return obj.tolist()
#        else:
#            return super(NpEncoder, self).default(obj)
#
#
#if __name__ == '__main__':
##    print(predict('herg', 'C1CN(CCC1NC2=NC3=CC=CC=C3N2CC4=CC=C(C=C4)F)CCC5=CC=C(C=C5)O'))
##    a = (predict('herg', 'CC1=C(C)C2=C(CC[C@@](C)(CCC[C@H](C)CCC[C@H](C)CCCC(C)C)O2)C(C)=C1O'))
#    a = (predict('herg', 'CCN(CC)CCCC(C)NC1=CC=NC2=CC(Cl)=CC=C12'))
##    a = (predict('herg', 'O=C1C2=C(C=CC=C2O)CC2=CC=CC(O)=C12'))
##    a = (predict('herg', 'NC1=CC=NC=C1N'))
##    a = predict('herg', 'C1CN(CCC1NC2=NC3=CC=CC=C3N2CC4=CC=C(C=C4)F)CCC5=CC=C(C=C5)O')
##    for element in a:
#        element.pop('new_minus_old', None)
#        element.pop('old', None)
#        element.pop('new', None)
#        element.pop('atom', None)
#    print(json.dumps(a, sort_keys=True, indent=2, cls=NpEncoder))
