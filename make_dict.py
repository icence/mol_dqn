from subprocess_call import call_herg, call_mutagenicity
from rdkit import Chem, DataStructs
from rdkit.Chem import AllChem
import sys

def get_similarity(smiles, smiles2):
    structure = Chem.MolFromSmiles(smiles)
    if structure is None:
      return 0.0
    fingerprint_structure = AllChem.GetMorganFingerprint(structure, radius=2)

    structure2 = Chem.MolFromSmiles(smiles2)
    if structure2 is None:
      return 0.0
    fingerprint_structure2 = AllChem.GetMorganFingerprint(structure2, radius=2)
    return DataStructs.TanimotoSimilarity(fingerprint_structure, fingerprint_structure2)

def value(s, l):
    s = s.strip()
    l = l.strip()
    val = get_similarity(s, l)
    val2 = call_herg(l)
    val3 = call_mutagenicity(l)
    return (val, val2, val3)

def make_str(s, v1, v2, v3):
    z = [s.strip(), str(round(v1,3)), str(round(v2,3)), str(round(v3,3))]
    z[0] = z[0] + (85-len(z[0]))*" "
    return z[0]+"\t"+z[1]+"\t"+z[2]+"\t"+z[3]+"\n"

d = '/src/mnt/dqn/6/mol_dqn/mu4/predict'
g = ['/6next', '/5next', '/4next', '/3next', '/2next', '/1next']
dic = {}
smile1 = ""
lists = []
for i in g:
    with open (d+i, "r") as f:
        for l in f:
            if smile1 == "":
                smile1 = l.split(',')[0]
##                print(smile1)
##                sys.exit(0)
            if dic.get(l) is None:
               a, b, c= value(smile1, l)
               lists.append(make_str(l, a, b, c))
               dic[l] = 1
            
with open(d + "/table", "w") as f:
    for i in lists:
        f.writelines(i)
