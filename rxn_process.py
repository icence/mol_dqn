from rdkit.Chem import AllChem
from rdkit import Chem
from itertools import chain
import json

def to_smiles(mol_tuple):
    return tuple(Chem.MolToSmiles(mol) for mol in mol_tuple)

def sanitize_mols(smiles):
    r = []
    for s in smiles:
        if Chem.MolFromSmiles(s):
            r.append(s)
    return r


class React():
    def __init__(self, dic):
        self.dic = dic
        self.rxn = AllChem.ReactionFromSmarts(dic.get('smarts'), useSmiles=True)
        self.reactants = [Chem.MolFromSmiles(x, sanitize=False) for x in dic.get('reactants')]
        self.backup = self.reactants[self.dic.get('placeholder')]

    def check_res_similarity(self, res, smiles):
        if smiles is None:
            l = 1
        else:
            l = len(smiles)
        r = []
        for i in res:
            if len(i)*2 > l and len(i) <= 300:
                r.append(i)
        return r

    def run_rxn(self, smiles, mol):
        lower_smiles = str.lower(smiles)
        for x in self.dic.get('sig'):
            if x not in lower_smiles:
                return []
            
        try:
            if mol:
                self.reactants[self.dic.get('placeholder')] = mol
                reactants = self.reactants
            else:
                return []
            res = self.rxn.RunReactants(reactants)
            res = [to_smiles(mol_tuple) for mol_tuple in res]
            res = sorted(list(set(chain(*res))))
            res = sanitize_mols(res)
            res = self.check_res_similarity(res, smiles)
            self.reactants[self.dic.get('placeholder')] = self.backup
            return res
        except:
            self.reactants[self.dic.get('placeholder')] = self.backup
            return []

def write_cache_file(smiles, value, filename):
    with open(filename, "w") as f:
        f.writelines("{}_{}\n".format(smiles, value))


if __name__ == '__main__':
    viewed = []
    with open('zxfp_reaction') as f:
        dics = [json.loads(x.strip().replace("'", '"')) for x in f]
    rxns = [React(x) for x in dics]
    import time
    while True:
        time.sleep(25)
        with open('zin') as f:
            lines = [x.strip() for x in f]
        lines = lines[-1:]
        if lines[-1] in viewed:
            print("viewed:", lines[-1])
            continue
        else:
            viewed.append(lines[-1])
        ret = []
        for x in lines:
            x = x.strip()
            mol = Chem.MolFromSmiles(x)
            for r in rxns:
                r = r.run_rxn(x, mol)
                ret.extend(r)
        ret = sorted(list(set(ret)))
        write_cache_file(x, ret, 'zout')       
