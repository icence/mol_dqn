from xai.load_model import predict
#from xai.back import predict
import numpy as np
import json
class NpEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        else:
            return super(NpEncoder, self).default(obj)


#print(predict('herg', 'C1CN(CCC1NC2=NC3=CC=CC=C3N2CC4=CC=C(C=C4)F)CCC5=CC=C(C=C5)O'))
#print(predict('mutagenicity', 'Oc1ccc(cc1)[N+](=O)[O-]'))
#print(predict('mutagenicity', r'C/C1=C2/N=C(/C=C3\N=C(/C(C)=C4\[N-]C(C)(C5N=C1C(C)(CCC(=O)NCC(C)OP(=O)([O-])O[C@@H]1[C@@H](CO)O[C@H](N6C=NC7=C6C=C(C)C(C)=C7)[C@@H]1O)C5CC(N)=O)C(C)(CC(N)=O)C4CCC(N)=O)C(C)(CC(N)=O)C3CCC(N)=O)C(C)(C)C2CCC(N)=O.[C-]#N.[Co+3]'))
##for i in range(3):
##print (predict('mutagenicity', r'C/C1=C2/N=C(/C=C3\N=C(/C(C)=C4/[C@@H](CCC(N)=O)[C@](C)(CC(N)=O)[C@](C)([C@@H]5N=C1[C@](C)(CCC(=O)NC[C@@H](C)OP(=O)([O-])O[C@@H]1[C@@H](CO)O[C@H](N6C=NC7=C6C=C(C)C(C)=C7)[C@@H]1O)[C@H]5CC(N)=O)N4[Co+]C#N)[C@@](C)(CC(N)=O)[C@@H]3CCC(N)=O)C(C)(C)[C@@H]2CCC(N)=O'))

##print(predict('herg','Cc1ccc(NN=C2C(=O)C=Cc3cc(S(=O)(=O)O)ccc32)cc1'))
##                     'Cc1ccc(NN=C2C(=O)C=Cc3cc(S(=O)(=O)O)ccc32)cc1'
##                        Cc1ccc(NN=C2C(=O)C=Cc3cc(S(=O)(=O)O)ccc32)cc1 


Oc1ccc(Cl)cc1Sc1cc(Cl)ccc1O 0.97621834
Clc1nc(NCCc2ccccc2)c2ncn(-c3ccccc3)c2n1 0.77470315
CC(C)Cn1cnc2c(SCc3ccc(Cl)cc3Cl)nc(N)nc21 0.99441904
CN(C)CCCN1c2ccc(O)cc2Sc2ccc(Cl)cc21 0.9986259
Cc1ccc(NN=C2C(=O)C=Cc3cc(S(=O)(=O)O)ccc32)cc1 0.9736744

print(predict('herg', 'Clc1nc(NCCc2ccccc2)c2ncn(-c3ccccc3)c2n1'))
