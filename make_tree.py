import os 
import queue  
from subprocess_call import call_herg
from rdkit import Chem, DataStructs
from rdkit.Chem import AllChem

q = queue.Queue(maxsize = 100000)

def get_similarity(smiles, smiles2):
    structure = Chem.MolFromSmiles(smiles)
    if structure is None:
      return 0.0
    fingerprint_structure = AllChem.GetMorganFingerprint(structure, radius=2)

    structure2 = Chem.MolFromSmiles(smiles2)
    if structure2 is None:
      return 0.0
    fingerprint_structure2 = AllChem.GetMorganFingerprint(structure2, radius=2)
    return DataStructs.TanimotoSimilarity(fingerprint_structure, fingerprint_structure2)

root = 0
def mkdir(s, l):
    os.makedirs(s, exist_ok=True)
    print(l)
    val = get_similarity(root, l)
    val2 = call_herg(l)
    with open(s + '/' + str(val) + '_' + str(val2), 'w'):
        pass

head = 'b/'
with open('iiii', 'r') as f:
    for l in f:
        l = l.strip()
        root = l
        mkdir(head + l, l)
        q.put(head + l + '/')

files = ['b/5', 'b/4', 'b/3', 'b/2', 'b/1']
count = -1
for i in files:
    with open(i, 'r') as f:
        for l in f:
            count += 1
            if count % 5 == 0:
                head = q.get()
            d, s = l.split()[0], l.split()[1]
            mkdir(head + s, s)
            q.put(head + s + '/')

