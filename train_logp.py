# coding=utf-8
# Copyright 2020 The Google Research Authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Lint as: python2, python3
"""Optimizes QED of a molecule with DQN.

This experiment tries to find the molecule with the highest QED
starting from a given molecule.
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import functools
import json
import os

from absl import app
from absl import flags

from rdkit import Chem

from rdkit.Chem import QED
from tensorflow.compat.v1 import gfile
from mol_dqn.chemgraph.dqn import deep_q_networks
from mol_dqn.chemgraph.dqn import molecules as molecules_mdp
from mol_dqn.chemgraph.dqn import run_dqn
from mol_dqn.chemgraph.dqn.tensorflow_core import core
from subprocess_call import call_protein_docking, call_herg, call_mutagenicity, call_carcinogens, call_cytotoxicity, call_coco2, call_bbb, call_all
import time
import math
import asyncio
import tensorflow as tf
import hashlib
import sys
from reaction_list import get_rxns, get_rxns_dicts
from cache import read
import requests

loop = asyncio.get_event_loop()

flags.DEFINE_string('inputfile',
                    '',
                    'The init mols.')

flags.DEFINE_string('rxn',
                    '',
                    'The rxn file.')

flags.DEFINE_string('predict_step',
                    '',
                    'The overwrite step.')
FLAGS = flags.FLAGS

H_DICTS = "fileh"
M_DICTS = "filem"
A_DICTS = "filea"
Y_DICTS = "filey"
O_DICTS = "fileo"
B_DICTS = "fileb"
MOB_DICTS = {}

def call_logp(smiles):
    mol = Chem.MolFromSmiles(smiles)
    if mol is None:
        return 0.0
    return Chem.Descriptors.MolLogP(mol)

def get_cache_file(smiles, filename):
    if os.path.exists(filename) == False:
        with open(filename, "w"):
            pass

    with open(filename) as f:
        for line in f:
            line = line.split()
            if line[0] == smiles.strip():
                return float(line[1])


def write_cache_file(smiles, value, filename):
    with open(filename, "a") as f:
        f.writelines("{} {}\n".format(smiles, value))


def print_tf(_name="", _type="trainable"):
  if _type == "trainable":
    all_vars = tf.trainable_variables()
  else:
    all_vars = tf.global_variables()

  if _name:
    v = tf.get_default_graph().get_tensor_by_name(_name)
    print("%s with value %s" % (v.name, tf.get_default_session().run(v)))
  else:
    for v in all_vars:
      print("%s with value %s" % (v.name, tf.get_default_session().run(v)))
##      print("%s with value " % (v.name, ))


def string_hash(text):
  return int(hashlib.md5(text.encode("utf-8")).hexdigest()[:24], 16)


def between(num, Max, Min):
  return max(min(num, Max), Min)

start_time = int(time.time())
class HergRewardMolecule(molecules_mdp.MultipleInitMolecule):
  """The molecule whose reward is the herg."""

  def __init__(self, discount_factor, idx, record_path=True, **kwargs):
    """Initializes the class.

    Args:
      discount_factor: Float. The discount factor. We only
        care about the molecule at the end of modification.
        In order to prevent a myopic decision, we discount
        the reward at each step by a factor of
        discount_factor ** num_steps_left,
        this encourages exploration with emphasis on long term rewards.
      **kwargs: The keyword arguments passed to the base class.
    """
    super().__init__(**kwargs)
    self.record_path=record_path
    self.reward_path=[]
    self.idx = idx
    self.discount_factor = discount_factor
    self.fileidx = 0
    self.result_reward = 0
    self.targets = ['logp']
    self.logstr = ""

  def _get_mol_reward(self, state, dicts, func, arg1=''):
      for i in range(3):
          try:
       ##      score = get_cache_file(state, dicts)
              score = None
              print("ls:", state, score)
              if score is not None:
                  return score
              else:
                  molecule = Chem.MolFromSmiles(state)
                  if arg1 == '':
                      score = 0.0 if (molecule is None or state == "") else func(state)
                  else:
                      score = 0.0 if (molecule is None or state == "") else func(state, arg1)
        ##        write_cache_file(state, score, dicts)
                  return score
          except Exception as e:
              time.sleep(10)
              if i == 2:
                   raise e

  def logp_score(self, score, threshold):
      score = -abs(1.5 - score)
      if score >= 0 and score <= 3:
          return pow(1.2, score)
      else:
          return pow(1.8, score)

  def weighted_score(self, score, threshold):
      score = 1 - score 
      if score > threshold:
          return threshold + 0.3 * (score - threshold)
      else:
          return score

  def get_xai_points(self, state, targets):
      if state == "":
          return [1,1,1,1,1,1,1,1]

      herg = self._get_mol_reward(state, H_DICTS, call_herg) if 'herg' in targets else 1
      muta = self._get_mol_reward(state, M_DICTS, call_mutagenicity) if 'mutagenicity' in targets else 1
      carc = self._get_mol_reward(state, A_DICTS, call_carcinogens) if 'carcinogens' in targets else 1
      cyto = self._get_mol_reward(state, Y_DICTS, call_cytotoxicity) if 'cytotoxicity' in targets else 1
      coco2 = between((self._get_mol_reward(state, O_DICTS, call_coco2) +3.5) / -4.5, 1, 0) if 'coco2' in targets else 1
      bbb = 1 - self._get_mol_reward(state, B_DICTS, call_bbb) if 'bbb' in targets else 1
      logp = self._get_mol_reward(state, B_DICTS, call_logp) if 'logp' in targets else 1
      docking = self._get_mol_reward(state, B_DICTS, call_protein_docking, 'casp3') if 'docking' in targets else 1

      return [herg, muta, carc, cyto, coco2, bbb, logp, docking]


  def get_penalty(self, state):
      sim_init = self.get_len_similarity(state, self._get_init_state())
      sim_last = self.get_len_similarity(state, self._get_last_state())
      stop_penalty = self._get_stop_penalty(state)
      return [sim_init, sim_last, stop_penalty]

  def _get_total(self, state, targets):
      if state == "":
          return 0, [0,0,0], [1,1,1,1,1,1,1,1]
      xai_points = self.get_xai_points(state, targets)
      penalty = self.get_penalty(state)

      score = 0
      threshold = [1,0.3,1,1,0.33,0.3, 1, 1]
      for i, j in zip(xai_points, threshold):

          score += self.weighted_score(i, j)
      score = self.logp_score(xai_points[-2], 0)
      # sudden death for non-similarity
      for s in self._path:
          if self.get_len_similarity(s, self._get_init_state()) < 0.45:
              print(s, self._get_init_state())
              print(self.get_len_similarity(s, self._get_init_state()))
              score = 0
      print("************score", score, xai_points)
      score = between(score, len(targets), 0)
      return score, penalty, xai_points

  def _get_init_state(self):
      return self._path[0]

  def _get_last_state(self):
      try:
          return self._path[-2]
      except:
          return self._path[0]

  def _get_stop_penalty(self, state):
      return 0.002 if state == self._get_last_state() else 0

  def _reward(self):
      """Reward of a state.
  
      Returns:
        Float. Diff of the current state.
      """
      if FLAGS.predict_step:
          return 0.0
      if self.num_steps_taken == 1:
          self.A = self._get_total(self._get_init_state(), targets=self.targets)
          self.last_total = self.A[0]
          self.last_sim = self.A[1]
          self.last_score = self.A[2]
          self.last_non_blank_total = self.last_total
          if self.idx == 0:
              t = int(time.time()) - start_time
              print("tim:", t//3600, t//60, t)
              with open(self.model_dir + "/" + str(self.fileidx), "w") as f:
                  f.writelines(make_str(self._get_last_state(), [self.last_total, 0, self.last_sim[-1], *(self.last_score)]))
  
      total, sim, score = self._get_total(self._state, targets=self.targets)
      self.write_log(total, sim, score)
      print(total, self.last_total, "************8")
      diff = total - self.last_total - sim[-1]
      self.last_sim = sim
      self.last_score = score
  
      ret = 0
      if self.num_steps_taken >= self.max_steps:
          ret = self.last_non_blank_total if self._state == "" else total

      self.last_total = total
      
      if self._state != "":
          self.last_non_blank_total = total
  
      if self._state:
          ret += diff
      else:
          ret -= 0.0005
      
      with open(self.model_dir + "/" + str(self.fileidx), "a+") as f:
          f.writelines(make_str(self._state, [total, ret, sim[-1], *score]))
      if self.num_steps_taken >= self.max_steps:
          self.fileidx += 1
          ret += total 
      print("ret:", ret)
      print("diff****************", diff)
      return ret

  def write_log(self, total, sim, score):
      targets = self.targets
      if self.idx == 0:
          if self.num_steps_taken == 1:  
              with open(self.model_dir + '/log' + str(self.fileidx), "w") as f:
                  rprint("No.:", 0, f)
                  rprint("Ori:", self._get_last_state(), f)
                  if 'herg' in targets: 
                      rprint("her:", self.last_score[0], f)
                  if 'mutagenicity' in targets:
                      rprint("mut:", self.last_score[1], f)
                  if 'carcinogens' in targets:
                      rprint("car:", self.last_score[2], f)
                  if 'cytotoxicity' in targets:
                      rprint("cyt:", self.last_score[3], f)
                  if 'coco2' in targets:
                      rprint("cac:", self.last_score[4], f)
                  if 'bbb' in targets:
                      rprint("bbb:", self.last_score[5], f)
                  if 'logp' in targets:
                      rprint("logp:", self.last_score[6], f)
                  if 'docking' in targets:
                      rprint("docking:", self.last_score[7], f)
  
          if self.num_steps_taken <= self.max_steps:
              with open(self.model_dir + '/log' + str(self.fileidx), "a+") as f:
                  rprint("No.:", self.num_steps_taken, f)
                  rprint("sta:", self._state, f)
                  if 'herg' in targets: 
                      rprint("her:", score[0], f)
                  if 'mutagenicity' in targets:
                      rprint("mut:", score[1], f)
                  if 'carcinogens' in targets:
                      rprint("car:", score[2], f)
                  if 'cytotoxicity' in targets:
                      rprint("cyt:", score[3], f)
                  if 'coco2' in targets:
                      rprint("cac:", score[4], f)
                  if 'bbb' in targets:
                      rprint("bbb:", score[5], f)
                  if 'logp' in targets:
                      rprint("logp:", score[6], f)
                  if 'docking' in targets:
                      rprint("docking:", score[7], f)
                  rprint("sim:", sim[0], f)
                  rprint("lst:", self.last_total, f)
                  rprint("all:", total, f)
                  rprint("dif:", total - self.last_total - sim[-1], f)
          else:
              with open(self.model_dir + '/log' + str(self.fileidx), "a+") as f:
                  rprint ("No.:", self.num_steps_taken, f)
                  rprint ("sta:", "END_STATE", f)
  
  
def make_str(s, a):
  if s.strip() == "":
     s = "END_STATE===================================================================================="
  z = [s.strip()]
 
  for i in a:
    z.append(str(round(i,3)))

  z[0] = z[0] + "\n"
  return "\t".join(z) + '\n'

def rprint(a, x, fp=None):
  if fp:
    if type(x) == type(""):
      fp.writelines(a + ' ' + x + '\n')
    else:
      fp.writelines(a + ' ' + str(round(x, 3)) + '\n')
  if type(x) != type(""):
    print(a, round(x, 3))
  else:
    print(a, x)


def main(argv):
  del argv  # unused.
  if FLAGS.hparams is not None:
    with gfile.Open(FLAGS.hparams, 'r') as f:
      hparams = deep_q_networks.get_hparams(**json.load(f))
  else:
    hparams = deep_q_networks.get_hparams()
  if hparams.rxn_files:
      rxns = get_rxns(hparams.rxn_files)
  else:
      rxns = None

  if FLAGS.rxn:
      rxn_dicts = get_rxns_dicts()
  else:
      rxn_dicts = None

  if FLAGS.old_model_dir:
      read(FLAGS.old_model_dir, H_DICTS, M_DICTS)
  dqn = deep_q_networks.DeepQNetwork(
      step = 0,
      input_shape=(hparams.batch_size, hparams.fingerprint_length*2 + 1),
      q_fn=functools.partial(
          deep_q_networks.multi_layer_model, hparams=hparams),
      optimizer=hparams.optimizer,
      grad_clipping=hparams.grad_clipping,
      num_bootstrap_heads=hparams.num_bootstrap_heads,
      gamma=hparams.gamma,
      epsilon=1.0)

  environments = []
  for i in range(1):
    environment = HergRewardMolecule(
        idx = i,
        discount_factor=hparams.discount_factor,
        atom_types=set(hparams.atom_types),
        init_mols=FLAGS.inputfile,
        allow_removal=hparams.allow_removal,
        allow_no_modification=hparams.allow_no_modification,
        allow_bonds_between_rings=hparams.allow_bonds_between_rings,
        allowed_ring_sizes=set(hparams.allowed_ring_sizes),
        max_steps=hparams.max_steps_per_episode,
        model_dir=FLAGS.model_dir,
        rxns=rxns,
        rxn_dicts=rxn_dicts)
    environments.append(environment)

  tf.reset_default_graph()

  with tf.Session() as sess:
    dqn.build()
    all_vars = tf.global_variables()
    if FLAGS.old_model_dir:
        sess.run(tf.global_variables_initializer())
##        saver = tf.train.Saver(tf.trainable_variables())
        saver = tf.train.Saver()
        saver.restore(sess, tf.train.latest_checkpoint(FLAGS.old_model_dir))
    else:
        sess.run(tf.global_variables_initializer())
    print_tf("d_1/OptimizeLoss/beta1_power:0", "")
    dqn.step = run_dqn.val_tf(dqn.scope + "/global_step:0")
    run_dqn.run_training(hparams, environments[0], dqn, sess)
  core.write_hparams(hparams, os.path.join(FLAGS.model_dir, 'config.json'))


if __name__ == '__main__':
    app.run(main)
