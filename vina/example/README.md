# Vina and Obabel
This package contains scripts for get affinity score of docking.
inputs: 
* 1a1b_ligand.smi
* 1a1b_protein.pdb

## parse ligand
```
# 產生 3d 的坐標. 需要 minimize energy 嗎? 需要哪些键是可以旋轉的嗎
obabel 1a1b_ligand.smi --gen3d -O 1a1b_ligand.smi.pdbqt 
```
## parse protein
```
# 加上 polar H
obabel 1a1b_protein.pdb --addpolarh -O 1a1b_protein.temp.pdbqt
# 保留 ATOM 開頭，去除水分子
cat 1a1b_protein.temp.pdbqt | grep ATOM | grep -v HOH > 1a1b_protein.pdb.pdbqt
```
## run Vina
```
vina --config vina.config --receptor 1a1b_protein.pdb.pdbqt --ligand 1a1b_ligand.smi.pdbqt
# choose the best mode
```
## make config
```
# 怎麼選中心點? 目前是選 protein 中心
center_x = ?
center_y = ?
center_z = ?
# 目前是固定是 40. 需要調成跟 ligand 或 protein 一樣大嗎?
size_x = ??
size_y = ??
size_z = ??
...
```



