from rdkit.Chem import AllChem
from itertools import chain
from rdkit.Chem import AllChem 
from rdkit import Chem 
import json

def to_smiles(mol_tuple): 
    return tuple(Chem.MolToSmiles(mol) for mol in mol_tuple)

def sanitize_mols(smiles):
    r = []
    for s in smiles:
        if Chem.MolFromSmiles(s):
            r.append(s) 
    return r 

reaction_list = [
"",
"reaction_files/1-Angeli_Rimini_hydroxamic_acid_synth.rxn",
"reaction_files/2-Baeyer_Villiger_carbonyl_oxidation.rxn",
"reaction_files/3-Barton_deamination.rxn",
"reaction_files/4-Baylis_Hillman_vinyl_alkylation.rxn",
"reaction_files/5-Beckmann_rearrangement.rxn",
"reaction_files/6-Benary_reaction.rxn",
"reaction_files/7-Biginelli_pyrimidone_synthesis.rxn",
"reaction_files/8-Bischler_Mohlau_indole_synthesis.rxn",
"reaction_files/9-Bischler_Napieralski_isoquinoline_synthesis.rxn",
"reaction_files/10-Blanc_chloromethylation.rxn",
"reaction_files/11-Blum_aziridine_synthesis_with_azide.rxn",
"reaction_files/12-Borch_reductive_amination.rxn",
"reaction_files/13-Borsche_Beech_aromatic_carbonyl_synthesis.rxn",
"reaction_files/14-Bouveault_Locquin_amino_acid_synthesis.rxn",
"reaction_files/15-Bredereck_imidazole_synthesis_from_acil_methyl_acilates.rxn",
"reaction_files/16-Bredereck_imidazole_synthesis_from_alpha_substituted_ketones.rxn",
"reaction_files/17-Bruylants_amination.rxn",
"reaction_files/18-Bruylants_amination_intramolecular.rxn",
"reaction_files/19-Bucherer_LePetit_naphtol_synthesis.rxn",
"reaction_files/20-Bucherer_LePetit_naphtylamine_synthesis.rxn",
"reaction_files/21-Buchwald_Hartwig_cross_coupling.rxn",
"reaction_files/22-Burgess_dehidration_reagent.rxn",
"reaction_files/23-Burton_trifluoromethylation.rxn",
"reaction_files/24-Chan_Lam_coupling.rxn",
"reaction_files/25-Chan_reduction_of_acetylenes.rxn",
"reaction_files/26-Chapman_rearrangement.rxn",
"reaction_files/27-Chichibabin_amination.rxn",
"reaction_files/28-Clay_Kinnear_Perren_phosphoryl_chloride_synthesis.rxn",
"reaction_files/29-Clemensen_reduction.rxn",
"reaction_files/30-Collman_carbonylation_reagent_in_aldehyde_synhesis.rxn",
"reaction_files/31-Cope_elimination.rxn",
"reaction_files/32-Corey_Chaykovsky_reaction.rxn",
"reaction_files/33-Corey_Winter_olefin_synthesis.rxn",
"reaction_files/34-Criege_glycol_cleavage.rxn",
"reaction_files/35-Curtius_rearrangement.rxn",
"reaction_files/36-Dakin_oxidation.rxn",
"reaction_files/37-Darapsky_degradation.rxn",
"reaction_files/38-Darzens_epoxide_synthesis.rxn",
"reaction_files/39-Delepine_aldehyde_oxidation.rxn",
"reaction_files/40-Delepine_amine_synthesis.rxn",
"reaction_files/41-Dess_Martin_alcohol_oxidizing_reagent.rxn",
"reaction_files/42-Doering_LaFlamme_allene_synthesis.rxn",
"reaction_files/43-Dondoni_homologation.rxn",
"reaction_files/44-Einhorn_Brunner_triazole_synthesis.rxn",
"reaction_files/45-Erlenmeyer_amino_acid_synthesis.rxn",
"reaction_files/46-Eschenmoser_fragmentation.rxn",
"reaction_files/47-Eschenmoser_methylenation.rxn",
"reaction_files/48-Eschweiler_Clarke_amine_methylation.rxn",
"reaction_files/49-Feist_Benary_furane_synthesis.rxn",
"reaction_files/50-Finnegan_tetrazole_synthesis.rxn",
"reaction_files/51-Fischer_indole_synthesis.rxn",
"reaction_files/52-Fischer_oxazole_synthesis_from_cyanohydrins.rxn",
"reaction_files/53-Fischer_oxazole_synthesis_from_hydroxyamides.rxn",
"reaction_files/54-Forster_diazo_synthesis.rxn",
"reaction_files/55-Freund_Gustavson_cyclopropane_synthesis.rxn",
"reaction_files/56-Friedel_Crafts_acylation.rxn",
"reaction_files/57-Friedlander_quinoline_synthesis.rxn",
"reaction_files/58-Fritsch_Buttenberg_Wiechell_acetylene_synthesis.rxn",
"reaction_files/59-Fukuyama_coupling.rxn",
"reaction_files/60-Gabriel_amine_synthesis.rxn",
"reaction_files/61-Gewald_aminofurane_synthesis.rxn",
"reaction_files/62-Gewald_aminopyrrole_synthesis.rxn",
"reaction_files/63-Gewald_aminothiophene_synthesis.rxn",
"reaction_files/64-Goldberg_coupling.rxn",
"reaction_files/65-Grignard_addition_to_carbonyl_compounds.rxn",
"reaction_files/66-Grignard_reagent_formation.rxn",
"reaction_files/67-Guaresky_Thorpe_pyridone_synthesis.rxn",
"reaction_files/68-Guy_Lemaire_Guette_ortho_chlorination.rxn",
"reaction_files/69-Guy_Lemaire_Guette_para_chlorination.rxn",
"reaction_files/70-Hass_Bender_carbonyl_synthesis.rxn",
"reaction_files/71-Heck_reaction.rxn",
"reaction_files/72-Hell_Volhardt_Zelinski_bromination.rxn",
"reaction_files/73-Henry_nitro_condensation.rxn",
"reaction_files/74-Herz_benzothiazole_synthesis.rxn",
"reaction_files/75-Hiyama_coupling.rxn",
"reaction_files/76-Hofmann_amide_degradation.rxn",
"reaction_files/77-Horner_Wadsworth_Emmons_olefination.rxn",
"reaction_files/78-Houben_Hoesch_phenol_acylation.rxn",
"reaction_files/79-Houben_Hoesch_phenol_formylation.rxn",
"reaction_files/80-Hunsdiecker_Borodin_Cristol_Firth_decarboxylation_bromination.rxn",
"reaction_files/81-Jones_oxidation_of_primer_alcohols.rxn",
"reaction_files/82-Jones_oxidation_of_secunder_alcohols.rxn",
"reaction_files/83-Keinan_silane_reagent_for_reductive_iodination.rxn",
"reaction_files/84-Kendall_Mattox_dehydrohalogenation.rxn",
"reaction_files/85-Knoevenagel_condensation.rxn",
"reaction_files/86-Knoevenagel_condensation_Doebner_modification.rxn",
"reaction_files/87-Knorr_quinoline_synthesis.rxn",
"reaction_files/88-Kornblum_aldehyde_synthesis.rxn",
"reaction_files/89-Kuhn_Winterstein_olefination.rxn",
"reaction_files/90-Kumada_Negishi_coupling.rxn",
"reaction_files/91-Kumada_coupling.rxn",
"reaction_files/92-Lawesson_thiacarbonylation.rxn",
"reaction_files/93-Leuckart_Wallach_reductive_amination.rxn",
"reaction_files/94-Lieben_hypohalide_oxidation_haloform_reaction.rxn",
"reaction_files/95-Madelung_indole_synthesis.rxn",
"reaction_files/96-Mannich_reaction.rxn",
"reaction_files/97-McFadyen_Stevens_reduction.rxn",
"reaction_files/98-Meerwein_Ponndorf_Verley_reduction.rxn",
"reaction_files/99-Milas_olefin_hydroylation.rxn",
"reaction_files/100-Miller_Loudon_Snyder_nitrile_synthesis.rxn",
"reaction_files/101-Miyaura_borylation_reaction.rxn",
"reaction_files/102-Negishi_coupling.rxn",
"reaction_files/103-Noyori_asymmetric_hydrogenation_of_carbonyls.rxn",
"reaction_files/104-O_Donnell_amino_acid_synthesis.rxn",
"reaction_files/105-Oppenauer_oxidation.rxn",
"reaction_files/106-Oxime_from_active_CH_with_nitrosation.rxn",
"reaction_files/107-Oxime_from_carbonyls_with_hydroxylamine.rxn",
"reaction_files/108-Paal_Knorr_furan_synthesis.rxn",
"reaction_files/109-Paal_Knorr_pyrrole_synthesis.rxn",
"reaction_files/110-Paal_Knorr_thiophene_synthesis.rxn",
"reaction_files/111-Passerini_condensation.rxn",
"reaction_files/112-Perkin_reaction.rxn",
"reaction_files/113-Perkin_rearrangement.rxn",
"reaction_files/114-Pfitzinger_quinoline_synthesis.rxn",
"reaction_files/115-Quellet_chloroalkylation.rxn",
"reaction_files/116-Reformatsky_reaction.rxn",
"reaction_files/117-Reformatsky_reaction_intramolecular.rxn",
"reaction_files/118-Reformatsky_reaction_of_thiocarbonyls.rxn",
"reaction_files/119-Ritter_reaction_intramolecular.rxn",
"reaction_files/120-Ritter_reaction_of_alcohols.rxn",
"reaction_files/121-Ritter_reaction_of_alkenes.rxn",
"reaction_files/122-Stille_carbonyl_synthesis.rxn",
"reaction_files/123-Stille_coupling.rxn",
"reaction_files/124-Suzuki_coupling.rxn",
"reaction_files/125-THP_group_cleavage.rxn",
"reaction_files/126-Ullmann_condensation.rxn",
"reaction_files/127-Ullmann_reaction_classic.rxn",
"reaction_files/128-Voight_alpha_aminoketone_synthesis.rxn",
"reaction_files/129-Volhardt_Erdmann_thiophene_synthesis.rxn",
"reaction_files/130-Wacker_Tsuji_olefin_oxidation.rxn",
"reaction_files/131-Wolff_rearrangement.rxn",
"reaction_files/132-acid_azide_synthesis.rxn",
"reaction_files/133-acyl_isocyanate_from_amides_with_oxalyl_chloride.rxn",
"reaction_files/134-alcohol_oxidation_with_Corey_Kim_reagent.rxn",
"reaction_files/135-alcohol_oxidation_with_Corey_reagent.rxn",
"reaction_files/136-alcohol_protection_with_Grieco_reagent.rxn",
"reaction_files/137-carboxylic_acid_chloride_synthesis.rxn",
"reaction_files/138-diazoalkane_acylation.rxn",
"reaction_files/139-diazotation_of_aromatic_amines.rxn",
"reaction_files/140-enamine_formation_with_modified_Mannich_reaction.rxn",
"reaction_files/141-imino_ester_synthesis.rxn",
"reaction_files/142-isocyanate_synthesis_from_amines_with_phosgene.rxn",
"reaction_files/143-isocyanate_with_nucleophile.rxn",
"reaction_files/144-nucleophile_acylation_with_acid_halide.rxn",
"reaction_files/145-tosylation_with_Koser_s_reagent.rxn",
"reaction_files/146-nitrogenReduction.rxn",
"reaction_files/147-chloro_nitro.rxn",
"reaction_files/148-NH2R_FR_RNHR.rxn",
"reaction_files/149-CL_BO2H2_.rxn",
"reaction_files/150-RH2_RCL_RNHR.rxn",
"reaction_files/151-pdc18tmp.rxn",
"reaction_files/152-pdc18tmp.rxn",
"reaction_files/153-pdc9a.rxn",
"reaction_files/154-pdc9b.rxn",
"reaction_files/155-pdc10a.rxn",
"reaction_files/156-pdc11b.rxn",
"reaction_files/157-producingPdc12.rxn",
"reaction_files/158-producingPdc6.rxn",
"reaction_files/159-producingPdc57.rxn",
"reaction_files/160-producingPdc9_X.rxn",
"reaction_files/161-producingPdc10_o.rxn",
"reaction_files/162-producingPdc14.rxn",
"reaction_files/163-producingPdc28a_X.rxn",
"reaction_files/164-producingPdc28b_X.rxn",
"reaction_files/165-producingPdc21a_X.rxn",
"reaction_files/166-producingPdc21b_X.rxn",
"reaction_files/167-producingPdc28pre.rxn",
"reaction_files/168-producingPdc24pre.rxn",
"reaction_files/169-producingPdc57pre.rxn",
"reaction_files/170-producingPdc24pre_X4F.rxn",
"reaction_files/171-producingPdc10_N.rxn",
"reaction_files/172-producingPdc10_X.rxn",
"reaction_files/173-producingPdc9_o.rxn",
"reaction_files/174-producingPdc9_N.rxn",
"reaction_files/175-producingPdc28a_o.rxn",
"reaction_files/176-producingPdc28a_N.rxn",
"reaction_files/177-producingPdc28b_o.rxn",
"reaction_files/178-producingPdc28b_N.rxn",
"reaction_files/179-producingPdc21a_o.rxn",
"reaction_files/180-producingPdc21a_N.rxn",
"reaction_files/181-producingPdc21b_o.rxn",
"reaction_files/182-producingPdc21b_N.rxn",
"reaction_files/183-chromenF.rxn",
"reaction_files/184-chromeOHCCfor6.rxn",
"reaction_files/185-ketone2alcohoalID6.rxn",
"reaction_files/186-thiazolCClID6tmp.rxn",
"reaction_files/187-chromenNHRfor16.rxn",
"reaction_files/188-chromeSbenzofluorofor2.rxn",
"reaction_files/189-benzoBralcoholfor2.rxn",
"reaction_files/190-chromenBrID12.rxn",
"reaction_files/191-CCChromeNSBRlinkfor6.rxn",
"reaction_files/192-SNketoneAlcohalfor12R.rxn",
"reaction_files/193-anilinecccNNbenzo.rxn",
"reaction_files/194-benzoicOpiperidyl9298585.rxn",
"reaction_files/195-benzoxazoleaminePiperydlfor1d.rxn",
"reaction_files/196-NNbenzoCOaniline.rxn",
"reaction_files/197-piperidylbenzeDiCl_compd47.rxn",
"reaction_files/198-piperylOCCC.rxn",
]

def get_rxns(lists):
    r = []
    for x in lists:
        if x == 153 or x == 154:
            continue
        r.append(AllChem.ReactionFromRxnFile(reaction_list[x]))
    return r

class Reaction():
    def __init__(self, smarts, reactants, line_id=0):
        self.smarts = smarts
        self.line_id = line_id
        self.rxn = None
        self.reactants_smiles = reactants
        maxlen = 0
        for i in reactants:
            maxlen = max(len(i), maxlen)
        for h, i in enumerate(reactants):
            if maxlen == len(i):
                self.pit = h

##        self.reactants = [Chem.MolFromSmiles(x, sanitize=False) for x in reactants]
##        self.reactants_pit = self.reactants[self.pit]
        self.backup = self.reactants_smiles[self.pit]

    def check_pit_similarity(self, smiles):
        abc = "abcdefghijklmnopqrstuvwxyz"
        lst = ["br", "si", "li", "na"]
        a = str.lower(smiles)
        b = str.lower(self.backup)
        for i in abc:
            if i in b:
                if i in a == False:
                    return False
        for i in lst:
            if i in b:
                if i in a == False:
                    return False
        return True

    def check_res_similarity(self, res, smiles):
        if smiles is None:
            l = 1
        else:
            l = len(smiles)
        r = []
        for i in res:
            if len(i)*2 > l and len(i) <= 300:
                r.append(i)
        return r

    def run_rxn(self, smiles=None):
        if smiles is None:
            pass
        elif self.check_pit_similarity(smiles):
            self.reactants_smiles[self.pit] = smiles
            reactants = [Chem.MolFromSmiles(smiles, sanitize=False)]
            self.reactants_smiles[self.pit] = self.backup
            for i in reactants:
                if i is None:
                    return []
        else:
            return []

        if self.rxn is None:
            self.rxn = AllChem.ReactionFromSmarts(self.smarts, useSmiles=True)

        try:
            res = self.rxn.RunReactants(reactants)
            res = [to_smiles(mol_tuple) for mol_tuple in res]
            res = sorted(list(set(chain(*res))))
            res = sanitize_mols(res)
            res = self.check_res_similarity(res, smiles)
            return res
        except:
##            print(self.line_id, self.smarts)
##            self.reactants[self.pit] = self.reactants_pit
            return []


def get_rxns_dicts():
    a = 'zz_reaction'
    with open (a) as f:
        for x in f:
            z = [json.loads(x.replace("'", '"')) for x in f]
##    z = [Reaction(i.get('smarts'), i.get('reactants'), n) for n, i in enumerate(z)]
    return z

import queue
import asyncio
loop = asyncio.get_event_loop()

def predict(smiles_filename, rxn_filename):
    with open(smiles_filename) as f:
        lines = [x.strip() for x in f]
    smiles = lines[0].split(',')[0]
    with open(rxn_filename) as f:
        lines = [x.strip() for x in f]
##    rxns = [json.loads(x.replace("'", '"')) for x in lines]
    rxns = lines
    out_queue = queue.Queue()
##    for rxn in rxns:
    for i in range(0, len(rxns), 20):
        tasks = [loop.create_task(_async_add_reaction2_job(smiles, x, smiles_filename, out_queue)) for x in rxns[i:i+20]]
        loop.run_until_complete(asyncio.wait(tasks))
        with open('/tmp/zlog', 'w') as f:
            f.write(str(i))
    return list(set(out_queue.queue))

def predict_rxn(smiles_filename):
    with open(smiles_filename) as f:
         lines = [x.strip() for x in f]
##    with open('/tmp/zlogg', 'w') as f:
##         f.write(lines[0])
##         f.write(lines[1])
    smiles = lines[0]
    rxn = lines[1]
    rxn = json.loads(rxn.replace("'", '"')) 
    ret = Reaction(rxn.get('smarts'), rxn.get('reactants')).run_rxn(smiles)
    return ret




from subprocess_call import call_rxn

def _add_reaction2_job(smiles, rxn, smiles_filename, out_queue):
##    ret = Reaction(rxn.get('smarts'), rxn.get('reactants')).run_rxn()
    ret = call_rxn(smiles, rxn, smiles_filename)

    for i in ret:
        out_queue.put(i)

async def _async_add_reaction2_job(smiles, rxn, smiles_filename, out_queue, func=_add_reaction2_job):
    return await loop.run_in_executor(None, func, smiles, rxn, smiles_filename, out_queue)



##if __name__ == '__main__':
##    for i in get_rxns_dicts():
##        import time
##        time.sleep(120)
##        break

##    from rdkit import Chem
##    print(type(get_rxns([1,3,4])[0]))
##    print(type(get_rxns([1,3,4])[1]))
##    print(type(get_rxns([1,3,4])[2]))
##    rxn = get_rxns([1,3,4])[2]
##    mols = [Chem.MolFromSmiles("COC(=O)C=C")]
##    mols = [Chem.MolFromSmiles("CCC=O"), mols[0]]
##    all_products_tuples = [rxn.RunReactants((mols[0], mols[1] ))]
##    print (all_products_tuples)
