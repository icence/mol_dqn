run:
	python main.py  --model_dir="/src/mnt/dqn/6/mol_dqn/2a"  --inputfile="/src/mnt/dqn/6/mol_dqn/in" --hparams="./configs/naive_dqn.json" ;
	sleep 10;
	python main.py  --model_dir="/src/mnt/dqn/6/mol_dqn/2b"  --old_model_dir="/src/mnt/dqn/6/mol_dqn/2a" --inputfile="/src/mnt/dqn/6/mol_dqn/in" --hparams="./configs/naive_dqn.json" ;
	sleep 10;
	python main.py  --model_dir="/src/mnt/dqn/6/mol_dqn/2c"  --old_model_dir="/src/mnt/dqn/6/mol_dqn/2b" --inputfile="/src/mnt/dqn/6/mol_dqn/in" --hparams="./configs/naive_dqn.json" ;
d:
	vi mol_dqn/chemgraph/dqn/deep_q_networks.py
m:
	vi mol_dqn/chemgraph/dqn/molecules.py
r:
	vi mol_dqn/chemgraph/dqn/run_dqn.py
c:
	vi configs/naive_dqn.json
z:
	python 4train.py --model_dir="/src/mnt/dqn/6/mol_dqn/4reward" --inputfile="/src/mnt/dqn/6/mol_dqn/min250" --hparams="./configs/naive_dqn.json"
a:
	vi train.py
r2:
	python train.py --model_dir="/src/mnt/dqn/6/mol_dqn/mu4" --old_model_dir="/src/mnt/dqn/6/mol_dqn/mu3"  --inputfile="/src/mnt/dqn/6/mol_dqn/min25" --hparams="./configs/naive_dqn.json"
z1:
	python train4.py --model_dir="/src/mnt/dqn/6/mol_dqn/40" --inputfile="/src/mnt/dqn/6/mol_dqn/min250" --hparams="./configs/naive_dqn.json"
z2:
	python train4.py --model_dir="/src/mnt/dqn/6/mol_dqn/41" --old_model_dir="/src/mnt/dqn/6/mol_dqn/40" --inputfile="/src/mnt/dqn/6/mol_dqn/min250" --hparams="./configs/naive_dqn.json"
z3:
	python train4.py --model_dir="/src/mnt/dqn/6/mol_dqn/42" --old_model_dir="/src/mnt/dqn/6/mol_dqn/41" --inputfile="/src/mnt/dqn/6/mol_dqn/min250" --hparams="./configs/naive_dqn.json"
z4:
	python train4.py --model_dir="/src/mnt/dqn/6/mol_dqn/43" --old_model_dir="/src/mnt/dqn/6/mol_dqn/42" --inputfile="/src/mnt/dqn/6/mol_dqn/min250" --hparams="./configs/naive_dqn.json"
e1:
	python train5.py --model_dir="/src/mnt/dqn/6/mol_dqn/50" --inputfile="/src/mnt/dqn/6/mol_dqn/min250" --hparams="./configs/naive_dqn.json"
e2:
	python train5.py --model_dir="/src/mnt/dqn/6/mol_dqn/51" --old_model_dir="/src/mnt/dqn/6/mol_dqn/50" --inputfile="/src/mnt/dqn/6/mol_dqn/min250" --hparams="./configs/naive_dqn.json"
e3:
	python train5.py --model_dir="/src/mnt/dqn/6/mol_dqn/52" --old_model_dir="/src/mnt/dqn/6/mol_dqn/51" --inputfile="/src/mnt/dqn/6/mol_dqn/min250" --hparams="./configs/naive_dqn.json"
e4:
	python train5.py --model_dir="/src/mnt/dqn/6/mol_dqn/53" --old_model_dir="/src/mnt/dqn/6/mol_dqn/52" --inputfile="/src/mnt/dqn/6/mol_dqn/min250" --hparams="./configs/naive_dqn.json"
w1:
	python train6.py --model_dir="/src/mnt/dqn/6/mol_dqn/60" --inputfile="/src/mnt/dqn/6/mol_dqn/min250" --hparams="./configs/naive_dqn.json"
w2:
	python train6.py --model_dir="/src/mnt/dqn/6/mol_dqn/61" --old_model_dir="/src/mnt/dqn/6/mol_dqn/60" --inputfile="/src/mnt/dqn/6/mol_dqn/min250" --hparams="./configs/naive_dqn.json"
w3:
	python train6.py --model_dir="/src/mnt/dqn/6/mol_dqn/62" --old_model_dir="/src/mnt/dqn/6/mol_dqn/61" --inputfile="/src/mnt/dqn/6/mol_dqn/min250" --hparams="./configs/naive_dqn.json"
w4:
	python train6.py --model_dir="/src/mnt/dqn/6/mol_dqn/63" --old_model_dir="/src/mnt/dqn/6/mol_dqn/62" --inputfile="/src/mnt/dqn/6/mol_dqn/min250" --hparams="./configs/naive_dqn.json"

a1:
	python train7.py --model_dir="/src/mnt/dqn/6/mol_dqn/70" --inputfile="/src/mnt/dqn/6/mol_dqn/min250" --hparams="./configs/naive_dqn.json"
a2:
	python train7.py --model_dir="/src/mnt/dqn/6/mol_dqn/71" --old_model_dir="/src/mnt/dqn/6/mol_dqn/70" --inputfile="/src/mnt/dqn/6/mol_dqn/min250" --hparams="./configs/naive_dqn.json"
a3:
	python train7.py --model_dir="/src/mnt/dqn/6/mol_dqn/72" --old_model_dir="/src/mnt/dqn/6/mol_dqn/71" --inputfile="/src/mnt/dqn/6/mol_dqn/min250" --hparams="./configs/naive_dqn.json"
aa:
	python train7.py --model_dir="/src/mnt/dqn/6/mol_dqn/72a" --old_model_dir="/src/mnt/dqn/6/mol_dqn/71" --inputfile="/src/mnt/dqn/6/mol_dqn/i1" --hparams="./configs/naive_dqn.json"
ab:
	python train8.py --model_dir="/src/mnt/dqn/6/mol_dqn/80b" --inputfile="/src/mnt/dqn/6/mol_dqn/i1" --hparams="./configs/naive_dqn.json"
ab2:
	python train8.py --model_dir="/src/mnt/dqn/6/mol_dqn/80c" --old_model_dir="/src/mnt/dqn/6/mol_dqn/80b" --inputfile="/src/mnt/dqn/6/mol_dqn/i1" --hparams="./configs/naive_dqn.json"
ab3:
	python train8.py --model_dir="/src/mnt/dqn/6/mol_dqn/80d" --old_model_dir="/src/mnt/dqn/6/mol_dqn/80c" --inputfile="/src/mnt/dqn/6/mol_dqn/i1" --hparams="./configs/naive_dqn.json"
ab4:
	python train8.py --model_dir="/src/mnt/dqn/6/mol_dqn/80e" --old_model_dir="/src/mnt/dqn/6/mol_dqn/80d" --inputfile="/src/mnt/dqn/6/mol_dqn/i1" --hparams="./configs/naive_dqn.json"
ab5:
	python train8.py --model_dir="/src/mnt/dqn/6/mol_dqn/80f" --old_model_dir="/src/mnt/dqn/6/mol_dqn/80e" --inputfile="/src/mnt/dqn/6/mol_dqn/i1" --hparams="./configs/naive_dqn.json"
ab51:
	python train9.py --model_dir="/src/mnt/dqn/6/mol_dqn/81a" --old_model_dir="/src/mnt/dqn/6/mol_dqn/80f" --inputfile="/src/mnt/dqn/6/mol_dqn/i1" --hparams="./configs/naive_dqn.json"
ab52:
	python train9.py --model_dir="/src/mnt/dqn/6/mol_dqn/81b" --old_model_dir="/src/mnt/dqn/6/mol_dqn/81a" --inputfile="/src/mnt/dqn/6/mol_dqn/i1" --hparams="./configs/naive_dqn.json"
ab53:
	python train9.py --model_dir="/src/mnt/dqn/6/mol_dqn/81c" --old_model_dir="/src/mnt/dqn/6/mol_dqn/81b" --inputfile="/src/mnt/dqn/6/mol_dqn/i1" --hparams="./configs/naive_dqn.json"
test3:
	python train.py --model_dir="/src/mnt/dqn/6/mol_dqn/90" --old_model_dir="/src/mnt/dqn/6/mol_dqn/81b" --inputfile="/src/mnt/dqn/6/mol_dqn/i1" --hparams="./configs/naive_dqn.json"
q1:
	python traina.py --model_dir="/src/mnt/dqn/6/mol_dqn/a1" --inputfile="/src/mnt/dqn/6/mol_dqn/i1" --hparams="./configs/naive_dqn.json"
q2:
	python traina.py --model_dir="/src/mnt/dqn/6/mol_dqn/a2" --old_model_dir="/src/mnt/dqn/6/mol_dqn/a1" --inputfile="/src/mnt/dqn/6/mol_dqn/i1" --hparams="./configs/naive_dqn.json"
q3:
	python traina.py --model_dir="/src/mnt/dqn/6/mol_dqn/a3" --old_model_dir="/src/mnt/dqn/6/mol_dqn/a2" --inputfile="/src/mnt/dqn/6/mol_dqn/i1" --hparams="./configs/naive_dqn.json"
bb1:
	python trainb.py --model_dir="/src/mnt/dqn/6/mol_dqn/b1" --inputfile="/src/mnt/dqn/6/mol_dqn/i1" --hparams="./configs/naive_dqn.json"
bb2:
	python trainb.py --model_dir="/src/mnt/dqn/6/mol_dqn/b2" --old_model_dir="/src/mnt/dqn/6/mol_dqn/b1" --inputfile="/src/mnt/dqn/6/mol_dqn/i1" --hparams="./configs/naive_dqn.json"
cc1:
	python trainb.py --model_dir="/src/mnt/dqn/6/mol_dqn/c1" --inputfile="/src/mnt/dqn/6/mol_dqn/i2" --hparams="./configs/naive_dqn.json"
cc2:
	python trainb.py --model_dir="/src/mnt/dqn/6/mol_dqn/c2" --old_model_dir="/src/mnt/dqn/6/mol_dqn/c1" --inputfile="/src/mnt/dqn/6/mol_dqn/i2" --hparams="./configs/naive_dqn.json"
kk:
	python m_process_predict.py 00
	python m_process_predict.py 01
	python m_process_predict.py 02
	python m_process_predict.py 10
	python m_process_predict.py 11
	python m_process_predict.py 12
	python m_process_predict.py 20
	python m_process_predict.py 21
	python m_process_predict.py 22
ab44:
	python train8.py --model_dir="/src/mnt/dqn/6/mol_dqn/80ee"  --inputfile="/src/mnt/dqn/6/mol_dqn/i1" --hparams="./configs/naive_dqn.json"
zz9:
	python train9.py --model_dir="/src/mnt/dqn/6/mol_dqn/90a" --inputfile="/src/mnt/dqn/6/mol_dqn/i1" --hparams="./configs/z.json"
s:
	python call_vina.py ./vina/1e66_protein.pdb ./vina/smi.smi
ss:
	python call_vina.py ./vina/1a1b_protein.pdb  ./vina/1a1b_ligand.mol2
sss:
	python call_vina.py ./vina/1a1b_protein.pdb  ./vina/test2.smi
qq:
	python call_multi_vina.py ./vina/1a1b_protein.pdb  ./vina/7a
qqq:
	python call_multi_vina.py ./vina/1a1b_protein.pdb  ./vina/8a
qqqq:
	python call_vina.py ./vina/1a1b_protein.pdb  ./vina/8a_0.smi
qqqqq:
	python call_vina.py ./vina/1a1b_protein.pdb  ./vina/7a_999.smi
zz1:
	python trainc.py --model_dir="/src/mnt/dqn/6/mol_dqn/zzz2" --inputfile="/src/mnt/dqn/6/mol_dqn/i2" --rxn="true" --hparams="./configs/naive_dqn.json" 
zz2:
	python trainc.py --model_dir="/src/mnt/dqn/6/mol_dqn/zzz3" --old_model_dir="/src/mnt/dqn/6/mol_dqn/zzz2" --inputfile="/src/mnt/dqn/6/mol_dqn/i2" --rxn="true" --hparams="./configs/naive_dqn.json" 
aa2:
	python traind.py --model_dir="/src/mnt/dqn/6/mol_dqn/zq1" --inputfile="/src/mnt/dqn/6/mol_dqn/i2" --rxn="true" --hparams="./configs/naive_dqn.json"
aa3:
	python traind.py --model_dir="/src/mnt/dqn/6/mol_dqn/za3" --old_model_dir="/src/mnt/dqn/6/mol_dqn/zzz2" --inputfile="/src/mnt/dqn/6/mol_dqn/i2" --rxn="true" --hparams="./configs/naive_dqn.json" > 1
zza2:
	python train_logp.py --model_dir="/src/mnt/dqn/6/mol_dqn/logp1" --inputfile="/src/mnt/dqn/6/mol_dqn/i" --rxn="true" --hparams="./configs/naive_dqn.json"
dock:
	python train_docking.py --model_dir="/src/mnt/dqn/backup6/6/mol_dqn/zw" --inputfile="/src/mnt/dqn/6/mol_dqn/idocking" --rxn="true" --hparams="./configs/naive_dqn.json"
mutaz:
	python train_mutagenicity.py --model_dir="/src/mnt/dqn/backup6/6/mol_dqn/muta" --inputfile="/src/mnt/dqn/backup6/6/mol_dqn/i" --rxn="true" --hparams="./configs/naive_dqn_muta.json"
logpz:
	python train_logp.py --model_dir="/src/mnt/dqn/backup6/6/mol_dqn/logp3" --inputfile="/src/mnt/dqn/backup6/6/mol_dqn/i" --rxn="true" --hparams="./configs/naive_dqn_muta.json"
logpz1:
	python train_logp_model.py --model_dir="/src/mnt/dqn/backup6/6/mol_dqn/logp4" --inputfile="/src/mnt/dqn/backup6/6/mol_dqn/i" --rxn="true" --hparams="./configs/naive_dqn_muta.json"
