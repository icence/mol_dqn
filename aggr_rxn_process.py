import os
import threading
import time
import json

def sleep():
    time.sleep(5)    

def job(num):
  print("Thread", num)
  time.sleep(1)

def read_first_line(filename):
    with open(filename) as f:
        return f.readline()

def get_cache_file(smiles, filename):
##    if os.path.exists(filename) == False:
##        with open(filename, "w"):
##            pass
    with open(filename) as f:
        for line in f:
            line = line.split('_')
            if line[0] == smiles.strip() and line[1].strip()[-1] == ']':
                return json.loads(line[1].strip().replace("'", '"'))
            else:
                print(line[0])
                print('zin',smiles.strip())
                print(line[0] == smiles.strip())
                print(line[1].strip()[-1] == ']')

def write_cache_file(smiles, value, filename):
    with open(filename, "w") as f:
        f.writelines("{}_{}\n".format(smiles, value))


if __name__ == '__main__':
  viewed = []
  inputfile = 'zin'
  outputfiles = []
  for i in range(16):
      outputfiles.append('zout'+str(i+1))

  while True:
    smiles = read_first_line(inputfile).strip()
    if smiles:
      if smiles in viewed:
        print("viewed", smiles)
      else:
        ret = []
        time.sleep(1)
        for i in outputfiles:
            while True:
                r = get_cache_file(smiles, i)
                if r is not None:
                    ret.extend(r)
                    break
                else:
                    print("wait job:", i)
                    time.sleep(1)
        write_cache_file(smiles, sorted(list(set(ret))), 'zout')
        viewed.append(smiles)
##        return smiles
##    else:
##        return []

##    o = []
##    for i in range(16):
##        o.append('zout'+str(i+1))
##    s = remote_call("", 'zin', o)
##    sleep()

       
##print(remote_call()
##print("Done.")
